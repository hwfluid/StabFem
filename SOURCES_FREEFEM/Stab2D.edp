//
//  PROGRAM Stab_2D.edp
//	
//	Performs the linear stability analysis of a 2D flow
//   
//
//  INPUT PARAMETERS (from keyboard or pipe) :
//   	Re , shift(Re,im) , Symmetry, Type, nev 
//
//   this solver will use either Arnoldi (if nev>1) or simple Shift-invert (if nev=1)
//
//	INPUT FILES :
//		BaseFlow.txt	-> base flow 
// 		mesh.msh 			->  mesh in Freefem format
//  OUTPUT FILES :
//	single-mode calculation :
//		Eigenmode.txt   (txt format for FreeFem)
//      Eigenmode.ff2m  (ff2m format for stabfem)
// 		EigenmodeA.txt  ADJOINT  (txt format for FreeFem)
//      EigenmodeA.ff2m ADJOINT  (ff2m format for stabfem)
//      Sensitivity.txt 
//		Sensitivity.ff2m
//
// multiple-mode calculation :
//  	Eigenmode##.txt   (txt format for FreeFem)
//      Eigenmode##.ff2m  (ff2m format for stabfem)
//
// in all modes :
//		Spectrum.txt -> All computed Eigenvalues
//
// 
// 	ERROR MANAGEMENT : 
//      if Newton iteration diverges, Eigenmode.txt is not generated and "iter" parameter in Eigenmode.ff2m is -1.
//

cout << "$$$$ ENTERING Stab2D.edp " << endl << "$$" << endl;    
//include "SF_Geom.edp"; // to be removed soon
include "Macros_StabFem.idp";


//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 1 : parameters		
//


	real Re;                  
	cout << "$$ Enter Reynolds                     >>   "; 
	cin  >> Re;
	cout << Re << endl;
	real shiftr,shifti;	
	cout << "$$ Enter SHIFT (re,im)                 >>  " ; 
	cin  >> shiftr >> shifti;
	cout << shiftr << " " << shifti << endl;
	complex shift = 1i*shifti+shiftr;
	string symmetry;
	cout << "$$ Symmetry properties ?? (A, S or N).  >> " ;
	cin >> symmetry;
	cout << symmetry << endl;
	string iadjoint; // 0 for direct ; 1 for adjoint
	cout << "$$ Direct (D), Adjoint (A), D&A+sensitivity (S) >> " ;
	cin >> iadjoint;	
	cout << iadjoint << endl;
	int nev;
    cout << "$$ Enter nev ? (will use simple shift-invert if nev = 1) >> " ;
    cin >> nev ;
    cout << nev << endl;
	

	
	cout << "$$ ### PARAMETERS SELECTED : " << endl;
	cout << "$$ Re = " << Re<< endl;
	cout << "$$ shift = " << shift << endl;
	
		if (symmetry =="S") {cout << "$$ Symmetric modes" << endl;}
		else if (symmetry =="A") {cout << "$$ Antiymmetric modes" << endl;}	
		else if (symmetry =="N") {cout << "$$ No symmetry axis" << endl;}
		;	
	
		if (iadjoint =="D") {cout << "$$ DIRECT PROBLEM" << endl;}
		else if (iadjoint =="A") {cout << "$$ ADJOINT PROBLEM (discrete)" << endl;}
		else if (iadjoint =="S"||iadjoint =="E") {cout << "$$ DIRECT+ADJOINT (discrete) PROBLEM INCLUDING SENSITIVITY/Endogeneity" << endl;}
		else {cout << "$$ ERROR WHEN SELECTING PROBLEM TYPE" << endl;}
		;
	
	cout << "$$ nev = " << nev << endl;
	
//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 2 : read files		
//

mesh th=readmesh(ffdatadir+"mesh.msh");                    ///// Read mesh

fespace Xh(th,P2);             ////// f.e. space for vel.
fespace Mh(th,P1);            ////// f.e. space for pressure
fespace XXMh(th,[P2,P2,P1]); ////// f.e. space for triplet [u,v,p]
XXMh<complex> [ux,uy,up];                 ///////  -> unknown for the Navier-Stokes problem
XXMh [vx,vy,q];                 ///////  -> test for Navier-Stokes (should be defined as complex ???)
XXMh [Ubx,Uby,Pb];          ///////  -> Base Flow



real ReB;
{
ifstream cbin(ffdatadir+"BaseFlow.txt");
cbin >> Ubx[] >> ReB;
}
if(Re!= ReB)
{ 
cout << "Warning : Re != ReB, is that really what you mean ???" << endl;
} 

	real nu=1./Re; 
	// If Nu is something else than 1/Re define a macro CUSTOMnu in your Macros_StabFem.idp file
    IFMACRO(CUSTOMnu)
    nu = CUSTOMnu;
    ENDIFMACRO


//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 3 : Customizable macros 	
//

// A. to disable complex mapping in case it is not needed
IFMACRO(!dX)
macro dX(a) dx(a) //EOM
macro dY(a) dy(a) //EOM
macro JJJ   1. //EOM
ENDIFMACRO

//
// Note : this solver requires three macros BoundaryConditionsStability, SFWriteMode, NormalizeMode.
// Below are "default" values for these macros, to be used in the most basic cases. If you want to
// customize these macros don't modify them here but copy them to your Macros_StabFem.idp file.
//

IFMACRO(!BoundaryconditionsStability)
macro BoundaryconditionsStability(u,v,symmetry)
	           on(1,ux#=0,u#y=0.0) 		/* label 1 = inlet */
             + on(2,21,22,23,u#x=0.0,u#y=0.0) 	/* label 2 = wall */
             							/* Label 3 is outlet : No-stress as a result of integration by parts, nothing to write here ! */
             + on(4,u#y=0.0)  			/* Label 4 is 'slip' or 'symmetry' condition on a horizontal boundary */
             + on(5,u#x=0.0)  			/* Label 5 is 'slip' or 'symmetry' condition on a vertical boundary */
             + int1d(th,6)(u#x*v#x*1e30*(symmetry=="A")+u#y*v#y*1e30*(symmetry=="S"))      
             							/* Label 6 is a symmmetry axis */
             							/* NB label 7 -> porous surface or deformable solid ; label 8 -> curved, deformable surface ; label 9 -> inner surface */  
//EOM
ENDIFMACRO

IFMACRO(!SFWriteMode)
macro SFWriteMode(namefile,u,ev,shift,typeFlow,iter)
		 {
		 ofstream file(namefile);
   		 fespace p1forff2m(th,P1); 
    	 p1forff2m<complex> vort1;  		 
 		 file << "### Data generated by Freefem++ ; " << endl;
    	 file << "Eigenmode for a 2D-incompressible problem " << endl;
    	 file << "datatype " << typeFlow << " datastoragemode CxP2P2P1 datadescriptors ux,uy,p" << endl;
    	 string descriptionFF="real* Re complex* eigenvalue complex shift int iter P1c vort"; 
    	 file << descriptionFF << endl << endl ; 
		 file << Re  << endl << real(ev) << endl << imag(ev) << endl << real(shift) << endl << imag(shift) << endl << iter << endl << endl;
		 vort1=-dY(u#x)+dX(u#y);		
		for (int j=0;j<vort1[].n ; j++) file << real(vort1[][j]) << endl << imag(vort1[][j]) << endl;
		};
//EOM
ENDIFMACRO	

IFMACRO(!NormalizeMode)
macro NormalizeMode(u)
{
real NORM = sqrt(int2d(th)(abs(u#x)^2+abs(u#y)^2));
u#x[] = u#x[]/NORM;
}
//EOM
ENDIFMACRO


macro div(u) (dX(u#x)+dY(u#y))// macro for divergence 
macro Conv(ua,ub,v) ( 
              ((ua#x*dX(ub#x)+ua#y*dY(ub#x))+(ub#x*dX(ua#x)+ub#y*dY(ua#x)))*v#x 
            + ((ua#x*dX(ub#y)+ua#y*dY(ub#y))+(ub#x*dX(ua#y)+ub#y*dY(ua#y)))*v#y 
                    ) // macro for mutual convection operator
macro D(u) [[dX(u#x), .5*(dX(u#y)+dY(u#x))], [.5*(dX(u#y)+dY(u#x)), dY(u#y)]] // macro for rate-of-deformation tensor
macro Diffusion(nu,ux,uy,vx,vy)  (-nu*( dX(ux)*dX(vx)+dY(ux)*dY(vx)+dX(uy)*dX(vy)+dY(uy)*dY(vy))) // integration by parts of nu (v.Delta u) 

real eps = 1e-12; // desingularisation term for matrix B, useful with some solvers


//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 4 : Definition of operators
//



/////////////////////////////////////////////////////////////////////////////////
////////    ---> varf for generalized eigenvalue problem: 		<--- //////////
////////////////////////////////////////////////////////////////////////////////////



varf   LNSE ([ux,uy,up],[vx,vy,q]) =
   int2d(th)( 
   			(-2*nu*(D(u):D(v))
             + up*q*(eps) 
             + up*div(v)
             + div(u)*q
             - Conv(u,Ub,v))*JJJ
             )
 + int2d(th)( -shift*(ux*vx+uy*vy)*JJJ )
 + BoundaryconditionsStability(u,v,symmetry);

varf   LNSEadjointDiscrete ([ux,uy,up],[vx,vy,q]) =
   int2d(th)( 
   			(-2*nu*(D(u):D(v))
             + up*q*(eps) 
             + up*div(v)
             + div(u)*q
             - Conv(v,Ub,u))*JJJ
             )
 + int2d(th)( -conj(shift)*(ux*vx+uy*vy)*JJJ ) // warning that shift/eigenvalues of adjoint are complex conjugates of direct !
 + BoundaryconditionsStability(u,v,symmetry); 


////// BUILD B-MATRIX 
	varf b([ux,uy,up],[vx,vy,q]) = int2d(th)( (ux*vx+uy*vy)*JJJ );
		matrix<complex> B= b(XXMh,XXMh,solver=CG);  //////// see freefem++doc.pdf for the solver 

matrix<complex> OP,OPA;
complex shiftOP,shiftOPA;




if(nev>1)
{
//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 5(a) : solves the problem (Arnoldi mode for nev>1)
//


////// BUILD OP-MATRIX=A-lambda*B 
if(iadjoint=="D")
	{ OP=LNSE(XXMh,XXMh,solver=sparsesolver); shiftOP = shift; }
else if(iadjoint=="A")
	{ OP=LNSEadjointDiscrete(XXMh,XXMh,solver=sparsesolver); shiftOP = conj(shift); }
else if(iadjoint=="S"||iadjoint=="cS"||iadjoint=="E") 
	{ SFwarning("$$ WARNING : in this program option S will work only for nev=1"); }
;

complex[int] ev(nev);                     ////// vector to store eigenvalues
XXMh<complex> [int] [eux,euy,eup](nev);   ////// vector to store EIGENVECTORS 


///////////////////// CALL TO ARPACK++ 
int ncv = 4*nev;   ///// Krylov Basis
int k=EigenValue(OP,B,sigma=shiftOP,value=ev,vector=eux,tol=1e-6,maxit=0,ncv=ncv);    //Arpack call
if(iadjoint=="A") { ev = conj(ev); } ;

//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 6(a) : post-processing for nev>1
//

//// POSTPROCESSING

	string namefile;
    namefile=ffdatadir+"Spectrum.txt";
    ofstream fileVP1(namefile); 
	
	for (int i=0;i<nev;i++)
	{
       fileVP1 << real(ev[i]) << " " << imag(ev[i]) << " " << Re << " " << 0 << " " << real(shift) << " " << imag(shift) << endl;
       	cout << "$$ Valeur propre : " << i+1 << "  : " << ev[i] << endl;

 		 if(iadjoint=="D"){namefile=ffdatadir+"Eigenmode"+(i+1);}
 		 else{namefile=ffdatadir+"EigenmodeA"+(i+1);};
 		 ux[] = eux[i][];
 		 NormalizeMode(u); 
 		  {
 		  ofstream fileMode(namefile+".txt");
 		  fileMode << ux[]; 
 		  fileMode << endl << endl << real(ev[i]) << endl << imag(ev[i])  << endl;
 		  }
 		 cout << "$$ Writing eigenmode in file " << namefile << endl;
 		 SFWriteMode(namefile+".ff2m",u,ev[i],shift,"EigenModeD",1);	 // MACRO DEFINED in StabFem_Macros.edp
 		 
 	};
}






else

/// SIMPLE SHIFT-INVERT IF ONLY ONE MODE IS RESQUESTED
{

//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 5(b) : solves the problem (shifted inverse power mode for nev==1)
//

XXMh<complex> [ux0,uy0,p0],[uxdirect,uydirect,pdirect],[uxadjoint,uyadjoint,padjoint]; 
complex lambdadirect;
complex lambda;	
int testCB = exec("ls "+ffdatadir+"/Eigenmode_guess.txt");
    	if (testCB!=0)
		{
		cout << "$$ No file Eigenmode_guess.txt : strarting from arbitrary initial condition" << endl;
		[ux0,uy0,p0] = [1,0,0];
		} 
		else
		{
		cout << "$$ Starting shift/invert from mode in file Eigenmode_guess.txt" << endl;
		ifstream cbin(ffdatadir+"Eigenmode_guess.txt");
		cbin >> ux0[];
		};


// selection of the computation to be performed : direct, adjoint or both
		
int directadjointA,directadjointB;
if(iadjoint=="D")
	{
	directadjointA = 1; directadjointB=1;
	cout << "$$ Shift-invert algorithm for DIRECT problem" << endl;
	}
else if(iadjoint=="A")
	{
	directadjointA = 2; directadjointB=2;
	cout << "$$ Shift-invert algorithm for ADJOINT problem" << endl;
	}
else if(iadjoint=="S"||iadjoint=="E")
	{
	directadjointA = 1; directadjointB=2;
	cout << "$$ Shift-invert algorithm for BOTH DIRECT AND ADJOINT(discrete) problem" << endl;
	}
;	
	

// LOOP TO SELECT WHICH COMPUTATION WILL BE DONE 
	for(int directadjoint=directadjointA;directadjoint<directadjointB+1;directadjoint++)	
	{

	if(directadjoint==1)
	{
		cout << "$$ Constructing operator for DIRECT problem ..." << endl;
		OP=LNSE(XXMh,XXMh,solver=sparsesolver);
		shiftOP = shift;
		cout << "$$ Solving DIRECT problem ..." << endl;
	}
	else if(directadjoint==2)
	{
		cout << "$$ Constructing operator for ADJOINT(discrete) problem ..." << endl;
		OP=LNSEadjointDiscrete(XXMh,XXMh,solver=sparsesolver);
		shiftOP = conj(shift);
		cout << "$$ Solving ADJOINT problem ..." << endl;
	}
	;

	int itmax = 50;
	complex lambda0 = 1e6;
	real err = 1e6;
	real errmax=1e-6;
	
	varf brhs([ux,uy,p],[vx,vy,q]) = int2d(th)( (ux0*vx+uy0*vy) );
	set(OP,solver=sparsesolver);  //// factorize matrix
	
	/// ITERATION LOOP
	int iter;
	for (iter=0; ((err>errmax)&&(iter<itmax)); iter++)
	{
    	complex[int] rhs= brhs(0,XXMh);  //////// see freefem++doc.pdf for the solver 
		complex[int] w = OP^-1*rhs;
		ux[] = w;
		complex XnXn1 = int2d(th)(ux0*ux+uy0*uy);
		complex Xn1Xn1 =  int2d(th)(ux*ux+uy*uy);
		complex GG = Xn1Xn1/XnXn1;
		// at each step the inverse shifted eigenval. is approximated by <X_{n+1}, Y> / <X_n, Y>
		//      ( the choice Y = X_{n+1} is convenient but we could choose something else )
		lambda = shiftOP+1/GG;
		err = abs(lambda-lambda0);
		cout << "$$ Iteration " << iter+1 << " : " << endl;	
		cout << "$$ Estimated eigenvalue lambda = " << lambda << endl;	
		ux0[] = 1/sqrt(abs(Xn1Xn1))*ux[];
		lambda0 = lambda;
	};
	/// END ITERATION LOOP	
	if(directadjoint==2||directadjoint==0){lambda=conj(lambda);};

//////////////////////////////////////////////////////////////////////////////////
//
//		CHAPTER 6(b) : post-processing for nev=1
//


// renormalization (if relevant)
IFMACRO(NormalizeMode)
	NormalizeMode(u);
ENDIFMACRO	
	
	string namefile,namefileFF,descriptionFF,typeDA; 
	if(iter<itmax)
	
	// post-processing if iteration was successful
	{	
		real lambdar = real(lambda) ; real lambdai = imag(lambda);
    	if(directadjoint==1)
    	{
    		ofstream fileVP(ffdatadir+"Spectrum.txt");
    		fileVP << lambdar << " " << lambdai << " " << Re <<  " " << real(shift) << " " << imag(shift) << endl;
    		namefile=ffdatadir+"Eigenmode.txt";
    		namefileFF=ffdatadir+"Eigenmode.ff2m";
    		typeDA = "D";
    		uxdirect[]=ux[];
    		lambdadirect = lambda;
    	};
    	
    	if(directadjoint==2)
    	{
    		ofstream fileVP(ffdatadir+"Spectrum.txt");
           	fileVP << lambdar << " " << lambdai << " " << Re << " " << real(shift) << " " << imag(shift) << endl;
    		namefile=ffdatadir+"EigenmodeA.txt";
    		namefileFF=ffdatadir+"EigenmodeA.ff2m";	
    		typeDA = "A";
    		uxadjoint[]=ux[];
    	};
    
    // write outpout in .txt format (for freefem)
    {
    ofstream file(namefile);
  	file << ux[] ;	
   	file << endl << endl << lambdar << "   " << lambdai  << endl;
    }

    // write output in .dff.2m format (for StabFem)
	SFWriteMode(namefileFF,u,lambda,shift,"EigenMode"+typeDA,iter);	 // MACRO DEFINED in StabFem_Macros.edp
	
	} // end of post processing of simple shift-invert case
else // case iteration failed
{
	SFerror("$$ SHIFT-INVERT ITERATION FAILED");
	cout << "$$ Leaving FreeFem++ with error code 202" << endl;
	exit(202);
};

if (testCB==0)
		{
		exec("rm Eigenmode_guess.txt");// to be sure this file is not reused unless explicitly requested
		};


}; //end of loop for direct/adjoint/selection


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// CHAPTER 7 : structural sensitivity
//
//


if(iadjoint=="S"||iadjoint=="E")
//
{
cout << "$$ Computation of sensitivity after direct and adjoint" << endl;
	
	// Sensitivity
	fespace p2(th,P2);fespace p1(th,P1);
	p2 sensitivity;
	sensitivity = sqrt(abs(uxadjoint)^2+abs(uyadjoint)^2) * sqrt(abs(uxdirect)^2+abs(uydirect)^2);
	real norm = sensitivity[].max;
	sensitivity=1/norm*sensitivity;

    // Endogeneity
	p2<complex> Endogeneity;
	Endogeneity = (conj(uxadjoint)*uxdirect+conj(uyadjoint)*uydirect);
	complex normalization = int2d(th)(conj(uxadjoint)*uxdirect+conj(uyadjoint)*uydirect);
	Endogeneity = lambdadirect/normalization*Endogeneity;

//	plot(Endogeneity,wait=1);
	complex sigmacheck = int2d(th)(Endogeneity);
	cout << "sigma based on Endogeneity : " << sigmacheck << endl;
   
    // Writing SENSITIVITY to files :
    string namefile,namefileFF;
    namefile=ffdatadir+"Sensitivity.txt";   
    namefileFF=ffdatadir+"Sensitivity.ff2m";
 	p1<complex> EndogeneityP1 = Endogeneity;
    p1 sensitivityP1 = sensitivity;
    {
         ofstream fileFF(namefileFF);
         fileFF << "### Data generated by Freefem++ ; " << endl;
         fileFF << "Sensitivity (wavemaker) for a 2D-incompressible problem" << endl;
    	 fileFF << "datatype Sensitivity datastoragemode ReP2.0" << endl; // if the field for adapt is endogeneity
	     fileFF << "real* Re complex* eigenvalue P1 sensitivity P1c endogeneity" << endl << endl ;
		 fileFF << Re << endl << real(lambda) << endl << imag(lambda) << endl << endl;
		 for (int j=0;j<sensitivityP1[].n ; j++) fileFF << sensitivityP1[][j] << endl;
		 for (int j=0;j<sensitivityP1[].n ; j++) fileFF << real(EndogeneityP1[][j]) << endl << imag(EndogeneityP1[][j]) << endl;
     };
	{
    	ofstream file(namefile);
		file << sensitivity[] ;	
    } 





// Writing ENDOGENEITY to files :
	namefile=ffdatadir+"Endogeneity.txt";   
    namefileFF=ffdatadir+"Endogeneity.ff2m";

    {
         ofstream fileFF(namefileFF);
         fileFF << "### Data generated by Freefem++ ; " << endl;
         fileFF << "Endogeneity for a 2D-incompressible problem" << endl;
    	 fileFF << "datatype Endogeneity datastoragemode CxP2.0" << endl; // if the field for adapt is endogeneity
	     fileFF << "real* Re complex* eigenvalue P1 sensitivity P1c endogeneity" << endl << endl ;
	     fileFF << Re << endl << real(lambda) << endl << imag(lambda) << endl << endl;
		 for (int j=0;j<sensitivityP1[].n ; j++) fileFF << sensitivityP1[][j] << endl;
		 for (int j=0;j<sensitivityP1[].n ; j++) fileFF << real(EndogeneityP1[][j]) << endl << imag(EndogeneityP1[][j]) << endl;
     };
	{
    	ofstream file(namefile);
		file << Endogeneity[] ;	
    } 



}; // end of selection of mutliple mode (krylov) / single mode (simple shift-invert)



};
cout << "$$$$ SUCCESSFULLY LEAVING Stab2D.edp " << endl ;

