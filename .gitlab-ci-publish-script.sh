#!/bin/bash

#
# This script is to publish automatically any matlab script containing the [Page] balise
# 

# TO TEST LOCALLY:
# export CI_RUNNER_TAGS='prodcom,u1804'
# ./.gitlab-ci-publish-script.sh

# ================================
# == Defining FreeFem++ command ==
# ================================
if [[  ! "$CI_RUNNER_TAGS" == *"prodcom"* ]]; then
  echo "TAG must contain 'prodcom'"
  exit 1
fi

if [[ "$CI_RUNNER_TAGS" == *"u1604"* ]]; then
    source /PRODCOM/bin/config.sh
    module load mpich/3.2.0/gcc-5.4
    module load freefem/3.61-1/gcc-5.4_mpich-3.2
    export FFBASEDIR="/PRODCOM/Ubuntu16.04/freefem/3.61-1/gcc-5.4-mpich-3.2"
    
elif [[ "$CI_RUNNER_TAGS" == *"u1804"* ]]; then
    source /PRODCOM/bin/config.sh
    module load Collection/Gnu-MPICH
    module load freefem++/3.61-1-gcc-7.3.0-mpich-3.2.1
    
    if [[ -z "${SF_AR_FFBASEDIR}" ]]; then
       export FFBASEDIR=${EBROOTFREEFEMPLUSPLUS}
    else
      export FFBASEDIR=${SF_AR_FFBASEDIR}
    fi
else
    echo "Ubuntu version should be given in gitlab-runner tags."
    exit 1
fi

if [ ! -f "${FFBASEDIR}/bin/FreeFem++" ]; then #### WARNING no more bin
  echo "Could not find FreeFem++ executable. Exiting..."
  exit 1
fi

# =============================
# == Defining MATLAB command ==
# =============================
if [[ -z "${SF_AR_MATVER}" ]]; then
  export MATVER='r2018b' # Because of the redesign of PRODCOM
else
  export MATVER=${SF_AR_MATVER}
fi

MATBIN=matlab${MATVER}

if [ ! -f "$(which ${MATBIN})" ]; then
  echo "Could not find matlab binary ${MATBIN}. Exiting..."
  exit 1
fi

### SAMPLE TEST : publish from EXAMPLE_Lshape



# =========================
# == Exporting SFBASEDIR ==
# =========================

export SFBASEDIR="$(pwd)"

# =================================
# == cleaning all html content  ==
# =================================

rm -rf */*/html

# =================================
# == GENERATING PUBLISH COMMANDS ==
# =================================
mkdir -p .local/published_html
#export SF_IGNORE_START=1 #Allows ignoring SF_core_start to use
                         # sfopts defined by AUTORUN/autorun_opts.m

for publish_script in `grep -rnw ./STABLE_CASES/*/*.m -e '\[\[PUBLISH\]\]' -l`; do
  script_name=$(basename ${publish_script})
  script_dir=$(dirname ${publish_script})
  case_name=$(basename ${script_dir})
  autopublish="./autopublish.m"

  echo "% -- AUTOPUBLISH --" > $autopublish
  echo "cd('AUTORUN')" >> $autopublish
  echo "autorun_opts" >> $autopublish
  echo "cd('../${script_dir}')" >> $autopublish
  echo "publish('${script_name}');" >> $autopublish
  echo "quit" >> $autopublish

  cmd="${MATBIN} -nodesktop -nosplash -r autopublish"
  echo "Running cmd: '${cmd}' with ${autopublish}: "
  cat $autopublish

  $cmd

  rm $autopublish

  cp -r $script_dir/html .local/published_html/${case_name}
done

