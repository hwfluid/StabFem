

%% Chapter 0 : Initialization 
SF_core_start('verbosity',3,'workdir','./WORK/');

mkdir('FIGURES');
subplot = @(m,n,p) subtightplot (m, n, p, [0.05 0.05], [0.05 0.03], [0.05 0.01]);


%% Chapter 1 : builds base flow/mesh
%
% creation of an initial mesh

    H = 2; Lpipe = 10; Rout = 80; % ,nb first case was done with Rout = 120 !
    ffmesh = SF_Mesh('mesh_ImpactingJet.edp','Params',[H Lpipe Rout],'problemtype','2D');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    
    bf = SF_Adapt(bf,'Hmax',5);
%%    
% computation of base flow with increasing Re

    for Re = [10 30 60 80 100 150 200 250 300]
        bf = SF_BaseFlow(bf,'Re',Re);
        bf = SF_Adapt(bf);
    end
%%
% Adaptation to the structure of an eigenmode AND A MASK 

    [ev,em] = SF_Stability(bf,'nev',10,'shift',0.005+0.i,'k',2,'type','D');
     Mask = SF_Mask(bf.mesh,[-10 H 0 10 .2]);
    bf = SF_Adapt(bf,em(1),Mask,'Hmax',5);

%% Chapter 2 : Demonstration of the spectrum explorator
%

plotmodeoptions = {'xlim',[-3 2],'ylim',[0 5],'colorrange','cropcenter'};
[ev,em] = SF_Stability(bf,'nev',10,'shift',1+0i,'k',2,'type','D','plotspectrum','yes','plotmodeoptions',plotmodeoptions);
[ev,em1] = SF_Stability(bf,'nev',10,'shift',1+1i,'k',2,'type','D','plotspectrum','yes','plotmodeoptions',plotmodeoptions);

%%
% If you click on the eigenvalues you will produce figures very similar to these ones :
figure;SF_Plot(em(1),'ux',plotmodeoptions{:});
figure;SF_Plot(em(2),'ux',plotmodeoptions{:});

%% Chapter 3 :  sonstruction of the curves sigma(k) for Re = 300
%
% It seems that it disapeared ! JAvier please put it back :)




%% Chapter 4 : construction of the curves sigma(Re) for k=0
% 
% 

% JAVIER IF YOU WANT TO PUBLISH THIS PART ON THE WEBSITE PLEASE MAKE IT
% CLEAN!

if (1==0)
%% 
% Stationary modes for e=2

listEVAnti = [];
listEVSymm = [];
listbf = [];
listEMAnti = [];
listEMSymm = [];

Relist = [450:-25:200]
for Re = Relist
    bf = SF_BaseFlow(bf,'Re',Re,'type','NEW');
    [evA,emA] = SF_Stability(bf,'nev',2,'shift',0.005+0.i,'k',0,'type','D','sym','A');
    [evS,emS] = SF_Stability(bf,'nev',2,'shift',0.005+0.i,'k',0,'type','D','sym','S');
    listbf = [listbf, bf];
    listEVAnti = [listEVAnti, evA];
    listEVSymm = [listEVSymm, evS];
    listEMAnti = [listEMAnti, emA];
    listEMSymm = [listEMSymm, emS];
end

%%
%figure;
%plot(Relist,real(listEVAnti),Relist,listEVSymm)
%xlabel('Re');ylabel('sigma');legend('Anti','Sym');

%%
% We extract the vorticity at the wall
%

% 7 corresponds to Re=300
yline = [8:0.01:12.0];
vortWall = SF_ExtractData(listbf(7),'vort',0,yline);
%figure; plot(yline,vortWall);
RPoint = interp1(vortWall,yline,0,'cubic');
RPointRef = RPoint/2.0;

figure; SF_Plot(listEMAnti(1),'vort','symmetry','XA');
figure; SF_Plot(listEMSymm(1),'vort','symmetry','XS');



%% Mirroring the mesh for full domain
bfM = SF_Mirror(bf);

figure;SF_Plot(bfM,'ux','xlim',[-5 5],'ylim',[-5 5]);
end

% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
