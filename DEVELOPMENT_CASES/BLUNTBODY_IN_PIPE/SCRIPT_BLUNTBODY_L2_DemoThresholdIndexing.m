%% PARAMETRIC STUDY OF INSTABILITIES IN THE WAKE OF A BLUNT BODY IN A PIPE
%
%
%    _____________________________
%   |     _____________          |
%   |    /    (L)     | (d/2)    | (D/2)    
%   |   |(l)          |          |
%    ----              -----------
%
% In the present script we use U=1 at lateral walls and inlet
% (this is controlled while generating mesh by assigning label 1 to lateral wall)
%
% Here d=1, L=2 (length of cylindrical part), l=2 (length of semiellipsoidal part)
%
% We vary d/D and Re.
%
% 
%

%% 
% THIS SCRIPT SHOWS HOW TO DO A PARAMETRIC STUDY, AND GENERATE FIGURES IN
% POST-PROCESSING MODE
 

%% 
% Initialization
%

workdir = './Work_L2_TEST/'; % WILL CREATE A NEW DATABASE ARBORESCENCE
SF_core_start('workdir',workdir,'verbosity',2); % 



%% Generation of the base flow and adapted mesh for Re=500,d/D=1/10.

dsurD = 0.1;
bf = SmartMesh_L2(dsurD);
bf.mesh.xlim = [-2 6];bf.mesh.ylim = [-5 5];%default ranges for figures associated to this mesh
bfREF = bf; % backup the base flow for Re =500, dsurD = 0.1

bf = SF_Load('BASEFLOW','last')


% plot the base flow

figure;SF_Plot(bf,'ux','contour','on','clevels',[0,0],'symmetry','XS','boundary','on','bdlabels',[1 21 22 23]);  %
pause(0.1);

%% guess values (determined at a previous stage in the exploration)
ResIguessstart = 478; omegaIguessstart = 0.647;
ResSguessstart = 313;
 
%% Loop
dsurD_tab = [0.1:0.1:0.3];%0.6 , 0.65 0.7 0.75 0.8 0.85 0.875 0.9];ResStab1 = ResS;
ResI_tab = []; 
omegaI_tab = []; 
ResS_tab = []; 
dsurDans = dsurD_tab(1);
for i=1:length(dsurD_tab)
    dsurD = dsurD_tab(i)
    % guess values for thresholds and frequency
   if(i<=2)   
        ResSguess = ResSguessstart; 
   else
        ResSguess = 2*ResS_tab(i-1)-ResS_tab(i-2); % guess is extrapolated
    end
    % stretching mesh
    Yratio = ((1/dsurD-1)/(1/dsurDans-1));
    bf = SF_MeshStretch(bf,'Yratio',Yratio,'Ymin',0.5);
    % adapting to bf/mode at guess threshold of unsteady mode
%    bf = SF_BaseFlow(bf,'Re',ResIguess);
%    [ev,em] = SF_Stability(bf,'m',1,'shift',omegaIguess*1i,'nev',10,'type','D');   
%    bf = SF_Adapt(bf,em(1));
    % Loop over Re in a range presumably containing the threshold
%    [EVI,ResI,omegasI] =
%    SF_Stability_LoopRe(bf,[5*round(ResIguess/5)+20:-10:5*round(ResIguess/5)-20],omegaIguess*1i,'plot','r');
%    Unsteady branch to be debugged...
    [EVS,ResS,omegasS] = SF_Stability_LoopRe(bf,[5*round(ResSguess/5)+20:-10:5*round(ResSguess/5)-20],0,'plot','b');
    ResS_tab(i) = ResS;
    dsurDans = dsurD;
end


%% POST-PROCESSING STAGE

clear all; close all;


workdir = './Work_L2_TEST/'; % 
SF_core_start('workdir',workdir,'verbosity',4); % reopens the database 

sfs = SF_Status('ALL'); % generates an index of the database ;

sfs.THRESHOLDS % this object contains the thresholds

% We can now plot the results 
figure(25);
plot(sfs.THRESHOLDS.dsurD,sfs.THRESHOLDS.Re,'r');
xlabel('d/D');ylabel('Re'); legend('Re_{c,s}','Re_{c,i}');
saveas(gcf,['NeutralCurves_Re_dD_BluntBondy_L2','.png'],'png')



