SF_core_start('workdir','WORK/')

%%
% Generate a mesh and a starting flow
ffmesh = SF_Mesh('Mesh_ConvectionCell.edp','Params',[4 30 5],'problemtype','2dboussinesq');

%%
% compute a "base flow" for Ra = 2000
bf = SF_BaseFlow(ffmesh,'Ra',2000,'Pr',0.03,'Qmag',0);

%%
% look for eigenmodes
[ev,em] = SF_Stability(bf,'k',0,'shift',5,'nev',10,'sort','lr');

% plot the leading eigenmode
figure; SF_Plot(em(1),'T'); hold on; SF_Plot(em(1),{'ux','uy'},'fgridparam',[40 10],'xlim',[0 4])
pause(0.1);

%%
% We construct a "guess" as a superposition of the previous bf + the
% unstable eigenmode
amp = 5; % amplitude (max. velocity) of the expected pattern. NB this coefficient has to be tuned by trial-error 
guess = SF_Add(bf,em(1),'coefs',[1 amp]); 

%%
% compute a steady state using this "guess" as a starting point
bfNL = SF_BaseFlow(guess,'Ra',2000,'Pr',0.03,'Qmag',0);

%%
% plot this nonlinear state
figure; SF_Plot(bfNL,'T'); hold on; SF_Plot(bfNL,{'ux','uy'},'fgridparam',[40 10],'xlim',[0 4])
pause(0.1);

%%
% If you want the value of the normalized heat flux and the max velocity (norm) 
% it is available in this way
bfNL.HeatFlux
bfNL.Umax

%%
% look for eigenmodes
[ev,em] = SF_Stability(bfNL,'k',0,'shift',10+10i,'nev',20,'sort','lr','plotspectrum','yes');


if(1==0)
%%
% do a loop over Ra to investigate the stability of this nonlinear
% state with respect to 2D perturbations
RaTAB = [2000 :200 : 4000]
for j=1:length(RaTAB)
    Ra = RaTAB(j);
    bfNL = SF_BaseFlow(bfNL,'Ra',Ra);
    ev = SF_Stability(bfNL,'k',0,'shift',10+10i,'nev',10,'sort','lr');
    evTAB(j) = ev(1)
end
figure;
plot(RaTAB,evTAB);
end
