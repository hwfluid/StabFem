/// 
/// file Macros_StabFem.idp
///
/// This file contains the case-dependent Macros for boundary conditions and postprocessing output.
///
/// This file can be customized according to requirements but it must contain the following macros :
///
/// boundary conditions : 
/// macro BoundaryconditionsBaseFlow(du,dp,dT,drho) 
/// macro BoundaryconditionsStability(du,us,dp,dT,drho,symmetry)
///
///
/// For post-processsing purposes :
/// macro SFWriteBaseFlow(filename,ux,uy,p,type,iter)
/// macro SFWriteMode(namefile,ux,uy,p,ev,shift,type,iter)
/// 
/// The following case is for the 2D cylinder and include the additional macros
/// required to compute lift and drag forces : Trainee(ux,uy,p,symmetry) and Portance(ux,uy,p,symmetry)
///
/// The file must also contain 

/// Solvers
load "MUMPS" 
string EigenSOLVER = "SLEPC";
IFMACRO(!EIGENSOLVER)
macro EIGENSOLVER  SLEPC //EOM
ENDIFMACRO 

/// Required storage modes 
macro STORAGEMODES() "P2,P2P2P1P1P1,P2P2P2P1P1P1" 
// EOM

int bclat=4,bcinlet=1,bcoutflow=3,bcwall=2,bcaxis=6,bclatNOP=41;

macro Uinlet(y) 1.0 //EOM
macro Vinlet(y) 0.0 //EOM
// inlet flow for constant flow



macro BoundaryconditionsBaseFlow(du,dp,dT,drho)
	           on(bcinlet,du#x=Uinlet(y)-Ubx,du#y=Vinlet(y)-Uby,drho=1.-Rhob,dT=1.-Tb)
	         + on(bcoutflow,du#x=Uinlet(y)-Ubx,du#y=0.0-Uby,drho=1.-Rhob,dT=1.-Tb) 
             + on(bcwall,du#x=Omegax*N.y-Ubx,du#y=-Omegax*N.x-Uby)
             + int1d(th,bcaxis)(du#x*us#x*1e30*(symmetryBaseFlow=="A")+du#y*us#y*1e30*(symmetryBaseFlow=="S"))       
//EOM

macro BoundaryconditionsStability(du,us,dp,dT,drho,symmetry)
	           on(bcinlet,dux#=0,du#y=0.0,dT=0.0,drho=0.0)
	         + on(bcoutflow,dux#=0,du#y=0.0,dT=0.0,drho=0.0)
             + on(bcwall,du#x=0.0,du#y=0.0)
             + int1d(th,bcaxis)(du#x*us#x*1e30*(symmetry=="A")+du#y*us#y*1e30*(symmetry=="S"))
//EOM

macro BoundaryconditionsStabilityMode3D(du,us,dp,dT,drho,symmetry)
	           on(1,dux#=0,du#y=0.0,du#z=0.0,dT=0.0,drho=0.0)
	         + on(3,dux#=0,du#y=0.0,du#z=0.0,dT=0.0,drho=0.0)
             + on(2,du#x=0.0,du#y=0.0,du#z=0.0)
             + int1d(th,6)((du#x*us#x+us#z*du#z)*1e30*(symmetry=="A")+du#y*us#y*1e30*(symmetry=="S"))
//EOM


// HERE ARE SOME MACROS WHICH ARE USEFUL FOR POST-PROCESSING WITH A 2D FLOW OVER A BLUFF BODY

macro Trainee(u,symmetry)   
	(

	int1d(th,2,21,22,23)((u#p-1)/(gamma*Ma^2)*N.x)
	-nu*int1d(th,2,21,22,23)( (((4./3.)*dx(u#x) - (2./3.)*dy(u#y))*N.x+ (dx(u#y)+dy(u#x))*N.y))
	
	)
//EOM
	

macro Portance(u,symmetry)   
	(
	(1*(symmetry==2)+2*(symmetry==0))
	*(
	int1d(th,2,21,22,23)(((u#p-1)/(gamma*Ma^2))*N.y)
	-nu*int1d(th,2,21,22,23)( (((4./3.)*dy(u#y) - (2./3.)*dx(u#x))*N.y+(dx(u#y)+dy(u#x))*N.x))
	)
	)
//EOM


macro CalcLx(Lx,u)  /* mesure de la longueur de recirculation */
 { 		real ddx = 0.001;real Uav = -2e-10;real Uap=-1e-10;int ix;
			for(ix=1;ix<20000;ix++)
			{   
				Uap =u#x(0.5+ix*ddx,0);
				if(Uap*Uav<0){break;};
				Uav = Uap;	
			}
			real xav = 0.5+(ix-1)*ddx; 
			real xap = 0.5+ix*ddx;
			Lx = xav *(Uap)/(Uap-Uav) + xap *(-Uav)/(Uap-Uav);
 }	
//EOM  


macro CalcPsi(psi,vort,rho,test)
      solve LapLace(psi,test) = int2d(th)((dx(psi)*dx(test)+dy(psi)*dy(test)))
					         - int2d(th)(rho*vort*test)
					         + on(6,2,21,22,23,psi=0)
					         + on(1,3,psi=y);
//EOM


	

//macro PostProcessBaseFlowOutput(file,u1,u2,p)
	
