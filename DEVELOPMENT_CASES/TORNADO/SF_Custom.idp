/// 
/// file SF_Custom.idp
///
/// This file defines a few options needed for you case. This file may content :
/// * Definition of solvers
/// * Definition of data storage modes used in .txt files (using a string-valued macro STORAGEMODES)
/// * the case-dependent Macros for boundary conditions and postprocessing output.
/// * miscelanous other things possibly needed by your drivers.
///
/// This program is part of the StabFem project distributed under GNU licence.
///
/// 
/// The present file is for the TORNADO CASE

load "MUMPS_seq" 


IFMACRO(!FREEFEMPLOTS)
macro FREEFEMPLOTS  NO//EOM
ENDIFMACRO 
// Set to YES to generate plots through the native FreeFem plotter ffglut (recommended in terminal /debug mode)
// set to NO to disable all plots (recommended in Matlab/StabFem mode)



IFMACRO(!ncomponentsBF)
macro ncomponentsBF 2//EOM  // THIS PARAMETERS SHOULD BE 2 if BASE FLOW is (U,V,P) and 3 if it is (U,V,W,P) (case with rotation)
ENDIFMACRO

// Required storage modes 
macro STORAGEMODES() "P2,P2P2P1,P2P2P2P1" 
// EOM

// INLET

macro PARAMRpipe() (int1d(th,3)(1))  //EOM
macro PARAMX1() (int1d(th,2,5)(x)/int1d(th,2,5)(1)) //EOM
macro PARAMHin() (int1d(th,1)(1)) //EOM
macro PARAMRcyl() (int1d(th,3)(1)) //EOM
macro PARAMHcyl() abs(int1d(th,1,23)(1)) //EOM

macro Uinlet() (0) //EOM

macro Vinlet() 
( -((x-PARAMX1)*(PARAMX1+PARAMHin-x)*4*PARAMRpipe^2/(3*PARAMRcyl*PARAMHcyl))*(int1d(th,5)(1)==0) 
  -((x-PARAMX1+PARAMHin)*(PARAMX1+PARAMHin-x)*PARAMRpipe^2/(3*PARAMRcyl*PARAMHcyl))*(int1d(th,5)(1)!=0) ) 
//EOM

macro Winlet() (-Omegax*Vinlet) //EOM

macro CalcPsi(psi,vort,test)
      solve LapLace(psi,test) = int2d(th)(1/y*(dx(psi)*dx(test)+dy(psi)*dy(test)))
					         - int2d(th)(vort*test)
					         + on(6,5,2,psi=0)
					         + on(21,22,23,psi=.5*PARAMRpipe^2);					         
//EOM


macro BoundaryconditionsBaseFlow(du,up,v)
	           on(1,du#r=Vinlet-up#r,du#x=Uinlet-up#x)
             + on(2,21,22,23,du#x=0.0,du#r=0.0)
             + on(5,du#x=0.0)+on(4,du#r=0)
             + on(6,du#r=0.0)      
			 + on(1,du#phi=Winlet-up#phi)
			 + on(2,21,22,23,du#phi=0.0) 
			 + on(6,du#phi=0.0)
//EOM


macro BoundaryconditionsStability(u,v,m)
	           on(1,2,21,22,23,u#x=0,u#r=0.0,u#phi=0.0)
	         + on(5,u#x=0.0)+on(4,u#r=0)
	         + int1d(th,6)(u#x*v#x*1e30*(abs(m)>0)+(u#r*v#r)*1e30*(abs(m)!=1))   
IFMACRO(ncomponentsBF,3)
             + on(2,21,22,23,u#phi=0.0)
             + int1d(th,6)((u#phi*v#phi)*1e30*(abs(m)!=1)) 
ENDIFMACRO
//EOM




	
