

SF_core_start('workdir','WORK/')
%%
% Generate a mesh and a starting flow
ffmesh = SF_Mesh('Mesh_TORNADO.edp','Params',[.5 10 5 5 1 8 2],'problemtype','axixrswirl');
bf = SF_BaseFlow(ffmesh,'Re',10,'Omegax',1);

%%
% progressively increase Re
bf = SF_BaseFlow(bf,'Re',50);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',500);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',2000);
bf = SF_Adapt(bf,'Hmax',.15);
bf = SF_BaseFlow(bf,'Re',3000);
bf = SF_Adapt(bf,'Hmax',.15);

%%
% plot baseflow
psilevels = [ 0 .05:.05:.2 .23 .24 .245 .25 .255 .26 .27 ]/2;
figure;SF_Plot(bf,'vortx','xlim',[-5 2],'ylim',[0 5],'cbtitle','\omega_z','colorrange',[-5 5]);
hold on;SF_Plot(bf,'psi','contour','on','clevels',psilevels,'cstyle','patchdashedneg','xystyle','off');

%%
% compute eigenmodes for m=1 and m=2 
[ev1,em1] = SF_Stability(bf,'shift',.2-.2i,'nev',20,'m',1,'sort','lr','plotspectrum','yes')
[ev2,em2] = SF_Stability(bf,'shift',.2-.2i,'nev',20,'m',2,'sort','lr','plotspectrum','yes')

%%
% plot the eigenmodes
figure;SF_Plot(em1(12),'ur','title','unstable eigenmode with m=1');
figure;SF_Plot(em2(15),'ur','title','unstable eigenmode with m=2');

%% 
% Structural sensitivity
[ev,es,ed,ea] = SF_Stability(bf,'shift',ev1(12),'nev',1,'type','S','m',1);
figure;SF_Plot(es,'sensitivity','title','sensitivity with m=1');

% Structural sensitivity
[ev,es,ed,ea] = SF_Stability(bf,'shift',ev2(15),'nev',1,'type','S','m',2);
figure;SF_Plot(es,'sensitivity','title','sensitivity with m=2');
