
%% this script creates a mirrored mesh to capture the asymetric branch
%%

% We first repeat the whole mesh adaptation procedure but using a different working directly 
addpath('../../SOURCES_MATLAB/');
%addpath('/Users/fabred/StabFem/SOURCES_MATLAB/'); % better with octave
SF_core_start('verbosity',4,'workdir','./Work_chi08_mirror/');
BETA = 0.8;

sfstat = SF_Status();
if strcmp(sfstat.status,'new')
  Xmin = -5;
  Xmax = 20;
  ffmesh = SF_Mesh('Mesh_SquareConfined.edp','Params',[BETA Xmin Xmax],'problemtype','2D');
  bf=SF_BaseFlow(ffmesh,'Re',1);
  for Re = [10 50 70 100 130 150 200]
      bf=SF_BaseFlow(bf,'Re',Re);
      bf = SF_Adapt(bf);
  end
  evguess1 = 0.149; % previously determined 
  evguess2 = 0.19701+6.15884i; % idem
  [ev1,em1] = SF_Stability(bf,'nev',1,'shift',evguess1,'type','A'); 
  [ev2,em2] = SF_Stability(bf,'nev',1,'shift',evguess2,'type','A'); 
  bf = SF_Adapt(bf,em1,em2);
  [ev1,em1] = SF_Stability(bf,'nev',1,'shift',evguess1,'type','A'); 
  [ev2,em2] = SF_Stability(bf,'nev',1,'shift',evguess2,'type','A'); 
  Mask = SF_Mask(bf.mesh,[-1 3 0 2 .1]); % to enforce grid step 0.1 in wake region
  bf = SF_Adapt(bf,em1,em2,Mask,'Hmax',5);
  % Here is the mirror mesg
  bf = SF_BaseFlow(bf,'Re',75); 
  bf = SF_Mirror(bf); % this will create a full mesh. 
else
    bf = SF_Load('lastadapted'); 
end

%Warning after this point you should not do any more adaptmesh as it will lose the symmetry of the mesh !




% plot BF (vorticity + st reamlines)
figure;
SF_Plot(bf,'vort','xlim',[-2 4],'colorrange',[-100 100]);
hold on;
SF_Plot(bf,'psi','contour','on','clevels',[ -.8 -.5 -.2 -.1 -.05 .05 0 .1 .2 .5 .8],'cstyle','patchdashedneg','xystyle','off','xlim',[-2 4]);

pause(0.1);


% with the half mesh we found a steady unstable mofe for Re<72.
% we first recover this mode for Re = 75 (the shift comes from the value found witgh he half mesh)
bf = SF_BaseFlow(bf,'Re',75); 
evguess = 0.048980; % value determined from the study using half-mesh
[ev, em] = SF_Stability(bf,'nev',1,'shift',evguess,'sym','N');
% note that here the eigenmodes are normalized using |u'(x=2,y=0)| = 1 (arbitrary but convenient)

% we catch the asymetric branch using symetric bf + steady mode as a "guess"
amp = 0.3; % this amplitude has to be determined by trial/error ; this is the most sensible part of the stuff
bfguess = SF_Add(bf,em,'coefs',[1 amp]);
bf = SF_BaseFlow(bfguess,'Re',75,'type','NEW'); 

% We now have a (slightly) asymetric base flow for Re = 75, 
% plot this BF (vorticity + streamlines)
figure;SF_Plot(bf,'vort','xlim',[-2 4],'colorrange',[-100 100]);hold on;
SF_Plot(bf,'psi','contour','on','clevels',[ -.8 -.5 -.2 -.1  0 .1 .2 .5 .8],'cstyle','patchdashedneg','xystyle','off','xlim',[-2 4]);

pause(0.1);


% Spectrum exploration
% a/ in vicinity of the unsteady mode
[evI,em] = SF_Stability(bf,'nev',10,'shift',6i,'plotspectrum','yes');
% b/ in vicinity of the steady mode
[evS,em] = SF_Stability(bf,'nev',10,'shift',0,'plotspectrum','yes');







%% Loop over Re for stability computations

% first loop to catch the stationary znd low-frequency modes (using nev = 10)
Re_TAB = [75:2.5:85 90:10:200]; % small steps to begin !
shift = 0.1;
for j= 1:length(Re_TAB)
    Re = Re_TAB(j);
    bf=SF_BaseFlow(bf,'Re',Re);
    if (abs(bf.Fy)<1e-10) warning('WARNING : Fy = 0, it seems you have lost the asymetric branch !') end;
    if (bf.iter==-1) error('WARNING : Newton diverged !');  end;  
    if(j==1) sort ='LR' ; else sort = 'cont'; end   % for first computation sort by increasing real part ; for next ones sort by continuation
    [evS,eM] = SF_Stability(bf,'nev',10,'shift',shift,'sort',sort);
    evS_TAB(:,j) = evS;
end  


% second loop to catch the high-frequency mode
shift = evI(1); % determined in precious step 
evI_TAB = zeros(size(Re_TAB));
for j = 1:length(Re_TAB)
    Re = Re_TAB(j)
    bf=SF_BaseFlow(bf,'Re',Re);
    if (bf.iter==-1) error('WARNING : Newton diverged !');  end;  
    if(j>=2) shift = 'cont'; end % switch to continuation mode 
    [evI,emI] = SF_Stability(bf,'nev',1,'shift',shift,'sort',sort);
    evI_TAB(j) = evI;
end 

%% Plot stability branches

figure(20);hold off;
for j = 1:4
    plot(Re_TAB,real(evS_TAB(j,:)),'-');hold on;
end
plot(Re_TAB,real(evI_TAB(1,:)),'--');
plot(Re_TAB,0*Re_TAB,'k:')
xlabel('Re' ); ylabel('\lambda_r')
%ylim([-.1,.35]);

figure(21);hold off;
plot(Re_TAB,abs(10*imag(evS_TAB(1,:))),'-');hold on;    
plot(Re_TAB,imag(evI_TAB(1,:)),'--'); hold on;

xlabel('Re' ); ylabel('\lambda_i ; 10 \lambda_i')
pause(0.1);




% eventually we plot the BF for Re = 200 (vorticity + streamlines)
figure;SF_Plot(bf,'vort','xlim',[-2 4],'colorrange',[-100 100]);hold on;
SF_Plot(bf,'psi','contour','on','clevels',[ -.8 -.5 -.2 -.1  0 .1 .2 .5 .8],'cstyle','patchdashedneg','xystyle','off','xlim',[-2 4]);




