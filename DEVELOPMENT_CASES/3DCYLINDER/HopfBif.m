close all;
addpath('../../SOURCES_MATLAB/');
verbosity=1;ffdatadir='./WORK/';
SF_Start; 


figureformat='png'; 
system('mkdir FIGURES');

%% Compute the neutral curve of the upper HopfBifurcation
alpha = [[5.4:-0.02:5.32]];
ResS_tab = nan*ones(size(alpha)); 
Re_RangeS = [73:0.5:73.5];
omegaS_tab = nan*ones(size(alpha));
omegaS_tab(1) = 0.1;
omegasSguess = 0.1;
k_Range = [0.00001];
kS_tab = 0.00001*ones(size(alpha));
status_tab = nan*ones(size(alpha));
Ma = 0.0;

for i=3:length(alpha)
    % Update parameter
    Omegax = alpha(i);
    % Update mesh
    [bf,EVS,ResS,omegasS,KOut,status] = SF_Stability_LoopBisRe(bf,Re_RangeS,k_Range,omegasSguess*1i,'Mach',Ma,...
                                            'Omegax',Omegax);
    % Save current values
    ResS_tab(i) = ResS;
    omegaS_tab(i) = omegasS;
    if(i<2) % guess = start value
        ResSguess = 0.9*ResS_tab(1); omegasSguess = omegaS_tab(1); kGuess = kS_tab(1);
        Re_RangeS = [ResSguess*0.95:ResSguess*.05:1.25*ResSguess];
    elseif(i<length(alpha)) % guess = extrapolation using two previous points
        ResSguess = interp1(alpha(1:i),ResS_tab(1:i),alpha(i+1),'spline','extrap');
        Re_RangeS = [ResSguess*1.02:ResSguess*0.01:1.03*ResSguess];
        % omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),alpha(i+1),'spline','extrap')
        omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),alpha(i+1),'spline','extrap');
        kGuess = interp1(alpha(1:i),kS_tab(1:i),alpha(i+1),'spline','extrap');
    end
    %k_Range = [kGuess*0.9:(kGuess*.2+0.1)/4.0:kGuess*1.1+0.1];
    k_Range = [0.00001];
end

% % bf=SF_BaseFlow(bf,'Re',73,'Omegax',5.4,'type','NEW');
% % [ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside
% % bf=SF_BaseFlow(bf,'Re',73.5,'Omegax',5.4,'type','NEW');
% % [ev2,em2] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside
% % bf=SF_BaseFlow(bf,'Re',73.8,'Omegax',5.4,'type','NEW');
% % [ev3,em3] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside
% bf=SF_BaseFlow(bf,'Re',76.9,'Omegax',5.36);
% [ev,em] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',10);
% bf=SF_BaseFlow(bf,'Re',77.3,'Omegax',5.36);
% [ev2,em2] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',3);
% bf=SF_BaseFlow(bf,'Re',77.4,'Omegax',5.36);
% [ev3,em3] = SF_Stability(bf,'k',0.00001,'shift',0.1,'nev',3);
