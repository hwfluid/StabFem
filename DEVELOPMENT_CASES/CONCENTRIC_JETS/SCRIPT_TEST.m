
run('../../SOURCES_MATLAB/SF_Start.m');
verbosity=1000;

%% first generate a mesh and base flow

ffmesh = SF_Mesh('Mesh_ConcentricJets.edp','params',[.5 1 1 50 10 1 1],'problemtype','AxiXRCOMPLEX');
% parameters are [R1, LS , D2, Xmax, Rmax Umax1 Umax2 ]
SF_Plot(ffmesh,'mesh');

bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf);


ReTab = [10 30 50 100 200 500 600 700 800 1000 1200 1400 1600]
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf);
end
Params = [4 1e30  1 0.5 20 1e30]; % Lm, LA, LC, gammac, yA, yB
ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params); 

%% Figures
figure;  SF_Plot(bf,'ux','xlim',[0 5],'ylim',[0 5]);
hold on; SF_Plot(bf,{'ux','ur'},'xlim',[0 5],'ylim',[0 5]);

%% Eigenvalues computation

% We have previously localized a mode with eigenvalue 0.016. We compute it
[ev,em] = SF_Stability(bf,'m',1,'shift',0.016,'nev',1);

% we then use the leading mode to readapt the mesh, and recompute the mode
bf = SF_Adapt(bf,em)
[ev,em] = SF_Stability(bf,'m',1,'shift',0.016,'nev',1);

% here is how to plot
plotoptions={'xlim',[0 10],'ylim',[0 5],'colorrange','cropcenter'};
figure;SF_Plot(em,'ux',plotoptions{:});

% here is how to compute 20 modes and plot in interactive mode
[ev,em] = SF_Stability(bf,'m',1,'shift',0.4+.5i,'nev',20,'plotspectrum','yes','plotmodeoptions',plotoptions)



%% then we will do a loop
Re_Range = [600 : -25 :375];guess_ev = 0.0163;
[EV,Rec,omegac] = SF_Stability_LoopRe(bf,Re_Range,guess_ev,'plot','yes');



