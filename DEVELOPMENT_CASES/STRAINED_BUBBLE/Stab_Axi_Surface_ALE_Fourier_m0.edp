// Computation of Stability properties for a STRAINED BUBBLE
//
// From Paul Bonnefis, 5/09/2017
// Incorporated in StabFem by DF, 13 july 2019, during a flight from Tokyo to Paris
//

include "Macros_StabFem.idp"
macro USETECPLOT() NO //EOM
macro POSTPROCESSPAUL YES //EOM
macro POSTPROCESSSTABFEM YES //EOM



	verbosity = 0;
	load "PETSc-complex"                             // PETSc plugin
	load "SLEPc-complex"                             // SLEPc plugin
	load "MUMPS"	
	int WAIT;
	int dataTest;


cout <<"       ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl;
cout <<"         Stability_bubble_UniaxialStraining_Fourier_Oh-We_m0.edp  "<<endl;
cout <<"       ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl<<endl;

//	cout <<"Enter 1 if you want to wait for the plot or 0 if not :"<<endl;
//	cin  >> WAIT;
	WAIT = 0.;

int isSTABFEM;
	cout <<"Enter 1 if you are using StabFem and 0 instead "<<endl;
cin  >> isSTABFEM;

	// window plot
	real a = 0.;
	real b = -0.05;
	real d = 4.;
	real c = 4.;
//	real a = 0.;
//	real b = -0.05;
//	real d = 20.;
//	real c = 20.;

	int m;
	cout <<"Enter m :";
	cin  >> m;
	cout << endl;
	
	//equatorial plane
	int s;
	cout <<"Enter 1 for symmetry and -1 for antisymmetry in the equatorial plane :";
	cin  >> s;
	cout << endl;
	string eqplane; 
	if (s==1)  {eqplane="symmetry";}
	if (s==-1) {eqplane="antisymmetry";}	 

	//Physical settings
	real Oh,We;
	cout <<"Enter Oh and We :";
	cin  >> Oh;
	cin  >> We;
	cout << endl;


	// warning, ARPACK may not work if nev is too big (>5-6)
	int nev;
	cout << "Enter the number of eigenvalue you want to find (don't ask too many) : "; 
	cin  >> nev; 
	cout << endl;

	real sigmar, sigmai;
	cout << "Enter the Re(shift) and Im(shift) : " ; 
	cin  >> sigmar >> sigmai; 
	cout << endl;
	
	complex sigma=sigmar+1i*sigmai;
	
	int NsF ; 
	cout << " Enter NsF (dimension of the Fourier series ) : " ; 
	cin  >> NsF ; 
	cout << endl;
	


	//Physical settings
	real gamma = 1.;
	real rho   = 1.;
	real R     = 1.;
	real S     = (We*gamma/(2*rho*R^3))^.5; // We = rho*(S*R)^2*2R/gamma  
	real nu    = Oh*(2*gamma*R/rho)^.5;     // Oh = (rho/(gamma*2*r))^.5*nu
	real Re    = S*R*2*R/nu;                // Re=2R*U/nu


	cout << endl;
	cout << "* m  = " << m  << endl;  
//	cout << "* s  = " << s  << "  ("+eqplane+")" << endl;  
	cout << "* "+eqplane << endl;  
	cout << "* Oh = " << Oh << endl;	
	cout << "* We = " << We << endl;	
	cout << "* Re = " << Re << endl;	
	cout << endl;
	cout << "* S     = " << S     << endl;	
	cout << "* nu    = " << nu    << endl;	
	cout << "* gamma = " << gamma << endl << endl;	


string OUTdirectory,INdirectory,MESH,BFlow; 
	//Output OUTdirectory and files
	if (isSTABFEM==0)
	{
	OUTdirectory = "MODES_Fourier";
	INdirectory = "../CHBASE3D_Fourier/";
//	string INdirectory = "../BASE_Flow_quadratic_convergence/CHBASE3D_Fourier/";
//	string INdirectory = "../CHBASE3D_Fourier_converged/";
	MESH  = INdirectory+"Fluid_Oh"+Oh+"We"+We+".msh";
	BFlow = INdirectory+"cbn_Oh"+Oh+"We"+We+"_uniaxialStraining.txt";
	exec("mkdir "+OUTdirectory);
	}
	else
	{
	OUTdirectory = ffdatadir;
	INdirectory = ffdatadir;
	MESH  = INdirectory+"mesh.msh";
	BFlow = INdirectory+"BaseFlow.txt";
	}
	
	cout <<"Modes saved in the directory : "+OUTdirectory << endl;
	cout <<"Mesh :      "+MESH<<endl;
	cout <<"Base Flow : "+BFlow<<endl<<endl;



//////////////////////////////////////////////////////////////////////////////////////////////
// Building Mesh
mesh th;

if (We==0)
{
	int d = 10;
	int db = d*5;
	real Roo=10;	
	real Lsurf = pi/2;
	real ee = 1.9;
	real cc = 1; 
	real R  = 1.; 
	real R8y=1;
	
	border AxeVertical	(t=(R8y*Roo-R)^(1/ee),0){y=t^ee+R;	x=0;		label=1;};	
	border SurfBulle	(t=pi/2,0)	    	{y=sin(t); 	x=cos(t);	label=2;};
	border AxeHorizontal	(t=0,(Roo-R)^(1/ee))	{y=0; 		x=(t)^ee+R;	label=3;};
	border Surfinfty	(t=pi/2,0)	    	{y=Roo*sin(t);	x=cos(t)*Roo;	label=8;};
	border SurfBulle2	(t=pi/2-0.01,0.01)	{y=2.5*sin(t);	x=2.5*cos(t);	label=100;};


	th = buildmesh(
		SurfBulle(Lsurf*db)
	+	SurfBulle2(Lsurf*d*2.5)
	+	AxeVertical((Roo-R)*d/cc)
	+	Surfinfty(-Lsurf*d*3)					
	+	AxeHorizontal((Roo-R)*d/cc)
	);
	if(isSTABFEM==0)
		{
			savemesh(th,"3dAxiMeshBubble_quiescent_Oh"+Oh+".msh");
		 	MESH  = "3dAxiMeshBubble_quiescent_Oh"+Oh+".msh";
		}
}

else {th=readmesh(MESH);}

plot(th);

////////////////////////////////////////////////////////////////////////////////
////////// Building Matices
/////

	///////////////////////////////////////////////
	/// Options available
	
	// Neumann or Dirichlet condition (i.e. slip of penalisation), Dirichlet seems fine
	string BCequator;
	BCequator="Dirichlet";		// Dirichlet condition
//	BCequator="Neumann";		// Neumann   condition	
	
	// Inner pressure of the bubble
	string wPb;	
	if(m==0 && s==1){wPb="yes";}	// only case where the symmetries don't garantee Vb=constant
	else    	{wPb="no";}	// symmetries garantee the volume conservation

	// type of arbitrary mesh displacement
	string MD="";
	if (We!=0) 	
	{
		cout << "Enter the type of mesh displacement : ";
		cin >> MD;
		cout << endl << endl;
		if( MD!="laplacian" && MD!="relaxed" && MD!="elasticity")
		{ 
			MD="laplacian"; 
			cout << endl << endl << "Wrong argument for the Mesh displacement !!!!" << endl;
			cout << " >> Laplacian displacement is chosen by default" << endl << endl;
		}		
	}


	cout <<"Matrices File : ";
	cout <<"Matrices_ALEdomain_m0.edp"<<endl<<endl;	
	include "Matrices_ALEdomain_m0.idp";		//ETA =P2xP2	
//	cout <<"Matrices_ALEdomain_m.idp"<<endl<<endl; 	
//	include "Matrices_ALEdomain_m.idp";		//ETA =P2xP2 
		
////////////////////////////////////////////////////////////////////////////////
////////// Computing eigenvalues with SLEPc
/////

if (nev!=0)
{
	//FE-function for the modes
	createUPvectorComplex(u,p,nev)		// ur  ur  p	// ALE
	createUPvectorComplex(uE,pE,nev)	// uEr uEz pE	// Eulerian quantities
	createUPvector(uR,pR,nev)		// uRr uRz pR	// Eulerian quantities
	createUPvector(uI,pI,nev)		// uIr uIz pI	// Eulerian quantities

	createETAvectorcomplex(Eta,nev)		// Etar  Etaz
	createETAvector(EtaR,nev)		// EtaRr EtaRz
	createETAvector(EtaI,nev)		// EtaIr EtaIz

	ETA<complex>[int] eta(nev);
	ETA[int] etaR(nev),etaI(nev);	

	K<complex>[int] K1(nev),N1(nev);
	K[int] k1R(nev),k1I(nev);	

	Sn<complex>[int] sign(nev);


	complex[int,int] Xeta(NsF,nev);
	complex[int,int] Xk1(NsF,nev);
	complex[int,int] Xsign(nP1axis,nev);
	complex[int,int] EigenVEC(OP.n,nev);
	complex[int] EigenVAL(nev);
	complex[int] pb(nev);

	//////////////////////////////////////////////////////
	////////// SLEPc
	/////
	cout << endl;
	cout << "SLEPc : Beginning the computation of "<<nev<<" eigenvalue around sigma = "<< sigma << "..."<< endl;
	cout.default <<endl;
	cout <<"* equatorial plane : "+eqplane    << endl;  
	cout <<"* m     = "<< m << endl; 	
	cout <<"* Oh    = "<< Oh << endl; 		
	cout <<"* We    = "<< We << endl;
	cout <<"* Re    = "<< Re << endl; 				
	cout <<"* Shift = "<< sigmar <<" + "<< sigmai <<" i"<< endl; 
	cout << endl;

	int[int] arrayIntersection(0);              // ranks of neighboring subdomains
	int[int][int] restrictionIntersection(0);   // local-to-neighbors renumbering
	real[int] D1(A.n);                           // partition of unity
	D1.resize(A.n);
	D1 = 1;

//	Mat<complex> DA(A, arrayIntersection, restrictionIntersection, D1);macro correctionEVal 0//EOM
	Mat<complex> DA(OP, arrayIntersection, restrictionIntersection, D1);
	macro correctionEVal sigma//EOM
	Mat<complex> DB(DA,B);

	complex sigmaSLEPc = 0.0001 + 1i*0.01;

	// Parameters for the distributed EigenValue solver
	string ssparams =            	
       " -eps_nev " + nev             	// Number of eigenvalues
      +" -eps_target " + sigmaSLEPc    	// Shift >> looking for the smallest eigenvalue of OP
//    +" -eps_smallest_real "   	// Shift option
      +" -eps_smallest_magnitude "   	// Shift option
//    +" -eps_smallest_imaginary "   	// Shift option
//    +" -eps_largest_real "   		// Shift option      
//    +" -eps_harmonic "             
//    +" -eps_largest_magnitude "    	// Shift option	  
//    +" -eps_gen_hermitian"         	// The problem is symmetric( A^T=A) or Hermitian (A*=A)
//    +" -eps_pos_gen_non_hermitian"    // The problem is not hermitian (A*!=A) with positive (semi-)definite B matrix
      +" -eps_gen_non_hermitian"        // The problem is not hermitian (A*!=A) by default
      +" -eps_type krylovschur "     	// Type of Eigen Problem Solver 
//    +" -eps_type arnoldi "         	// Type of Eigen Problem Solver 
      +" -st_type sinvert "          	// Spectral Transformation
      +" -st_pc_type lu "            	// Solver for the linear systems
//    +" -st_pc_type gamg "          	// Solver for the linear systems
//    +" -st_pc_type sor "           	// Solver for the linear systems
      +" -st_pc_factor_mat_solver_type mumps "
      
//    +" -eps_view " 
//    +" -eps_conv_abs "
      ;

	int k;
	real timeEig = clock();
	cout << "* Entering Slepc Solver" << endl; 
	{
	k = EPSSolve
	(
	 DA,              	// matrix OP = A − sigma*B
	 DB,              	// B matrix
	 array   = EigenVEC, 	// Array to store the FEM-EigenFunctions
	 values  = EigenVAL, 	// Array to store the EigenValues
	 sparams = ssparams  	// Parameters for the distributed EigenValue solver
	);
	}
	timeEig = clock()-timeEig; //doesn't seem to be right

	//////////////////////////////////////////////////////

	cout <<"Eigenvalues found : "<< k <<" in "<< timeEig <<" s"<< endl;
	cout <<"------------------------------------------"<< endl;
	
	string spectrumfilename ; 
	if(isSTABFEM)
		{spectrumfilename = ffdatadir+"Spectrum.txt";}
	else
		{spectrumfilename = "spectrum.txt";};		
	ofstream fspec(spectrumfilename);//,append);
	
	for (int i=0; i<nev; i++)
		{
		EigenVAL[i]+=correctionEVal; // adding the value of the shift
		real evr = real(EigenVAL[i]);
		real evi = imag(EigenVAL[i]);		
		int nold=cout.precision(9);
		cout.default << i <<"  ";
		cout.showpos.fixed <<  evr <<"  "<< evi <<"i      "<<endl;

		string TAG = "_Oh"+Oh+"_We"+We+"_m"+m+"_s"+s+"_lambda"+evr+"_"+evi+"i_"+MD;
		

		int nold2=fspec.precision(16);
		fspec.fixed <<  evr <<"  "<< evi <<" ";
		fspec.default << Oh <<" "<< We <<" "<< m <<" "<< s <<" "<< Nv <<" ";
	//	fspec.default << NsF <<" "<< Lx <<" "<< Ly <<" "<< sigma << " ";
	//	fspec.default << "SLEPc ";	
	//	fspec.default << "BCequator="+BCequator <<" ";
	//	fspec.default << "MD=" << MD <<" ";
	//	fspec.default << TAG ;	
		fspec.default << endl;
		}				
	cout <<"------------------------------------------"<<endl<<endl;
	
////////////////////////////////////////////////////////////////////////////////
////////// Processing the results and outputs
/////
	for (int i=0; i<nev; i++)
	{

		real evr = real(EigenVAL[i]);
		real evi = imag(EigenVAL[i]);
		complex ev = EigenVAL[i];
		complex[int] eVi = EigenVEC(:,i);

		if( We!=0 && BCequator=="Neumann"   && wPb=="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Xsign(:,i) , Etar[i][] , pb(i) ] = EigenVEC(:,i); }
		if( We!=0 && BCequator=="Neumann"   && wPb!="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Xsign(:,i) , Etar[i][] ] = EigenVEC(:,i); }
		if( We!=0 && BCequator=="Dirichlet" && wPb=="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Etar[i][] , pb(i) ] = EigenVEC(:,i); }
		if( We!=0 && BCequator=="Dirichlet" && wPb!="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Etar[i][] ] = EigenVEC(:,i); }
	
		if( We==0 && BCequator=="Neumann"   && wPb=="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Xsign(:,i), pb(i) ] = EigenVEC(:,i); }
		if( We==0 && BCequator=="Neumann"   && wPb!="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), Xsign(:,i) ] = EigenVEC(:,i); }
		if( We==0 && BCequator=="Dirichlet" && wPb=="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i), pb(i)] = EigenVEC(:,i); }
		if( We==0 && BCequator=="Dirichlet" && wPb!="yes" ){ [ ur[i][], Xeta(:,i), Xk1(:,i)] = EigenVEC(:,i); }

		for(int ik=0;ik<NsF;ik++)
			{
			 eta[i][] += Xeta(ik,i)*FK[ik][];
			 K1[i][]  += Xk1(ik,i)*FK[ik][];
			 N1[i][]  -= Xeta(ik,i)*FKP[ik][];				 
			};

		// Normalisation of the modes
		complex norm;
		{
		ETA<complex> etaTEMP=eta[i];
		real[int] Xx = etaTEMP[].re;
		real[int] Yy = etaTEMP[].im;
		real[int] etaRAD  = Xx.*Xx;
		 	  etaRAD += Yy.*Yy;
		etaRAD = sqrt(etaRAD);
		int Imax = etaRAD.imax;
		norm =  2.*etaTEMP[][Imax]/1i;
		}

		[vectors(u,i),p[i]]=NormUPvector(u,p,norm,i);	

		[vectorsETA(Eta,i)]=NormETAvector(Eta,norm,i);		// Need to normalize before converting in Eulerian	
		[vectorsETA(EtaR,i)]=ImRePartETA(real,Eta,i);		
		[vectorsETA(EtaI,i)]=ImRePartETA(imag,Eta,i);		

		[vectors(uE,i),pE[i]]=ConvertInEulerian(u,p,U,Eta,i);	// u_Euler = u_ALE + eta.grad(U_base)
		[vectors(uR,i),pR[i]]=ImRePartUP(real,uE,p,i);		
		[vectors(uI,i),pI[i]]=ImRePartUP(imag,uE,p,i);		
		
	 	eta[i] = eta[i]/norm;
		etaR[i] = real(eta[i]);
		etaI[i] = imag(eta[i]);

	 	K1[i] = K1[i]/norm;
		k1R[i] = real(K1[i]);
		k1I[i] = imag(K1[i]);

	 	N1[i] = N1[i]/norm;

		pb(i) = pb(i)/norm;
		if(wPb=="yes")
		{
		cout << "pb("+i+") = "+ pb(i) << endl;
		}

		if( BCequator=="Neumann" )
		{
	 	sign[i][] = MatPaxis'*Xsign(:,i);				
		sign[i][] /= norm;
		}
	
	}


////////////////////////////////////////////////////////////////////////////////
////////// Diplaying the results
/////
//	cout << endl;

	IFMACRO(dim,3)
	macro PlotAzimutalU
	plot(uRt[i], wait=WAIT, value=1, cmm="mode "+i+"   real ut',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
	plot(uIt[i], wait=WAIT, value=1, cmm="mode "+i+"   imag ut',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);//EOM
	ENDIFMACRO

	IFMACRO(dimETA,3)
	macro PlotAzimutalETA 
	plot(EtaRt[i], wait=WAIT, value=1, cmm="mode "+i+"   real Eta Azimutal',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
	plot(EtaIt[i], wait=WAIT, value=1, cmm="mode "+i+"   imag Eta Azimutal',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);//EOM
	ENDIFMACRO

	IFMACRO(dim,2)
	macro PlotAzimutalU cout<<" ";//EOM
	ENDIFMACRO

	IFMACRO(dimETA,2)
	macro PlotAzimutalETA cout<<" ";//EOM
	ENDIFMACRO

	for (int i=0; i<nev; i++)
	{
		real evr = real(EigenVAL[i]);
		real evi = imag(EigenVAL[i]);
	//	plot([urr[i], uzr[i]], wait=WAIT, value=1, cmm=i+"   real velocity, mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
	//	plot([uri[i], uzi[i]], wait=WAIT, value=1, cmm=i+"   imag velocity, mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);

	//	plot(pr[i], value=1, wait=WAIT, cmm=i+"   real pressure mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
	//	plot(pi[i], value=1, wait=WAIT, cmm=i+"   imag pressure mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);

	//	plot(FK[NsF-1], wait=WAIT, value=1, cmm="mode "+i+"  highest Fourier function,  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
	//	plot(etaR[i], wait=WAIT, value=1, cmm="mode "+i+"  highest Fourier function,  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);

//		plot(URr, wait=WAIT, value=1, cmm="mode "+i+"   real ur ALE',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
//		plot(URz, wait=WAIT, value=1, cmm="mode "+i+"   real uz ALE',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);


		plot(p[i],  wait=WAIT, value=1, cmm="mode "+i+"   real p',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
//		plot(pi[i],  wait=WAIT, value=1, cmm="mode "+i+"   imag p',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);		
//		plot(uri[i], wait=WAIT, value=1, cmm="mode "+i+"   imag ur',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
//		plot(uzi[i], wait=WAIT, value=1, cmm="mode "+i+"   imag uz',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
		plot(uRr[i], wait=WAIT, value=1, cmm="mode "+i+"   real ur',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
		plot(uRz[i], wait=WAIT, value=1, cmm="mode "+i+"   real uz',  lambda = "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);

		PlotAzimutalU;

	if (MD!="")
		{  	
		plot([EtaRr[i], EtaRz[i]], wait=WAIT, value=1, cmm=i+"   real mesh velocity, mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
		plot([EtaIr[i], EtaIz[i]], wait=WAIT, value=1, cmm=i+"   imag mesh velocity, mode no"+i+" "+evr+" + "+evi+"i", bb=[[a,b],[c,d]]);
		}

		PlotAzimutalETA;
	}

////////////////////////////////////////////////////////////////////////////////
////////// Processing the results and outputs
/////

	IFMACRO(dim,2)
	macro TecMode() "tecMode_m0.edp"//EOM
	ENDIFMACRO

	IFMACRO(dim,3)
	macro TecMode() "tecMode.edp"//EOM
	ENDIFMACRO

//	cout << endl;
	string answer1="y";
	cout << "Save the data (shape, interface data, ...) ? (y/N)" <<endl;
//	cin  >> answer1;	
	if (isSTABFEM==1) {answer1="n";};

	string answer2="y";
	cout << "Save the modes ? (y/N)" <<endl;
//	cin  >> answer2;
	if (isSTABFEM==1) {answer2="n";};
	cout << endl;
	
	//Loop on the differents modes
	for (int i=0; i<nev; i++)
	{
		real evr = real(EigenVAL[i]);
		real evi = imag(EigenVAL[i]);

		// Axis, shape and interface data
		if(answer1=="y")
		{
			//if(m==0){ GetAxisOzDATA; }
			//else    { GetAxisRZDATA; };
			GetInterfaceDATA;
			Get3Dshape(m,s,50);
			//GetFourierSpectrumEtaAndK1;
			string interfaceData = "interface_Oh"+Oh+"_We"+We+"_m"+m+"_s"+s+"_lambda"+evr+"_"+evi+"i_"+MD+".txt";
			string ShapeData = "Shape3D_Oh"+Oh+"_We"+We+"_m"+m+"_s"+s+"_lambda"+evr+"_"+evi+"i_"+MD+".txt";
			
			exec("mv mode_interface"+i+".txt "+OUTdirectory+"/"+interfaceData);
			exec("mv Shape3D_"+i+".txt "+OUTdirectory+"/"+ShapeData);
		}
		//cout << endl;
		// Base flow field data
		if(answer2=="y")
		{
			string MODEtxt = "mode_Oh"+Oh+"_We"+We+"_m"+m+"_s"+s+"_lambda"+evr+"_"+evi+"i_"+MD+".txt";
			string MODEdat = "mode_Oh"+Oh+"_We"+We+"_m"+m+"_s"+s+"_lambda"+evr+"_"+evi+"i_"+MD+".dat";

			Ur[]=0;
			[vector(U),P]=[vectors(uE,i),pE[i]];
			ofstream f(MODEtxt);
			int np=f.precision(16);
			 f << Ur[]	<< endl
			   << pb(i)    	<< endl 			  
			   << Oh    	<< endl 
		 	   << We	<< endl 
			   << Re	<< endl
			   << Pb    	<< endl 
		 	   << Nv    	<< endl 
		 	   << Nsurface	<< endl 	 
		 	   << Rx   	<< endl 
		 	   << Ry   	<< endl 
		 	   << Volume	<< endl 
		  	   << Area  	<< endl 
		 	   << Chi  	<< endl 	
			   << m		<< endl 
			   << s		<< endl 	 
			   << MD	<< endl 	 	 
			   << evr	<< endl 
			   << evi	<< endl 
			   << NsF	<< endl;
//IFMACRO(USETECPLOT,YES)
//			exec("echo "+MESH+" "+MODEtxt+" "+MODEdat+" | FreeFem++ -v 0 "+TecMode);	
//ENDIFMACFRO
			exec("mv "+MODEdat+" "+MODEtxt+" "+OUTdirectory);
			cout << "Mode "+MODEtxt+"... saved" << endl << endl;;
		}


IFMACRO(!SFWriteMode)	
macro SFWriteMode(namefile,u,ev,shift,m,typeFlow,iter)
		 {
		 ofstream file(namefile);
   		 fespace p1forff2m(th,P1); 
    	 p1forff2m<complex> vort1;  		 
 		 file << "### Data generated by Freefem++ ; " << endl;
    	 file << " Eigenmode for ALE Free surface flow (written with generic macro)" << endl;
    	 file << "datatype " << typeFlow << "  datastoragemode CxP2P2P1.2 datadescriptors ur,uz,p,pb,lambda" << endl;
	     file << " real* Oh real* We int* m int* sym complex* eigenvalue ";
	     file << " complex shift int iter P1c vort1 P1surfc eta P1surfc k1"  << endl << endl ; 
		 file << Oh << endl << We << endl << m << endl << s << endl 
		 	  << real(ev) << endl << imag(ev) << endl ; 
		 file << real(shift) << endl << imag(shift) << endl << iter << endl << endl;
		 vort1=-dy(u#r)+dx(u#z);
		 for (int j=0;j<vort1[].n ; j++) 
		 	{file << real(vort1[][j]) << " " << imag(vort1[][j]) << endl;};
		 file << endl;
		 for(int ksurf=0;ksurf<Nsurface+1;ksurf++) 
	     { 
	       file << real(UUeta(xPointSurf(ksurf), yPointSurf(ksurf) )) << endl;
	       file << imag(UUeta(xPointSurf(ksurf), yPointSurf(ksurf) )) << endl; 
   	     };
		file << endl;
		for(int ksurf=0;ksurf<Nsurface+1;ksurf++) 
	     { 
	       file << real(UUk1(xPointSurf(ksurf), yPointSurf(ksurf) )) << endl;
	       file << imag(UUk1(xPointSurf(ksurf), yPointSurf(ksurf) )) << endl; 
   	     };		
		};
//EOM	
ENDIFMACRO

		
		if (isSTABFEM==1)
			{
				string namefile=ffdatadir+"Eigenmode"+(i+1);
				UP<complex>  [UUr,UUz,UUp] = [vectors(uE,i),pE[i]];
				ETA<complex> UUeta = eta[i];
				K<complex> 	 UUk1 = K1[i];
				SFWriteMode(namefile+".ff2m",UU,EigenVAL[i],sigma,m,"EigenModeD",1);	 
				// MACRO DEFINED in SF_Custom.idp
				ofstream f(namefile+".txt");
				int np=f.precision(16);
			 	f << UUr[]	<< endl
			   	  << pb(i)  << endl
			   	  << EigenVAL[i]     << endl; 
; 
			};
//			cout << endl;
			

	}

	}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//										//
//				END OF SCRIPT					//		
//										//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////// Computing eigenvalues - SHIFT AND INVERT Algo
/////
/*
cout << endl;
string question;
cout << "Try with a SHIFT AND INVERT Algo ? (y/n) " << endl;
cin  >> question;

if(question == "y")
{
	real timeSinvert = clock();
	
	//Velocity - pressure
	UP<complex> [dur0,duz0,dp0];
	//Deformation
	ETA<complex> eta, eta0;
	//Curvature
	K<complex> k1, k10;
	//Normal Stress
	Sn<complex> dsign0;
	//Mesh velocity
	ETAf<complex> [etar0,etat0,etaz0];

	cout.default << endl;
	cout << "SHIFT and INVERT : Beginning eigenvalue computation around sigma = "<< sigma << "..."<< endl;

	[dur0,dut0,duz0,dp0] = [1/(x^2+y^2),1/(x^2+y^2),0,0];
	complex[int] Xeta(NsF),Xeta0(NsF),Xk1(NsF),Xk10(NsF),Xsign(nP1axis),Xsign0(nP1axis);
	Xeta0=0;
	Xk10=0;
	Xsign0=0;

	complex lambda,lambda0;
	real error = 1.;

	set(OP,solver=sparsesolver,tgv=TGV);


	int i=0;
	while(error>1e-11 && i < 200)
	{
		// Shift and invert algo
		complex[int] X(OP.n),X0(OP.n);

	if(BCequator=="Dirichlet" && We==0) {X0=[dur0[],Xeta0,Xk10];}
	if(BCequator=="Neumann"   && We==0) {X0=[dur0[],Xeta0,Xk10,Xsign0];}
	if(BCequator=="Dirichlet" && We!=0) {X0=[dur0[],Xeta0,Xk10,etar0[]];}
	if(BCequator=="Neumann"   && We!=0) {X0=[dur0[],Xeta0,Xk10,Xsign0,etar0[]];}	
	
		complex[int] BX0 = B*X0;

			X=OP^(-1)*BX0;

	if(BCequator=="Dirichlet" && We==0) {[dur[],Xeta,Xk1]=X;}
	if(BCequator=="Neumann"   && We==0) {[dur[],Xeta,Xk1,Xsign]=X;}
	if(BCequator=="Dirichlet" && We!=0) {[dur[],Xeta,Xk1,etar[]]=X;}
	if(BCequator=="Neumann"   && We!=0) {[dur[],Xeta,Xk1,Xsign,etar[]]=X;}
	
		for(int ik=0;ik<NsF;ik++)
	  	{
			eta[] += Xeta(ik)*FK[ik][];
			k1[]  += Xk1(ik)*FK[ik][];
		};

		complex dlambda = int2d(th)((dur0*dur0+duz0*duz0)*r)/int2d(th)((dur*dur0+duz*duz0)*r);

		lambda = sigma + dlambda;

		error = abs(lambda-lambda0);

		int nold=cout.precision(9);
		cout.default <<" Iter "<< i <<"  ";
		cout.showpos.fixed <<  real(lambda) <<"  "<< imag(lambda) <<"i";
		cout.scientific << "       error = "<< error << endl;

		complex norm = (int1d(th,2)( (dur*conj(dur)+duz*conj(duz)+dp*conj(dp)+eta*conj(eta)+k1*conj(k1))*r )/int1d(th,2)(x) )^.5;

		[dur0,dut0,duz0,dp0]=[dur/norm,dut/norm,duz/norm,dp/norm];
		[etar0,etat0,etaz0]=[etar/norm,etat/norm,etaz/norm];		
		Xeta0 = Xeta/norm;
		lambda0=lambda;
		eta0=eta/norm;
		k10=k1/norm;
		dsign0=dsign/norm;

		plot(dur0, value=1, cmm=i+"  dur", bb=[[a,b],[c,d]]);
		i++;
	}

	timeSinvert = clock()-timeSinvert;
	cout.default <<"Eigenvalue found in "<< timeSinvert <<" s"<< endl;
	////////////////////////////////////////////////////////////////////////////////
	////////// Processing Data
	/////
	

	// Normalisation of the modes
	complex Norm;
	{
	real[int] Xx = eta[].re;
	real[int] Yy = eta[].im;
	real[int] etaRAD  = Xx.*Xx;
	 	  etaRAD += Yy.*Yy;
	etaRAD = sqrt(etaRAD);
	int Imax = etaRAD.imax;
	Norm =  eta[][Imax]/1i;
	}


	UP<complex> [ur,ut,uz,p] = [dur,dut,duz,dp];
	UP [urr,utr,uzr,pr]=[real(dur/Norm),real(dut/Norm),real(duz/Norm),real(dp/Norm)];	
	UP [uri,uti,uzi,pi]=[imag(dur/Norm),imag(dut/Norm),imag(duz/Norm),imag(dp/Norm)];
	ETA etaR = real(eta/Norm);
	ETA etaI = imag(eta/Norm);
	K k1R = real(k1/Norm);
	K k1I = imag(k1/Norm);

	real evr = real(lambda);
	real evi = imag(lambda);

	{
		ofstream fspec("spectrum.txt",append);
		int nold2=fspec.precision(16);
		fspec.scientific <<  evr <<"  "<< evi <<" ";
		fspec.default << Oh <<" "<< We <<" "<< m <<" "<< s <<" "<< Nv <<" ";
		fspec.default << NsF <<" "<< Lx <<" "<< Ly <<" "<< sigma << " ";
		fspec.default << "SHIFTandINVERT ";		
		fspec.default << MD;
		fspec.default << endl;
	}
	///////////////////////////////////////////////////////////
	// INTERFACE DATA >> mode_interface.txt 
		{
			femp1<complex> drur = dr(ur);
			femp1<complex> druz = dz(ur);
			femp1<complex> dzur = dr(uz);
			femp1<complex> dzuz = dz(uz);

		 	ofstream f("mode_interface.txt");
			for (int k = 0; k<=Nsurface; k++)
				{
				int I = IndexPointSurf(k);
				real s0 = S0PointSurf(k)/S0PointSurf(Nsurface);
				real xx = xPointSurf(k);
				real yy = yPointSurf(k);
				real unr = urr(xx,yy)*N0x[][I]+uzr(xx,yy)*N0y[][I];		
				real uni = uri(xx,yy)*N0x[][I]+uzi(xx,yy)*N0y[][I];		
				real utr = urr(xx,yy)*T0x[][I]+uzr(xx,yy)*T0y[][I];		
				real uti = uri(xx,yy)*T0x[][I]+uzi(xx,yy)*T0y[][I];		

				complex sigmaN = -dp(xx,yy)
				  +nu*( 2*drur(xx,yy)*n0x(k)^2+2*dzuz(xx,yy)*n0y(k)^2
					   +2*(druz(xx,yy)+dzur(xx,yy))*n0x(k)*n0y(k)      );

				complex bilanN = sigmaN-gamma*(k10[][I]);		

				f << s0	     		<<" "	 					// 1
				  << etaR[][I]  	<<" "<< etaI[][I]  	<<" "	// 2  3 
				  << k1R[][I]		<<" "<< k1I[][I]	<<" "	// 4  5 
				  << pr(xx,yy) 		<<" "<< pi(xx,yy)	<<" "	// 6  7 
				  << unr          	<<" "<< uni        	<<" "	// 8  9  
				  << utr          	<<" "<< uti        	<<" "	// 10 11
				  << urr(xx,yy) 	<<" "<< uri(xx,yy)	<<" "	// 12 13
				  << uzr(xx,yy) 	<<" "<< uzi(xx,yy)	<<" "	// 14 15
				  << real(sigmaN) 	<<" "<< imag(sigmaN)<<" "	// 16 17
				  << real(bilanN) 	<<" "<< imag(bilanN)<<" "	// 18 19
				  << xx           	<<" "<< yy          <<" "	// 20 21
				  << N0x[][I]      	<<" "<< N0y[][I]    <<" "	// 22 23
				  << evr          	<<" "<< evi         <<" "	// 24 25
				  << endl ;
				}

				f  << endl ;
	
		};

	plot([urr,uzr], value=1, wait=WAIT, cmm="velocity real", bb=[[a,b],[c,d]]);
	plot([uri,uzi], value=1, wait=WAIT, cmm="velocity imag", bb=[[a,b],[c,d]]);
	plot(pr,  value=1, wait=WAIT, cmm="pr", bb=[[a,b],[c,d]]);
	plot(pi,  value=1, wait=WAIT, cmm="pi", bb=[[a,b],[c,d]]);
	plot(urr, value=1, wait=WAIT, cmm="real ur", bb=[[a,b],[c,d]]);
	plot(uri, value=1, wait=WAIT, cmm="imag ur", bb=[[a,b],[c,d]]);
	plot(utr, value=1, wait=WAIT, cmm="real ut", bb=[[a,b],[c,d]]);
	plot(uti, value=1, wait=WAIT, cmm="imag ut", bb=[[a,b],[c,d]]);
	plot(uzr, value=1, wait=WAIT, cmm="real uz", bb=[[a,b],[c,d]]);
	plot(uzi, value=1, wait=WAIT, cmm="imag uz", bb=[[a,b],[c,d]]);

	

}
*/

