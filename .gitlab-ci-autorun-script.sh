#!/bin/bash

# ================================
# == Defining FreeFem++ command ==
# ================================
if [[  ! "$CI_RUNNER_TAGS" == *"prodcom"* ]]; then
  echo "TAG must contain 'prodcom'"
  exit 1
fi

if [[ "$CI_RUNNER_TAGS" == *"u1604"* ]]; then
    source /PRODCOM/bin/config.sh
    module load mpich/3.2.0/gcc-5.4
    module load freefem/3.61-1/gcc-5.4_mpich-3.2
    export FFBASEDIR="/PRODCOM/Ubuntu16.04/freefem/3.61-1/gcc-5.4-mpich-3.2"
    
elif [[ "$CI_RUNNER_TAGS" == *"u1804"* ]]; then
    source /PRODCOM/bin/config.sh
    module load Collection/Gnu-MPICH
    module load freefem++/3.61-1-gcc-7.3.0-mpich-3.2.1
    
    if [[ -z "${SF_AR_FFBASEDIR}" ]]; then
      export FFBASEDIR=${EBROOTFREEFEMPLUSPLUS}
    else
      export FFBASEDIR=${SF_AR_FFBASEDIR}
    fi
else
    echo "Ubuntu version should be given in gitlab-runner tags."
    exit 1
fi

if [ ! -f "${FFBASEDIR}/bin/FreeFem++" ]; then
  echo "Could not find FreeFem++ executable. Exiting..."
  exit 1
fi

# =============================
# == Defining MATLAB command ==
# =============================
if [[ -z "${SF_AR_MATVER}" ]]; then
  export MATVER='r2018b' # Because of the redesign of PRODCOM
else
  export MATVER=${SF_AR_MATVER}
fi

MATBIN=matlab${MATVER}

if [ ! -f "$(which ${MATBIN})" ]; then
  echo "Could not find matlab binary ${MATBIN}. Exiting..."
  exit 1
fi

# =======================
# == Defining run mode ==
# ======================
if [[ -z "${SF_AR_INTERACT}" ]]; then
  cmd="${MATBIN} -sd AUTORUN -nodesktop -nosplash -r autorun_core -logfile autorun.log"
else
  export SFARKEEP=1
  cmd="${MATBIN} -sd AUTORUN -r autorun_core"
fi

# ==============================
# == Choosing autorun version ==
# ==============================
# SFARVER=short -> Short autorun testing only basic functionnalities
# SFARVER=long  -> Long autorun testing advanced functionnalities and
#                  physical results for cases of reference
if [[ -z "${SF_AR_SFARVER}" ]]; then
  export SFARVER="short"
else
  export SFARVER=${SF_AR_SFARVER}
fi

# =========================
# == Exporting SFBASEDIR ==
# =========================

export SFBASEDIR="$(pwd)"

# ==========================
# == Exporting MATLABPATH ==
# ==========================

export MATLABPATH="$(pwd)/SOURCES_MATLAB"

# =====================
# == Running autorun ==
# =====================

echo "Using MATLAB from: ${MATBIN}"
echo "Using FreeFem++ from: ${FFBASEDIR}"
echo "Running a ${SFARVER} autorun."

echo "Running cmd: ${cmd}"
$cmd

exit $?

