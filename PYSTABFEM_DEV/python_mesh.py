import numpy as np
#import vtk
import matplotlib.pyplot as plt
#from vtk.util.numpy_support import vtk_to_numpy
import matplotlib.cm as cm
import os



class ffmesh:
	
	def __init__(self,filename):
	# adapted from ffreadmesh.m by chloros and SFcore_ImportMesh.m
		
		# First read the file "mesh.msh"
		fid = open(filename,'r')
		#self.points = fid.read()
		lines_list = fid.readlines()
		fid.close()

		self.nv,self.nt,self.nbe = (int(val) for val in lines_list[0].split())
		self.points = np.array([[float(val) for val in line.split()] for line in lines_list[1:self.nv+1]])
		self.tri = np.array([[int(val) for val in line.split()] for line in lines_list[self.nv+1:self.nv+self.nt+1]])
		self.bound = np.array([[int(val) for val in line.split()] for line in lines_list[self.nt+self.nv+1:self.nv+self.nt+self.nbe+1]])

        #self.labels=unique(bounds(3,bound(3,:)~=0)); // the labels present in third row of array boundaries
        
      # Now we have to read files "mesh.ff2m", "mesh_connectivity.ff2m" if they are present. 
	  # for this we need to translate the ugly SFcore_ImportData...
	  # at the end this should be something like
	  # if exist(mesh.ff2m) self.description = sf.importdata("mesh.ff2m")
	  # if exist(mesh_connectivity.ff2m) self.connectivity = sf.importdata("mesh_connectivity.ff2m")
	  # 
		
		
	def show(self):
		print("This is a mesh object")
		print(self.nv)
		print(self.nt)	
		print(self.nbe)	

class ffdata:
    def __init__(self,filename,mesh):
        self.mesh = mesh 
        fid = open(filename,'r')
		#self.points = fid.read()
        lines_list = fid.readlines()
        fid.close()

        self.length = int(lines_list[0])
        T = []
        for line in lines_list[1:]:
            for i in range(len(line.split())):
                T.append((float(line.split()[i])))
        self.T = np.array(T) 
		
    def show(self):
        print("This is a Data object")
        print("Contains a P1 field with name T")

def sfplot(bf,field):    
    # This example works for P1 fields stored direcly without triangle data
    umin,umax=np.min(bf.T),np.max(bf.T)
    levels = np.linspace(0,umax,21)
    cmap   = cm.get_cmap(name='jet', lut=None)
    plt.tricontour(bf.mesh.points[:,0], bf.mesh.points[:,1], bf.T, levels=levels,colors="black",linewidths=0.5);
    plt.tricontourf(bf.mesh.points[:,0], bf.mesh.points[:,1], bf.T, levels=levels, cmap=cmap );
    plt.colorbar(ticks=[0,0.02, 0.04, 0.06, 0.08])
    plt.tick_params(labelsize=20)
    plt.axis('equal')

def runfreefem(filename):
    stringcommand = "/usr/local/bin/FreeFem++  "+filename;
    os.system(stringcommand);

# EXAMPLE


#os.system("/usr/local/bin/FreeFem++  Lshape_Steady_P1.edp")
runfreefem("Lshape_Steady_P1.edp")

ff = ffmesh("mesh.msh")
ff.show()

bf = ffdata("Data.txt",ff)
bf.show()

sfplot(bf,'T')




	
	