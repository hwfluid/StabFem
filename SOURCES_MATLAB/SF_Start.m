% SCRIPT SF_Start
% 
% This program us used to set the global variables needed by StabFem
%
% It is basically equivalent to SF_core_start except that it 
% is a script and not a function. It also adds the SF source directory to 
% the matlab/octave path using absolute location
% (necessary with Octave !)
% 
% This program belongs to the StabFem project

% If verbosity and ffdatadir exit prior launching the script we get their value 
% (for legacy mode)
if ~exist('verbosity','var')
    verbosity = 4;
end
if ~exist('ffdatadir','var')
    ffdatadir = './WORK/';
end
    
SF_core_start('verbosity',verbosity,'workdir',ffdatadir);

addpath([SF_core_getopt('sfroot') '/SOURCES_MATLAB']);


if ~exist(ffdatadir,'dir') && ~strcmp(ffdatadir,'./')
    SF_core_log('n','Creating arborescence');
    SF_core_arborescence('create');
end