function SFcore_addParameter(p,baseflow,ParamName,Default,expectedtype)
% This function is used to bypass the "Addparameter" to tweak default values as follows :
% 1/ if the parameter is in the list of options in the arguments of the
%       function we use the corresponding value
% 2/ instead, if the parameter is a field of the "baseflow" we use this value 
% 3/ otherwise we use the default value.

if (isfield(baseflow, ParamName)) 
    TheDefault = baseflow.(ParamName);
else
    TheDefault = Default;
end
addParameter(p, ParamName, TheDefault, expectedtype); % Reynolds
end
