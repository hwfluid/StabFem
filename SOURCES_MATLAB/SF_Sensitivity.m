function [sensitivity] = SF_Sensitivity(baseflow, eigenmodeD, eigenmodeA, varargin)
%
% TODO :
%
global sfopts;
ff = 'FreeFem++';
ffMPI = 'FreeFem++-mpi';
ffdir = sfopts.ffdir;
ffdatadir = sfopts.ffdatadir;

%% Parse variable inputs
p = inputParser;
SFcore_addParameter(p, baseflow,'Re', 1, @isnumeric); % Reynolds
if (isfield(baseflow,'Ma'))
   SFcore_addParameter(p, baseflow,'Mach', baseflow.Ma, @isnumeric); % Mach
else
    SFcore_addParameter(p, baseflow,'Mach', 0.01, @isnumeric); % Mach
end
if (isfield(eigenmodeD,'k'))
   SFcore_addParameter(p, eigenmodeD,'k', eigenmodeD.k, @isnumeric); % k
else
    SFcore_addParameter(p, eigenmodeD,'k', 0, @isnumeric); % spanwise waven
end
addParameter(p, 'Type','S'); % Mode selection
parse(p, varargin{:});

%% Position input files for FreeFem solver
SFcore_MoveDataFiles(baseflow.mesh.filename,'mesh.msh','cp');
SFcore_MoveDataFiles(baseflow.filename,'BaseFlow.txt','cp');
SFcore_MoveDataFiles(eigenmodeD.filename,'EigenmodeDS.txt','cp');
SFcore_MoveDataFiles(eigenmodeA.filename,'EigenmodeAS.txt','cp');

%% Selects the right solver
switch(lower(baseflow.mesh.problemtype))
    case('2dcomp')
        SF_core_log('n', '## Entering SF_BaseFlow (2D-Compressible)');
        ffparams = [ num2str(p.Results.Re), ' ',...
            num2str(p.Results.Mach), ' ', p.Results.Type, ' ', ...
            baseflow.mesh.symmetry, ' ', num2str(p.Results.k)];
        ffsolver = 'Sensitivity2D_Comp.edp';
        ffbin = ffMPI;
    otherwise
        error(['Error in SF_Sensitivity : not currently implemented for problemtype = ',baseflow.mesh.problemtype]);
end

%% Launching FF solver
value = SF_core_freefem(ffsolver,'parameters',ffparams,'bin',ffbin);
if (value==210) 
    SF_core_log('e','An input file is missing!');
    return
elseif (value>0)
    SF_core_log('e','Stop here');
end



%% Importing results
if (p.Results.Type == "S")
    filename = SFcore_MoveDataFiles('StructSen.ff2m','MISC');
    sensitivity = SFcore_ImportData(baseflow.mesh,filename);
end

if (p.Results.Type == "SMa")
    filename = SFcore_MoveDataFiles('SensitivityMa.ff2m','MISC');
    sensitivity = SFcore_ImportData(baseflow.mesh,filename);
end

if (p.Results.Type == "SForc")
    filename = SFcore_MoveDataFiles('SensitivityForcing.ff2m','MISC');
    sensitivity = SFcore_ImportData(baseflow.mesh,filename);
end
%     SF_core_log('n',' Estimating base flow and quasilinear mode from WNL')
%     SF_core_log('n',['### Mode characteristics : AE = ', num2str(selfconsistentmode.AEnergy), ' ; Fy = ', num2str(selfconsistentmode.Fy), ' ; omega = ', num2str(imag(selfconsistentmode.lambda))]);
%     SF_core_log('n',['### Mean-flow : Fx = ', num2str(meanflow.Fx)]);
%end
    SF_core_arborescence('cleantmpfiles') 
     
    SF_core_log('d', '### END FUNCTION SF_Sensitivity');
end