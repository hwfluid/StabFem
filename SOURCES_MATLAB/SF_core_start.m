function SF_core_start_new(varargin)
% Starting process:
%  1 - Checking the path to MATLAB_SOURCES is correctly defined
%  2 - If Octave, adding the path to OCTAVE_SOURCES
%  3 - Loading live opts
%  4 - Loading static opts
%  5 - Post-startup

% ============================
% == Reading user arguments ==
% ============================
numvarargs = length(varargin);
optsnames = {'matlabpath','octavepath','liveopts', 'staticopts', 'poststartup', 'force'};
vararginval = {@check_matlab_sources_path;
               @check_octave_sources_path;
               @load_live_opts;
               @load_static_opts;
               @poststartup_tasks;
               false};
optstest = {@isfcthandle, @isfcthandle, @isfcthandle, @isfcthandle, @isfcthandle, @islogical};
           
if (numvarargs>0)
    if (~mod(numvarargs,2))
        for i=1:2:(numvarargs-1)
            pos=find(strcmpi(optsnames,varargin(i)));
            if ~isempty(pos)
                if ~feval(optstest{pos},varargin{i+1})
                    error(['Incorrect value for ' varargin{i} ]);
                else
                    vararginval(pos)=varargin(i+1);
                end
            else
                fprintf('%s\n',char(varargin(i)));
                error('unknown input parameter');
            end
        end
    else
        error('wrong number arguments');
    end
end

% ==============================
% == Executing start-up steps ==
% ==============================
%TODO: handle the "force" mode from David.
%TODO: each function should return a boolean, if true, startup may continue.
%TODO: add a pre-startup
headers = {'Startup 01: Checking MATLABPATH';
    'Startup 02: Checking Octave Path';
    'Startup 03: Loading live options';
    'Startup 04: Loading static options';
    'Startup 05: Post-startup tasks'};
for i=1:5
    disp(headers{i});
    if nargout(vararginval{i})==0
      feval(vararginval{i});
    else
      r = feval(vararginval{i});
      if ~r
          error(['Error triggered by ' func2str(varargin{i}) ' in SF_core_start_new.']);
      end
    end
end


% ======================
% == NESTED FUNCTIONS ==
% ======================
    function r = isfcthandle(a)
        r = isa(a,'function_handle');
    end
    function check_matlab_sources_path()
        % We are currently executing Sf_core_start() so currently
        % SOURCES_MATLAB is either in the path or is the working directory.
        %
        % Just in case, we (re?)add it to the path
        curFilePath = mfilename('fullpath');
        [curDirPath, ~, ~] = fileparts(curFilePath);
        addpath(curDirPath);
    end

    function check_octave_sources_path()
        isoctave = (exist('OCTAVE_VERSION', 'builtin')~=0);
        if ~isoctave
            SF_core_log('dd', 'No need to check for Octave PATH, we are in MATLAB.');
        else
            %TODO: check that inputParser exists
        end
    end

    function load_live_opts()
        % -- Reseting options --
        SF_core_opts('reset');
        
        % -- Set verbosity --
        SF_core_setopt('verbosity', 3, 'sanitizer', @uint8, 'live', true, 'settable', true);
        
        % -- Detecting platform --
        if ispc
            platform = 'pc';
        elseif ismac
            platform = 'mac';
        elseif isunix
            platform = 'linux';
        end
        SF_core_setopt('platform', platform, 'live', true);
        
        % -- Detecting environment (MATLAB/OCTAVE) --
        SF_core_setopt('isoctave', exist('OCTAVE_VERSION', 'builtin')~=0, 'live', true);
        
        % -- Detecting whether GIT is available --
        [s,~] = system('git --version');
        if s==0
            SF_core_log('n', 'Git is available on this platform.');
        end
        SF_core_setopt('gitavailable',s==0, 'live', true, 'tester', @islogical);
        
        % -- Detecting whether we are in git repository --
        if SF_core_getopt('gitavailable')
            [s,~] = system('git rev-parse --is-inside-work-tree');
            gitrepository = s==0;
        else
            gitrepository = false;
        end
        SF_core_setopt('gitrepository', gitrepository, 'live', true, 'tester', @islogical);
        
        % -- If we are in git repository, set sfroot as live --
        if SF_core_getopt('gitrepository')
            try
                [s, t] = system('git rev-parse --show-toplevel');
            catch
                s = 1;
            end
            if s==0
                sfroot = SF_core_path(t,strcmp(SF_core_getopt('platform'),'pc'),true);
                SF_core_setopt('sfroot', sfroot, 'live', true)
            end
        end
        
        % -- If sfroot not set previously, trying fetching it from env var --
        if ~SF_core_isopt('sfroot') && ~isempty(getenv('SF_PROJECT_ROOT'))
            sfroot = getenv('SF_PROJECT_ROOT');
            if exist(sfroot,'dir')==7
                SF_core_setopt('sfroot', sfroot, 'live', true);
            end
        end
        
        % -- Try locating FreeFem++ automatically --
        %TODO: check that bin name FreeFem++ is the same on all platforms.
        %      I suspect a .exe as windows extension, in which case we ought
        %      to first define a ffexe (and ffexempi) which is platform dependent. 
        ffroot = [];
        try
            [s,t] = SF_core_syscommand('which', 'FreeFem++');
        catch
            s = 1;
        end
        if s==0
            [ffroot,~,~] = fileparts(t);
        else
            ffrootEnvVars = {'EBROOTFREEFEMPLUSPLUS' 'SF_FREEFEM_ROOT'};
            for I=1:numel(ffrootEnvVars)
                if ~isempty(getenv(ffrootEnvVars{I}))
                    ffrootTmp = getenv(ffrootEnvVars{I});
                    if exist(ffrootTmp, 'dir')==7
                        % either ffrootTmp already refers to the bin folder
                        if exist([ffrootTmp filesep 'FreeFem++'], 'file')==2
                            ffroot = ffrootTmp;
                            break;
                        % or it refers to the root of freefem++ installation
                        elseif exist([ffrootTmp filesep 'bin' filesep 'FreeFem++'], 'file')==2
                            ffroot = [ffrootTmp filesep 'bin'];
                            break;
                        end
                    end
                end
            end
        end
        if isempty(ffroot)
            % Attempt some classic locations
            if ismac && exist('/usr/local/bin/FreeFem++', 'file')==2
                ffroot='/usr/local/bin';
            elseif isunix && exist('/usr/bin/FreeFem++', 'file')==2
                ffroot='/usr/bin';
            end
        end
        if ~isempty(ffroot)
                SF_core_setopt('ffroot', ffroot, 'live', true);
        end
        
        % -- Setting a few more options --
        SF_core_setopt('ffarg', '-nw -v 0', 'live', true, 'settable', true);
        SF_core_setopt('ffargDEBUG', '-v 1', 'live', true, 'settable', true);
        SF_core_setopt('ffdatadir', './WORK/', 'live', true, ...
                                    'settable', true, 'sanitizer', @ffdatadirSanitizer, ...
                                    'watcher', @ffdatadirWatcher);
        SF_core_setopt('storagemode', 2, 'live', true, 'settable', true);
    end

    function load_static_opts()
        % First, attempt to read option file
        SF_core_opts('read');
        
        % Then, ask missing options to user
        while ~SF_core_isopt('sfroot')
            disp('The root of your current StabFem installation could not be detected.')
            t=input(sprintf('Please enter the path to the root of StabFem: '),'s');
            if isempty(t)
                disp('Please enter a value');
                continue
            end
            if ~exist(t,'dir')==7
                disp('This folder does not exist.')
                continue
            end
            sfroot = SF_core_path(t);
            SF_core_setopt('sfroot', sfroot);
        end
        
        while ~SF_core_isopt('ffroot')
            disp('FreeFem++ could not be automatically located on your system.');
            t=input(sprintf('Please enter the folder containing the FreeFem++ executable: '),'s');
            if isempty(t)
                disp('Please enter a value');
                continue
            end
            if ~exist(t,'dir')==7
                disp('This folder does not exist.')
                continue
            end
            if exist([t filesep 'FreeFem++'],'file')==2
                ffroot = SF_core_path(t);
            elseif exist([t filesep 'bin' filesep 'FreeFem++'],'file')==2
                ffroot = SF_core_path([t filesep 'bin']);
            else
                disp('FreeFem++ could not be found in this folder.');
                continue
            end
            SF_core_setopt('ffroot', ffroot);
        end
        
    end

    function r = poststartup_tasks()
        % Load a few missing options
        ffdir = [SF_core_getopt('sfroot') filesep 'SOURCES_FREEFEM' filesep];
        SF_core_setopt('ffdir', ffdir);
        SF_core_setopt('ffincludedir', [ffdir 'INCLUDE' filesep]);
        SF_core_setopt('ffloaddir', [ffdir 'LOAD' filesep]);
        
        
        
        % Write options in file
        
        % Check that all required options are available
        r = true; %Return false if some options are missing.
        
        % Set a few more:
        %  ffversion
        %  workdir as settable with watcher
        SF_core_setopt('workdir', './', 'live', true, 'settable', true, 'watcher', @watcherMkdir);
        
    end

    function watcherMkdir(optionname)
        SF_core_syscommand('mkdir', SF_core_getopt(optionname));
    end

    function output = ffdatadirSanitizer(input)
        if strcmp(input(end), filesep)
            output = input;
        else
            output = [input filesep];
        end
    end

    function ffdatadirWatcher(optname)
        if ~exist([SF_core_getopt(optname) 'STATS'],'dir')
            SF_core_arborescence('create');
        end
    end

end
