%> @file SOURCES_MATLAB/SF_core_freefem.m
%> @brief Matlab function handling request to execute FreeFem++
%>
%> Usage: [status, msg] = SF_core_freefem(cmd, paramName, paramValue, ...)
%> @param[out] status: returned status code
%> @param[out] msg: output produced by system call
%>
%> Available parameters:
%>  * 'bin' (def: 'FreeFem++'): freefem executable name
%>  * 'parameters' (def: ''): parameters provided to FreeFem+++sf
%>  * 'arg' (def: ''): arguments to give to the freefem executable
%>  * 'errormsg' (def: ''): error message threw when problem detected
%>  * 'logfile' (def: ''): name of a file in which the freefem output
%>                         should be redirected
%>  * 'logpath' (def: ''): path of the directory in which create the
%>                         logfile
%>  * 'prepipe' (def: ''): command redirected through a pipe to executable
%>     (not recommended any more)
%>
%>
%> Resulting system command:
%> $ prepipe | ffdir/bin ffargs arg cmd
%> or
%> echo parameters | ffdir/bin sfopt.ffarg ffarg cmd
%> @author Maxime Pigou
%> @version 1.1
%> @date 12/11/2018 Start writing version 1.0
%> @date 20/11/2018 Switching to input parser treatment of arguments
function [status] = SF_core_freefem(cmd,varargin)
% Parse arguments
p = inputParser;
addParameter(p, 'bin', 'FreeFem++', @ischar);
addParameter(p, 'parameters', '', @ischar);
addParameter(p, 'prepipe', '', @ischar);
addParameter(p, 'arg', '', @ischar);
addParameter(p, 'errormsg', '', @ischar);
addParameter(p, 'logfile', '', @ischar);
addParameter(p, 'logpath', '', @ischar);
parse(p, varargin{:});

bin = p.Results.bin;

% we should provide either 'parameter' or 'prepipe'
if ~isempty(p.Results.parameters)
    prepipe = [ 'echo ' p.Results.parameters];
elseif ~isempty(p.Results.prepipe)
   prepipe = p.Results.prepipe;
else 
    prepipe = '';
end

errormsg = p.Results.errormsg;
logfile = p.Results.logfile;
logpath = p.Results.logpath;

% appends the options to the default ones according to verbosity level
if(SF_core_getopt('verbosity')<8)
    args = [ SF_core_getopt('ffarg') p.Results.arg ];
else
     args = [ SF_core_getopt('ffargDEBUG') p.Results.arg ];
end

% Check options definition
if ~SF_core_opts('test')
    SF_core_log('e', 'SF_core_freefem: current options do not form a consistent execution environment.');
    status = 1;
    return;
end

% Check requested binary existence
if ~strcmp(SF_core_getopt('platform'),'pc')
    ffbin = sprintf('%s/%s', SF_core_getopt('ffroot'), bin);
    if exist(ffbin,'file')~=2
        SF_core_log('e', 'SF_core_freefem: FreeFem++ binary does not exist.');
        status = 2;
        return;
    end
else 
    ffbin = 'FreeFem++.exe'; % This should work this way on Windows
end

% Check program (either in current directory or in ffdir common directory) 
if (exist(cmd,'file'))
    SF_core_log('d',[ 'SF_core_freefem : solver ' cmd '  found in current directoty']);
else
    if ~strcmp(SF_core_getopt('platform'),'pc')
        if(exist([SF_core_getopt('ffdir') cmd],'file'))
             SF_core_log('d',[ 'SF_core_freefem : solver ' cmd '  found ffdir common directoty']);
            cmd = [SF_core_getopt('ffdir') cmd];
        else
            SF_core_log('e',[' Error in SF_Launch : FreeFem++ program ' ,cmd, ' not found']);
        end
    else
        cmd = [SF_core_getopt('ffdir') cmd];
    end
end


if ~isempty(prepipe)
    ffcmd = sprintf('%s | %s %s %s %s', prepipe, ffbin, args,  cmd);
else
    ffcmd = sprintf('%s %s %s %s', ffbin,  args, cmd);
end



SF_core_log('d', 'SF_core_freefem: starting freefem execution');
SF_core_log('d', '===========================================');
SF_core_log('d', '$$                                         ');
SF_core_log('nn', '$$                                         ');
SF_core_log('nn', sprintf('$$ > %s', ffcmd));
SF_core_log('nn', '$$                                         ');


 % -- Creation of the file workdir.idp (in case it was changed meanwhile...)
    fid = fopen('workdir.idp','w');
    fprintf(fid,'%s\n',[ 'string ffdatadir = "', SF_core_getopt('ffdatadir') , '" ;']);
    fclose(fid);
    
    SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') 'freefemerror.txt']);
    SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') 'freefemwarning.txt']);
    

% -- Creation of the file freefem++.pref
fid = fopen('freefem++.pref','w');
fprintf(fid,'%s\n',['loadpath += "', SF_core_getopt('ffloaddir'), '"']);
fprintf(fid,'%s\n',['includepath += "', SF_core_getopt('ffincludedir'), '"']);
fclose(fid);


if(SF_core_getopt('verbosity')>=4)%&&SF_core_getopt('verbosity')<10)
    [s] = system(ffcmd); % FreeFem outputs are displayed 'on the flight' 
else
    [s,t] = system(ffcmd); % FreeFem outputs are not displayed (but put in variable t) 
end
%if ~isempty(t); SF_core_log('dd', t); end % DAVID : no

SF_core_syscommand('rm', 'freefem++.pref');

SF_core_log('d', '$$                                         ');
SF_core_log('d', '===========================================');
SF_core_log('d', 'SF_core_freefem: ending freefem execution');

 fidwarning = fopen([SF_core_getopt('ffdatadir') 'freefemwarning.txt']);
    if fidwarning>0
        ffwarning = fgets(fidwarning);
        while ~isequal(ffwarning,-1)
            SF_core_log('w',['WARNING IN FREEFEM : ', ffwarning]);
            ffwarning = fgets(fidwarning);
        end
        fclose(fidwarning);
    end
    
    fiderror = fopen([SF_core_getopt('ffdatadir') 'freefemerror.txt']);
    if fiderror>0
        errormsg = fgets(fiderror);
        % SF_core_log('w',['ERROR IN FREEFEM : ', errormsg]); 
        % NB this is done above
        fclose(fiderror);
    end

if ~isempty(logfile)
    logRedirect = true;
    logFilePath = logfile;
    if ~isempty(logpath)
        if exist(logpath,'dir')~=7
            SF_core_log('w', 'SF_core_freefem: folder for log redirection does not exist, no redirection.');
            logRedirect = false;
            return;
        else
            logFilePath = SF_core_path(sprintf('%s/%s',logpath,logfile));
        end
    end
    if logRedirect
        fh = SF_core_file('fopentextwrite', logFilePath);
        fprintf(fh, t);
        fclose(fh);
    end
end

if s~=0 || (SF_core_getopt('isoctave') && (s~= 0 && s~=141 && s~=13))
    %if ~isempty(errormsg)
    %    SF_core_log('e', sprintf('SF_core_freefem: %s', errormsg));
    %end
    
   if isempty(errormsg)
    switch s
        case(1) 
            errormsg = 'Syntax error in your .edp program';
            errortype = 'e';
        case(7)
            errormsg = 'Attempt to read in a non-existing or invalid file';
            errortype = 'e';
        case(5)
            errormsg = 'Problem during mesh generation';
            errortype = 'e';
        case(134)
            errormsg = 'Solver error (MUMPS ?) (or maybe .edp file not found)';
            errortype = 'e';
        case(201)
            errormsg = ' Your Newton iteration did not converge';
            errortype = 'w';
        case(202)
            errormsg = ' Your Shift-invert iteration did not converge';
            errortype = 'w';
        case(210)
            errormsg = ' AdaptMesh failed (probably the number of fields is too large or you are using a format not yet implemented). Please report to the authors !';
            errortype = 'e';
        otherwise
            errormsg = ' unrepertoriated Freefem++ error';
            errortype = 'e';
    end
   else
       errortype = 'w';
   end
    SF_core_log(errortype, ['SF_core_freefem: Error while using FreeFem++  ; error code = ',num2str(s) ,' : ' ,errormsg]);
    status = s;
    % remarque : c'est un warning pas une erreur ! 
    % La generation d'une erreur (ou non) est faite  
    % dans la fonction qui appelle SF_core_freefem. les cas 201 et 202 ne
    % sont pas consideres comme des erreurs, le programme ne doit pas
    % s'arreter ! 
    % Par exemple si on fait une boucle et qu'on detecte une
    % divergence, on doit garder les résultats avant la divergence.
    % C'est le cas par exemple dans SF_Stability_LoopRe.m 
    
else
    status = 0;
end
end
