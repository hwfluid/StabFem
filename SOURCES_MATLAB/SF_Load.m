function flow = SF_Load(mode,index)
%>
%> function SF_Load
%> 
%> This function is used to load previously computed meshes/baseflows ;
%> useful when restarting from previously computed data
%>
%> USAGE : 
%>
%>   1/ bf = SF_Load('mesh+bf',i)
%>      -> load mesh/baseflow number i found in the MESHES/ subfolder
%>
%>   2/ bf = SF_Load('bf',i)
%>      -> load baseflow number i found in the BASEFLOW/ subfolder
%>
%>   3/ bf = SF_Load('EIGENMODE',i)
%>      -> load eigenmode number i found in the EIGENMODES/ subfolder
%>
%>   3b/ bf = SF_Load('EIGENMODE',filename)
%>      -> load eigenmode found in the EIGENMODES/ subfolder matching filename
%>
%>
%> (next options are obsolete/not validated ?)
%>
%>   3/ mesh = SF_Load('lastmesh')
%>       -> load mesh only
%> 
%>   4/ bf = SF_Load('lastadaptedbaseflow')
%>    
%>   5/ bf = SF_Load('lastbaseflow')
%>
%>   6/ mf = SF_Load('lastmeanflow') ( NOT YET VALIDATED !)
%>
%>   7/ dnsflow = SF_Load('lastDNS')
%>
%> This program belongs to the StabFem project, freely distributed under GNU licence.
%> copyright D. Fabre, 31 march 2019


sfstats = SF_Status('ALL','QUIET');

if (nargin==1) 
    index = 'last';
end

if ~isempty(sfstats.MESHES)
    switch lower(mode)
      
    case({'mesh+bf','bf+mesh','meshes','mesh'})
        if ~isempty(sfstats.MESHES)
            if (nargin>1)&&ischar(index)&&strcmp(index,'last')
                index = length(sfstats.MESHES);
            end
            SF_core_log('n',['Loading mesh + associated baseflow number ',num2str(index)]);
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(index).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow   = SFcore_ImportData(ffmesh,['MESHES/',sfstats.MESHES(index).filename]);      
        else
            flow = [];
            SF_core_log('e','Requested mesh not found');
        end
    
    case({'bf','baseflow','baseflows'})
        if ~isempty(sfstats.MESHES)
            if ischar(index) && strcmp(index,'last')
              index = length(sfstats.BASEFLOWS);
            end
            SF_core_log('n',['Loading baseflow number ',num2str(index), ' associated to current mesh']);
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(end).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow   = SFcore_ImportData(ffmesh,['BASEFLOWS/',sfstats.BASEFLOWS(index).filename]); 
        else
            flow = [];
            SF_core_log('e','Requested base flow not found');
        end
        
        case({'dns','dnsfield','dnsfields'})
        if ~isempty(sfstats.MESHES)
            if ischar(index) && strcmp(index,'last')
              index = length(sfstats.DNSFLOWS);
            end
            SF_core_log('n',['Loading dns snapshot number ',num2str(index), ' associated to current mesh']);
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(end).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow   = SFcore_ImportData(ffmesh,['DNSFIELDS/',sfstats.DNSFIELDS(index).filename]); 
        else
            flow = [];
            SF_core_log('e','Requested base flow not found');
        end    
        
    case({'eigenmode','eigenmodes'})
        if ~isempty(sfstats.MESHES)
            if ischar(index) && strcmp(index,'last')
              index = length(sfstats.EIGENMODES);
            end
            if ~ischar(index) 
               filename = sfstats.EIGENMODES(index).filename;
            else
                [~,f,d] = fileparts(index); 
                filename = [f,d]; % must work as well if there is a path
            end
            SF_core_log('n',['Loading EIGENMODE number ',num2str(index), ' associated to current mesh']);
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(end).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow   = SFcore_ImportData(ffmesh,['EIGENMODES/',filename]); 
        else
            flow = [];
            SF_core_log('e','Requested eigenmode not found');
        end
       
    case('mesh')
        if (nargin>1)&&ischar(index)&&strcmp(index,'last')
          index = length(sfstats.MESHES);
        end
        SF_core_log('n',['Loading mesh number ',num2str(index)]);
        flow   = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(index).MESHfilename]);     
    
    % next modes are not to be used any more
    case('lastmesh')
        if ~isempty(sfstats.MESHES)
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(end).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow = ffmesh;
            SF_core_log('n',['Loading last adapted mesh : ' sfstats.MESHES(end).MESHfilename]);
        else
            flow = [];
            SF_core_log('n','Last mesh not found');
        end
            
    case({'lastadaptedbaseflow','lastadapted'})
        if ~isempty(sfstats.MESHES)
            SF_core_log('n','Loading last adapted mesh + associated baseflow ');
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.MESHES(end).MESHfilename]);
            ffmesh = setsym(ffmesh);
            flow = SFcore_ImportData(ffmesh,['MESHES/',sfstats.MESHES(end).filename]);
            SF_core_log('n','Loading last adapted bf+mesh : ')
            SF_core_log('n',['    -> Mesh      : ', sfstats.MESHES(end).MESHfilename]);  
            SF_core_log('n',['    -> Base FLow : ', sfstats.MESHES(end).filename]);  
        else
            flow = [];
            SF_core_log('n','Last mesh not found');
        end
        
    case({'lastbaseflow','lastcomputed'})
        if ~isempty(sfstats.BASEFLOWS)
            ffmesh = SFcore_ImportMesh(['MESHES/',sfstats.LastMesh]);
            ffmesh = setsym(ffmesh);
            flow = SFcore_ImportData(ffmesh,['BASEFLOWS/',sfstats.BASEFLOWS(end).filename]);
            SF_core_log('n',['Importing last base flow :' sfstats.BASEFLOWS(end).filename]);
        else
            flow = [];
            SF_core_log('n','Last base flow not found');
        end
        
    case('lastmeanflow')% not validated
        ffmesh = SFcore_ImportMesh(sfstats.LastMesh);
        ffmesh = setsym(ffmesh);
        flow = SFcore_ImportData(ffmesh,sfstats.LastComputedMeanFlow);
    case('lastdns') % not validated
        ffmesh = SFcore_ImportMesh(sfstats.LastMesh);
        ffmesh = setsym(ffmesh);
        flow = SFcore_ImportData(ffmesh,sfstats.LastDNS);
    otherwise
        error('Error in SF_Load : option not recognized');
    end
    

    
    
else
    flow = [];
end

end
  
function ffmesh = setsym(ffmesh)
       %sets keyword 'symmetry'       
        if (ismember(6,ffmesh.labels)&&abs(min(ffmesh.points(2,:)))<1e-10)
            sym = 'S';
            SF_core_log('n','IN SF_Load : assuming symmetry = ''S'' ');
        else
            sym = 'N';
            SF_core_log('n','IN SF_Load : assuming symmetry = ''N'' ');
        end
        ffmesh.symmetry=sym; 
end
