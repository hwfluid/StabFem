%> @file SOURCES_MATLAB/SF_Adapt.m
%> @brief Matlab driver for Mesh Adaptation
%> 
%> Usage : 
%> 1/ mesh-associated mode (for linear problems without baseflows, e.g. sloshing, acoustics...)
%> [mesh [,flow1,flow2,...] ] = SF_Adapt(mesh,flow1 [,flow2,...] [,'opt1','val1']) 
%> 
%> 2/ baseflow-associated mode (for stability problems with baseflow)
%> [flow1 [,flow2,...] ] = SF_Adapt(flow1 [,flow2,...] [,'opt1','val1']) 
%>
%> @param[in] mesh : mesh-object (only when using in mesh-associated mode)
%> @param[in] flow1 : flow provided for mesh adaptation.
%> @param[in] (optional) flow2, flow3, etc... 
%>             additional flows for adaptation to multiple flows (max number currently 3) 
%> OPTIONS :
%> 
%> @param[out] flow1: flow structure reprojected on adapted mesh
%> @param[out] flow2; ... if asked, eigenmode recomputed on adapted mesh
%>
%> IMPORTANT NOTE : if using mode 2 and if flow1 is of type "BaseFlow" then it is recomputed
%>                  after flow adatpation. additional flows are simply reprojected 
%>                  on new mesh, not recomputed.
%>
%>
%> @author David Fabre & J. Sierra, redesigned in nov. 2018
%> @version 2.1
%> @date 02/07/2017 Release of version 2.1
%>
%> NB : Rationalisation in feb. 2019 but remains to be simplified !!

function varargout = SF_Adapt(varargin)
ffdatadir = SF_core_getopt('ffdatadir');

varargout = {}; % introduced to avoid a bug at line 227 in some cases (to be rationalized)

SF_core_log('d', '### ENTERING SF_ADAPT')

% managament of optional parameters
% NB here the parser had to be customized because input parameter number 2
% is optional and is a structure ! we should find a better way in future

%% sorting the input parameters into fields and options.
nfields=0;
%vararginopt={};
for i=1:nargin
if(isstruct(varargin{i}))
    % input mode for adaptation to base flow
    nfields=nfields+1;
%    flowforadapt(i) = varargin{i} % This does not work in this way ! see
%    below.
end
end
%nfields = length(flowforadapt)
%narginopt = nargin-nfields;
vararginopt = {varargin{nfields+1:end}};
if(strcmpi(varargin{1}.datatype,'mesh'))
    vararginfields = {varargin{2:nfields}};
    nfields = nfields-1;
    ffmesh = varargin{1};
elseif strcmpi(varargin{1}.datatype,'freesurface')
    vararginfields = {varargin{2:nfields}};
    nfields = nfields-1;
    ffmesh = varargin{1}.mesh;
    SFcore_MoveDataFiles(varargin{1}.filename,'FreeSurface.txt','cp');
else
    vararginfields = {varargin{1:nfields}};
    ffmesh = varargin{1}.mesh;
end


%if strcmp(vararginfields{1}.datatype,'BaseFlowSurface')
%    SF_core_log('w',' Your first object is a "BaseFlowSurface", you cannot adapt to it !'); 
%    SF_core_log('e',' use directly a mode or something suitable ');
%end
% creating an array of structures "flowtoadapt"
% here we want to do 
%   flowtoadapt = [varargin{1:nfields}] 
% but this does not work because the fields may have dissimilar structures !
% below is a WORKAROUND found there 
% https://fr.mathworks.com/matlabcentral/answers/152580-converting-a-cell-array-of-dissimilar-structs-to-an-array-of-structs
uniqueFields = unique(char(cellfun(@(x)char(fieldnames(x)),{vararginfields{1:nfields}},'UniformOutput',false)),'rows');
for k=1:nfields
     for u=1:size(uniqueFields,1)
         fieldName = strtrim(uniqueFields(u,:));
         if ~isfield(vararginfields{k}, fieldName)
             vararginfields{k}.(fieldName) = [];
         end
     end
end
flowforadapt = [vararginfields{1:nfields}];
% END WORKAROUND 

%% designation of the adapted mesh
if(isfield(ffmesh,'meshgeneration'))
     meshgeneration = ffmesh.meshgeneration;
else
    meshgeneration = 0;
end
designation = ['_adapt',num2str(meshgeneration+1)];
% this desingation will be added to the names of the mesh/BF files

if(strcmpi(flowforadapt(1).datatype,'BaseFlow'))
if (isfield(flowforadapt(1),'Re'))
   designation = [designation, '_Re' , num2str( flowforadapt(1).Re)];
end
if (isfield(flowforadapt(1),'Ma'))
   designation = [designation, '_Ma' , num2str( flowforadapt(1).Ma) ];
end
if (isfield(flowforadapt(1),'Omegax'))
   designation = [designation, '_Omegax' , num2str( flowforadapt(1).Omegax) ];
end
if (isfield(flowforadapt(1),'Cu'))
   designation = [designation, '_Cu' , num2str( flowforadapt(1).Cu) ];
end
if (isfield(flowforadapt(1),'AspectRatio'))
   designation = [designation, '_AspectRatio' , num2str( flowforadapt(1).AspectRatio) ];
end
if (isfield(flowforadapt(1),'nRheo'))
   designation = [designation, '_nRheo' , num2str( flowforadapt(1).nRheo) ];
end
end


%% Interpreting parameters
p = inputParser;
%    addRequired(p,'baseflow');
%    addOptional(p,'eigenmode',0);
addParameter(p, 'Hmax', 10);
addParameter(p, 'Hmin', 1e-4);
addParameter(p, 'Ratio', 10.);
addParameter(p, 'InterpError', 1e-2);
addParameter(p, 'rr', 0.95);
addParameter(p, 'Splitin2', 0);
addParameter(p, 'Splitbedge', 0);
addParameter(p, 'Thetamax', 10);
parse(p, vararginopt{:});

%% Writing parameter file for Adapmesh
writeParamFile('Param_Adaptmesh.edp',p.Results); %% see function defined at bottom


%% constructing option string and positioning files
optionstring = [' ', num2str(nfields), ' '];
for i=1:nfields
     %SF_core_syscommand('cp',flowforadapt(i).mesh.filename, [ffdatadir, 'mesh.msh']);
     SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
 %    SF_core_syscommand('cp',flowforadapt(i).filename, [ffdatadir, 'FlowFieldToAdapt',num2str(i),'.txt']);
     SFcore_MoveDataFiles(flowforadapt(i).filename, ['FlowFieldToAdapt',num2str(i),'.txt'],'cp');
%    if(isfield(flowforadapt(i),'datastoragemode')) 
    [~,storagemode,nscalars] = fileparts(flowforadapt(i).datastoragemode); % this is to extract two parts of datastoragemode, e.g. 
    if(strcmp(nscalars,''))
        nscalars = '0';
    else
        nscalars = nscalars(2:end); %to remove the dot
    end
%    else 
%        storagemode = 'ReP2P2P1';
%        nscalars = 1;
%    end
    optionstring = [optionstring, ' ', storagemode, ' ' , nscalars , ' '];
end



 
%% Invoking FreeFem++ program AdaptMesh.edp   

    SF_core_freefem('AdaptMesh.edp','parameters',optionstring);      


%% for ALE cases : must produce a secondary file for mesh inside the bubble.
if strcmp(ffmesh.problemtype,'axifreesurf')
     SF_core_freefem('CreateInnerMeshForALE.edp','parameters','postadapt');
end

%% OUTPUT    
if strcmp(ffmesh.problemtype,'axifreesurf')
    meshfilename = SFcore_MoveDataFiles('mesh_adapt.msh','MISC');
else
    meshfilename = SFcore_MoveDataFiles('mesh_adapt.msh',['MESHES/mesh',designation,'.msh']);
end

newmesh = SFcore_ImportMesh(meshfilename,'problemtype',ffmesh.problemtype);


%sets keyword 'symmetry'

if (isfield(ffmesh,'symmetry'))
   newmesh.symmetry=ffmesh.symmetry;
end

newmesh.meshgeneration=flowforadapt(1).mesh.meshgeneration+1;


if(strcmpi(varargin{1}.datatype,'mesh')) % UGLY FIX TO BE DONE BETTER
    nargoutF=nargout-1;
else
    nargoutF=nargout;
end

for i = 1:nargoutF
    if strcmp(ffmesh.problemtype,'axifreesurf')&&(i==1) % must do differently here to save the file
       finalname = SFcore_MoveDataFiles('FlowFieldAdapted1.txt','MISC/BaseFlow_ADAPTED_NO_RECOMPUTED.txt','cp');
       varargout{i} = SFcore_ImportData(newmesh,finalname);
    else
    varargout{i} = flowforadapt(i); %  copies the structure but the fields may be wrong !
    varargout{i}.filename = [ffdatadir,'FlowFieldAdapted',num2str(i),'.txt']; % ffdatadir is still here !
    varargout{i}.mesh = newmesh;
    % remove unneccesary fields from structure (see WORKAROUND at beginning)
    for u=1:size(uniqueFields,1)
         fieldName = strtrim(uniqueFields(u,:));
         if (length(getfield(varargout{i},fieldName))==0);
             varargout{i} = rmfield(varargout{i},fieldName);
         end
    end
    end
end

%% if first field is a base flow we have to recompute it !     
if strcmp(varargin{1}.datatype,'BaseFlow')&&~strcmp(ffmesh.problemtype,'axifreesurf') 
    SF_core_log('n',' SF_Adapt : recomputing base flow');
    baseflowNew = SF_BaseFlow(varargout{1}, 'type', 'POSTADAPT'); 
     if (baseflowNew.iter > 0)
     %  Newton successful : Store adapted mesh/base flow in directory "MESHES"
    finalname = SFcore_MoveDataFiles(baseflowNew.filename,['MESHES/BaseFlow',designation, '.txt'],'cp');
%    finalname = baseflowNew.filename;
    baseflowNew.filename = finalname;%[ffdatadir, 'MESHES/BaseFlow',designation, '.txt'];
    varargout{1} = baseflowNew; 
     
     % after adapt we clean the "BASEFLOWS","EIGENMODES",etc... directories 
     % as the previous baseflows are no longer compatible, and do a backup
     % of the baseflow in the BASEFLOW directory

%     SFcore_CleanDir('POSTADAPT'); 
     SF_core_arborescence('clean'); % will clean only if SF_core_getopt('storagemode')=2
     
     newfilename = SFcore_MoveDataFiles(finalname,['BASEFLOWS/BaseFlow',designation,'.txt'],'cp');
     else
         error('ERROR in SF_Adapt : baseflow recomputation failed');
     end
    
end

%% if first input was a mesh, then first output will be the mesh
if(strcmpi(varargin{1}.datatype,'mesh'))  
    if(isfield(ffmesh,'gamma'))
        newmesh.gamma = ffmesh.gamma;
    end
     if(isfield(ffmesh,'rhog'))
        newmesh.rhog = ffmesh.rhog;
     end
    varargout  = {newmesh varargout{:}}; % leave it this way even if not elegant ! varargout may be empty
elseif (strcmpi(varargin{1}.datatype,'freesurface')) 
    % if first argument was a "freesurface" description
    newname = SFcore_MoveDataFiles('FreeSurface_adapt.txt','MESHES');
    newsurface = SFcore_ImportData(newmesh,newname);
    varargout  = {newsurface varargout{:}}; % leave it this way even if not elegant ! varargout may be empty
end
SF_core_log('nnn','IN SF_Adapt : sghould we clean the Eigenmodes.* ?')   
%myrm([ffdatadir 'Eigenmode*']); % remove all eigenmode files

% eventually clean working directory from temporary files
SF_core_arborescence('cleantmpfiles')
 
SF_core_log('d', '### LEAVING SF_ADAPT')

end

function [] = writeParamFile(filename,p)
fid = fopen(filename, 'w');
fprintf(fid, '// Parameters for adaptmesh (file generated by matlab driver)\n');
fprintf(fid, ['real Hmax = ', num2str(p.Hmax), ' ;\n']);
fprintf(fid, ['real Hmin = ', num2str(p.Hmin), ' ;\n']);
fprintf(fid, ['real Ratio = ', num2str(p.Ratio), ' ;\n']);
fprintf(fid, ['real error = ', num2str(p.InterpError), ' ;\n']);
fprintf(fid, ['real rr = ', num2str(p.rr), ' ;\n']);
fprintf(fid, ['int Nbvx = 100000; \n']);        
fprintf(fid, ['real Thetamax = ', num2str(p.Thetamax),'; \n']);
fprintf(fid, 'real Verbosity    = 1; \n');
if(p.Splitbedge==0)
  fprintf(fid, 'bool Splitpbedge= false; \n');
else
  fprintf(fid, 'bool Splitpbedge= true; \n');
end 
if (p.Splitin2 == 0)
    fprintf(fid, ['bool Splitin2 = false ; \n']);
else
    fprintf(fid, ['bool Splitin2 = true ; \n' ]);
end
fclose(fid);
end


