%> @file SOURCES_MATLAB/SF_BaseFlow.m
%> @brief StabFem wrapper for Base flow calculation (Newton iteration)
%>
%> @param[in] baseflow: baseflow guess to initialise NEwton iterations
%> @param[in] varargin: list of parameters and associated values
%> @param[out] baseflow: baseflow solved by Newton iterations
%>
%> usage: 
%> 1/ baseflow = SF_BaseFlow(baseflow,['Param1',Value1,...])
%> 2/ baseflow = SF_BaseFlow(ffmesh,['Param1',Value1,...])
%>
%> in mode 1 (first argument is a previously computed baseflow) 
%             this previous baseflow will be used to initialmize the Newton iter.  
%> in mode 2 (first argument is a mesh) 
%>              No initial condition is prescribed. The solver will use a default one 
%>             unless a macro "DefaultGuessForNewton" is defined in your Macro_StabFem.idp  
%>
%> This wrapper will launch the Newton FreeFem++ program of the corresponding
%>  case. Nota Bene: if baseflow was already created, it is simply copied from
%>  the "BASEFLOW" directory (unless specified otherwise by parameter 'Type').
%>
%> List of valid parameters:
%>   - Re          Reynolds number
%>   - Ma          Mach number (for compressible cases)
%>   - Omegax      Rotation rate (for swirling axisymetric or 2D body)
%>   - Darcy       Darcy number (for cases with porous body)
%>   - Porosity    Porosity (for cases with porous body)
%>   - Ra          Rayleigh number (for Boussinesq case)
%>   - Pr          Prandtl number  (for Boussinesq case) 
%>   - Type
%>      - 'Normal' (default) ;
%>      - 'NEW' to force new computation ;
%>      - 'POSTADAPT' for recomputing baseflow after mesh adaptation ;
%>      - 'PREV' if connection was lost (obsolete ?)
%>   - ncores      Number of cores (for parallel computations)
%>
%>
%> SF IMPLEMENTATION:
%> Depending on set parameters, this wrapper will select and launch one of the
%>  following FreeFem++ solvers:
%>       'Newton_Axi.edp'
%>       'Newton_AxiSWIRL.edp'
%>       'Newton_2D.edp'
%>       'Newton_2D_Comp.edp'
%>         (... maybe many others...)
%>
%> Nota Bene: if for some reason the mesh/baseflow compatibility was lost, use
%>  SF_BaseFlow(baseflow,'Re',Re,'type','PREV') to reconstruct the structure and
%>  relocate files correctly. Similarly, to force recomputation even if files exist,
%>  (for instance, just after adaptmesh), use SF_BaseFlow(baseflow,'Re',Re,'type','NEW').
%>
%> This syntax allows to do baseflow=SF_BaseFlow(baseflow) which is useful
%>  for instance to recompute the baseflow after mesh adaptation.
%>
%> @author David Fabre
%> @date 2017-2018
%> @copyright GNU Public License

function baseflowNEW = SF_BaseFlow(baseflow, varargin)

SF_core_log('d', '### ENTERING FUNCTION SF_BaseFlow ');

% MANAGEMENT OF PARAMETERS (Re, Mach, Omegax, Porosity...)
% Explanation
% (Mode 1) if parameters are transmitted to the function we use these ones.
%      (for instance baseflow = SF_BaseFlow(baseflow1,'Re',10)
% (Mode 2) if no parameters are passed and if the field exists in the previous
% baseflow, we take these values
%      (for instance SF_BaseFlow(bf) is equivalent to SF_Baseflow(bf,'Re',bf.Re) )
% (Mode 3) if no previous value we will define default values set in the next lines.


p = inputParser;

SFcore_addParameter(p, baseflow,'Re', 1, @isnumeric); % Reynolds
if (isfield(baseflow,'Ma'))
   SFcore_addParameter(p, baseflow,'Mach', baseflow.Ma, @isnumeric); % Mach
else
    SFcore_addParameter(p, baseflow,'Mach', 0.01, @isnumeric); % Mach
end

if (isfield(baseflow,'Omegax'))
   SFcore_addParameter(p, baseflow,'Omegax', baseflow.Omegax, @isnumeric); % Mach
else
    SFcore_addParameter(p, baseflow,'Omegax', 0, @isnumeric); % Mach
end

SFcore_addParameter(p, baseflow,'Darcy',1 , @isnumeric); % For porous body
SFcore_addParameter(p, baseflow,'Porosity', 0.95, @isnumeric); % For porous body too

SFcore_addParameter(p, baseflow,'Cu', 0, @isnumeric); % For rheology
SFcore_addParameter(p, baseflow,'AspectRatio', 0, @isnumeric); % For rheology
SFcore_addParameter(p, baseflow,'nRheo', 0, @isnumeric); % For rheology

SFcore_addParameter(p, baseflow, 'Ra', 1705,  @isnumeric); % for Boussinesq
SFcore_addParameter(p, baseflow, 'Pr', 10,    @isnumeric); % for Boussinesq
SFcore_addParameter(p, baseflow, 'Qmag', 0,   @isnumeric); % for Boussinesq

addParameter(p, 'type', 'Normal', @ischar); % mode type
addParameter(p, 'ncores', 1, @isnumeric); % number of cores to launch in parallel

addParameter(p, 'MappingDef', 'none'); % complex Mapping type (OBSOLETE) 
addParameter(p, 'MappingParams', 'default'); % Array of parameters for the cases involving mapping

parse(p, varargin{:});


% Now the right parameters are in p.Results
Re = p.Results.Re;
Ma = p.Results.Mach;
%Omegax = p.Results.Omegax;
%Darcy = p.Results.Darcy;
%Porosity = p.Results.Porosity;
%ncores = p.Results.ncores; % By now only for the 2D compressible


%%% Position input files
    if (strcmpi(baseflow.datatype,'baseflow')||strcmpi(baseflow.datatype,'addition'))
        SF_core_log('n',['Computing base flow for Re = ', num2str(Re), '  starting from guess']);
        SFcore_MoveDataFiles(baseflow.filename,'BaseFlow_guess.txt','cp');
        SFcore_MoveDataFiles(baseflow.mesh.filename,'mesh.msh','cp');
        mesh = baseflow.mesh;
        problemtype = baseflow.mesh.problemtype;
        
    elseif (strcmpi(baseflow.datatype,'mesh'))
        SF_core_log('n', ['Computing base flow for Re = ', num2str(Re), ' starting from guess']);
        mymyrm('BaseFlow_guess.txt'); % To be modified soon
        problemtype = baseflow.problemtype;
        mesh = baseflow;
        SFcore_MoveDataFiles(mesh.filename,'mesh.msh','cp');
        
    else
        % imported meshes do not work
        error('wrong type of argument to SF_BaseFlow')
    end

%%% SELECTION OF THE SOLVER TO BE USED DEPENDING ON THE CASE

ffbin = 'FreeFem++'; % (other option is FreeFem++-mpi ; specified if required)

switch (lower(problemtype))
    
     case ({'2d','2dmobile'})
        SF_core_log('n', '## Entering SF_BaseFlow (2D INCOMPRESSIBLE)');
        ffparams = [ num2str(Re), ' ', num2str(p.Results.Omegax), ' ' , mesh.symmetry];
        ffsolver = 'Newton_2D.edp';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), '_Omegax', num2str(p.Results.Omegax)];
        
    case ('2dboussinesq')
        SF_core_log('n', '## Entering SF_BaseFlow (2D Boussinesq)');
        ffparams = [ num2str(p.Results.Ra), ' ', num2str(p.Results.Pr), ' ' ,num2str(p.Results.Qmag)];
        ffsolver = 'Newton_2D_Boussinesq.edp';
        BFfilename = '';%[ 'BaseFlow_Re', num2str(Re), '_Omegax', num2str(p.Results.Omegax)];    

    case ('2drheology')
        SF_core_log('n', '## Entering SF_BaseFlow (2D INCOMPRESSIBLE) with rheology');
        ffparams = [ num2str(Re), ' ', num2str(p.Results.Omegax), ' ' , num2str(p.Results.AspectRatio), ' ' , num2str(p.Results.Cu), ' ' , num2str(p.Results.nRheo), ' ' , mesh.symmetry];
        ffsolver = 'Newton_2D_Rheology.edp';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), '_Cu', num2str(p.Results.Cu), '_AspectRatio', num2str(p.Results.AspectRatio), '_nRheo', num2str(p.Results.nRheo)];
        
    case ('axixr') % Newton calculation for axisymmetric base flow
        SF_core_log('n', '## Entering SF_BaseFlow (axisymmetric case)');
       ffparams = num2str(Re);
       ffsolver = 'Newton_Axi.edp';
       BFfilename = [ 'BaseFlow_Re', num2str(Re)];
        
    case ('axixrcomplex') % Newton calculation for axisymmetric base flow WITH COMPLEX MAPPING
        
        SF_core_log('n', '## Entering SF_BaseFlow (axisymmetric case)');
        %%% Writing parameter file for Adapmesh
        if ~strcmp(p.Results.MappingDef,'none')
            SF_core_log('w','Using COMPLEX BASE FLOW Newton_Axi_COMPLEX.edp (not recommended any more)'); 
            SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams);
            ffsolver = 'Newton_Axi_COMPLEX.edp';
        else
            SF_core_log('n','Using regular REAL base flow with solver Newton_Axi.edp'); 
            ffsolver = 'Newton_Axi.edp';
        end
        ffparams = num2str(Re);        
        BFfilename = [ 'BaseFlow_Re', num2str(Re)];
   
    case ('axicompcomplex')
        SF_core_log('n', '## Entering SF_BaseFlow (axisymmetric Compressible case COMPLEX)');
        % generating file "Param_Mapping.edp" used by Newton and stab. solver
        %%% Writing parameter file for Adapmesh
        SFcore_CreateMappingParamFile('Type2',p.Results.MappingParams); %% See auxiliary function of this file
        %if(length(p.Results.MappingParams)==9) % if no parameters are specified then the file must already exist
        %    fid = fopen('Param_Mapping.edp', 'w');
        %        fprintf(fid, '// Parameters for complex mapping (file generated by matlab driver)\n');
        %        fprintf(fid, ['real ParamMapx0 = ', num2str(p.Results.MappingParams(1)), ' ;']);
        %        fprintf(fid, ['real ParamMapx1 = ', num2str(p.Results.MappingParams(2)), ' ;']);
        %        fprintf(fid, ['real ParamMapLA = ',  num2str(p.Results.MappingParams(3)), ' ;']);
        %        fprintf(fid, ['real ParamMapLC = ', num2str(p.Results.MappingParams(4)), ' ;']);
        %        fprintf(fid, ['real ParamMapGC = ',  num2str(p.Results.MappingParams(5)), ' ;']);
        %        fprintf(fid, ['real ParamMapyo = ', num2str(p.Results.MappingParams(6)), ' ;']);
        %        fprintf(fid, ['real ParamMapLAy = ',  num2str(p.Results.MappingParams(7)), ' ;']);
        %        fprintf(fid, ['real ParamMapLCy = ', num2str(p.Results.MappingParams(8)), ' ;']);
        %        fprintf(fid, ['real ParamMapGCy = ',  num2str(p.Results.MappingParams(9)), ' ;']);
        %    fclose(fid);
        %end
        SF_core_log('n', '## Entering SF_BaseFlow (Axi-COMPLEX COMPRESSIBLE) ');
        ffparams = [num2str(Re), ' ', num2str(p.Results.Mach)];
        ffsolver = 'Newton_Axi_Comp_COMPLEX.edp';
        ffbin = 'FreeFem++-mpi';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), 'Ma', num2str(Ma)];
    
    case ('axicompcomplex_m') % with azimuthal mode
        SF_core_log('n', '## Entering SF_BaseFlow (axisymmetric Compressible case COMPLEX)');
        % generating file "Param_Mapping.edp" used by Newton and stab. solver
        %%% Writing parameter file for Adapmesh
        SFcore_CreateMappingParamFile('Type2',p.Results.MappingParams); %% See auxiliary function of this file
        %if(length(p.Results.MappingParams)==9) % if no parameters are specified then the file must already exist
        %    fid = fopen('Param_Mapping.edp', 'w');
        %        fprintf(fid, '// Parameters for complex mapping (file generated by matlab driver)\n');
        %        fprintf(fid, ['real ParamMapx0 = ', num2str(p.Results.MappingParams(1)), ' ;']);
        %        fprintf(fid, ['real ParamMapx1 = ', num2str(p.Results.MappingParams(2)), ' ;']);
        %        fprintf(fid, ['real ParamMapLA = ',  num2str(p.Results.MappingParams(3)), ' ;']);
        %        fprintf(fid, ['real ParamMapLC = ', num2str(p.Results.MappingParams(4)), ' ;']);
        %        fprintf(fid, ['real ParamMapGC = ',  num2str(p.Results.MappingParams(5)), ' ;']);
        %        fprintf(fid, ['real ParamMapyo = ', num2str(p.Results.MappingParams(6)), ' ;']);
        %        fprintf(fid, ['real ParamMapLAy = ',  num2str(p.Results.MappingParams(7)), ' ;']);
        %        fprintf(fid, ['real ParamMapLCy = ', num2str(p.Results.MappingParams(8)), ' ;']);
        %        fprintf(fid, ['real ParamMapGCy = ',  num2str(p.Results.MappingParams(9)), ' ;']);
        %    fclose(fid);
        %end
        SF_core_log('n', '## Entering SF_BaseFlow (Axi-COMPLEX COMPRESSIBLE) ');
        ffparams = [num2str(Re), ' ', num2str(p.Results.Mach)];
        ffsolver = 'Newton_Axi_Comp_COMPLEX_m.edp';
        ffbin = 'FreeFem++-mpi';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), 'Ma', num2str(Ma)];
        
    case ({'axixrporous','axixrswirl'}) % axisymmetric WITH SWIRL
        SF_core_log('n', '## Entering SF_BaseFlow (axisymmetric case WITH SWIRL)');
        ffparams = [ num2str(Re), ' ', num2str(p.Results.Omegax), ' ', num2str(p.Results.Darcy), ' ', num2str(p.Results.Porosity)];
        ffsolver = 'Newton_AxiSWIRL.edp';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), '_Omega', num2str(p.Results.Omegax), '_Da', num2str(p.Results.Darcy), '_Por', num2str(p.Results.Porosity)];
        
    case ('2dcomp')
        %fff = [ ffMPI ' -np ',num2str(ncores) ]; %does not work with FreeFem++-mpi
        %SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams); %% See auxiliary function of this file
        SF_core_log('n', '## Entering SF_BaseFlow (2D COMPRESSIBLE COMPLEX) ');
        ffparams = [num2str(Re), ' ', num2str(p.Results.Mach), ' ', num2str(p.Results.Omegax), ' ',mesh.symmetry];
        ffsolver = 'Newton_2D_Comp.edp';
        ffbin = 'FreeFem++-mpi';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), 'Ma', num2str(Ma), 'Omegax', num2str(p.Results.Omegax)];

    case ('2dcompsponge')
        %fff = [ ffMPI ' -np ',num2str(ncores) ]; %does not work with FreeFem++-mpi
        SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams); %% See auxiliary function of this file
        SF_core_log('n', '## Entering SF_BaseFlow (2D COMPRESSIBLE SPONGE) ');
        ffparams = [num2str(Re), ' ', num2str(p.Results.Mach), ' ', num2str(p.Results.Omegax), ' ',mesh.symmetry];
        ffsolver = 'Newton_2D_Comp_Sponge.edp';
        ffbin = 'FreeFem++-mpi';
        BFfilename = [ 'BaseFlow_Re', num2str(Re), 'Ma', num2str(Ma), 'Omegax', num2str(p.Results.Omegax)];

        % case (other cases...)
    otherwise
        SF_core_log('e',['ERROR : problem type ' problemtype ' not recognized ']); 
end %switch
if (myexist([BFfilename, '.txt'])&&~strcmpi(p.Results.type, 'NEW')&&~strcmpi(p.Results.type, 'POSTADAPT'))&&SF_core_getopt('storagemode')~=1
    SF_core_log('n', ['Base flow already computed for Re = ', num2str(Re)]);
 %   baseflowNEW = baseflow;
    baseflowNEW = SFcore_ImportData(mesh,['BASEFLOWS/' BFfilename, '.txt']);
    baseflowNEW.iter = 0;
    
else
        
    % CALL NEWTON SOLVER

    value = SF_core_freefem(ffsolver,'parameters',ffparams,'bin',ffbin);
    
    if (value==201) 
        SF_core_log('w','Newton iteration DID NOT CONVERGE !');
        baseflowNEW = baseflow; 
        baseflowNEW.iter = -1;
        return
    elseif (value>0)
        SF_core_log('e','Stop here');
    end
   
    if ~isempty(BFfilename)
        BFfilename = ['/' BFfilename, '.txt']; % to allow autoindexing mode
    end
    
    if (strcmpi(baseflow.datatype,'mesh'))
        SFcore_MoveDataFiles('BaseFlow.txt',['BASEFLOWS' BFfilename ],'cp');
        finalname = SFcore_MoveDataFiles('BaseFlow.txt','MESHES');
    else
    if(strcmpi(p.Results.type, 'POSTADAPT')==1)
         % after adapt we clean the "BASEFLOWS" directory as the previous baseflows are no longer compatible 
       %  SFcore_MoveDataFiles('BaseFlow.txt',['MESHES' BFfilename ],'cp');
%         SFcore_CleanDir('BASEFLOWS'); 
        SF_core_arborescence('clean'); % this will erase BASEFLOWS 
         finalname = SFcore_MoveDataFiles('BaseFlow.txt',['BASEFLOWS' BFfilename ]);
    else
            % Copy under the expected name
     finalname = SFcore_MoveDataFiles('BaseFlow.txt',['BASEFLOWS' BFfilename ]);
    end
    end 

   
% import data
baseflowNEW = SFcore_ImportData(mesh,finalname);

message = '=> Base flow converged '; 
     if isfield(baseflowNEW, 'iter')
        message = [message ' in ', num2str(baseflowNEW.iter), ' iterations '];
    end
    if isfield(baseflowNEW, 'Fx')  %% adding drag information for blunt-body wake
        message = [message, '; Fx = ', num2str(baseflowNEW.Fx)];
    end
    if isfield(baseflowNEW, 'Lx')  %% adding drag information for blunt-body wake
        message = [message, '; Lx = ', num2str(baseflowNEW.Lx)];
    end
    if (isfield(baseflowNEW, 'deltaP0') == 1) %% adding pressure drop information for jet flow
        message = [message, '; deltaP0 = ', num2str(baseflowNEW.deltaP0)];
    end
    SF_core_log('n', message);


end

    
% eventually clean working directory from temporary files
SF_core_arborescence('cleantmpfiles')
 
SF_core_log('d', '### END FUNCTION SF_BASEFLOW ');

end


function mymyrm(filename)
SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') filename]);
end

function SFcore_addParameter(p,baseflow,ParamName,Default,expectedtype)
% This function is used to bypass the "Addparameter" to tweak default values as follows :
% 1/ if the parameter is in the list of options in the arguments of the
%       function we use the corresponding value
% 2/ instead, if the parameter is a field of the "baseflow" we use this value 
% 3/ otherwise we use the default value.

if (isfield(baseflow, ParamName)) 
    TheDefault = baseflow.(ParamName);
else
    TheDefault = Default;
end
addParameter(p, ParamName, TheDefault, expectedtype); % Reynolds
end


function res = myexist(filename)
res = exist([SF_core_getopt('ffdatadir') 'BASEFLOWS/' filename],'file');
end