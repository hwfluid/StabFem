function baseflow = SF_BFContinue(varargin)
ffdatadir = SF_core_getopt('ffdatadir');
%>
%> TO BE DONE

%% Read parameters
baseflow = varargin{1}(end);
ffmesh = baseflow.mesh;
vararginopt = {varargin{2:end}};

p = inputParser;
addParameter(p, 'step', 0.01);
addParameter(p, 'param', 'Re');
parse(p, vararginopt{:});

% Rename variables for easy access
param = p.Results.param;
step = p.Results.step;
Re = baseflow.Re;

if(isfield(baseflow,'tangent1'))
    tangent1 = baseflow.tangent1;
else
    tangent1 = 1.0;
end

if(isfield(baseflow,'tangent2'))
    tangent2 = baseflow.tangent2;
else
    tangent2 = 1.0;
end

if(isfield(baseflow,'Ma'))
    Mach = baseflow.Ma
end

if(isfield(baseflow,'Omegax'))
    Omegax = baseflow.Omegax;
else
    Omegax = 0;
end

%%% Position input files

if (strcmpi(baseflow.datatype,'baseflow')||strcmpi(baseflow.datatype,'addition'))
    SF_core_log('n',['Computing base flow for Re = ', num2str(Re), '  starting from guess']);
    SFcore_MoveDataFiles(baseflow.filename,'BaseFlow_guess.txt','cp');
    SFcore_MoveDataFiles(baseflow.mesh.filename,'mesh.msh','cp');
    mesh = baseflow.mesh;
    problemtype = baseflow.mesh.problemtype;
elseif (strcmpi(baseflow.datatype,'mesh'))
    SF_core_log('n', ['Computing base flow for Re = ', num2str(Re), 'starting from guess']);
    mymyrm('BaseFlow_guess.txt'); % To be modified soon
    problemtype = baseflow.problemtype;
    mesh = baseflow;
    SFcore_MoveDataFiles(mesh.filename,'mesh.msh','cp');
    % imported meshes do not work
else
    error('wrong type of argument to SF_BaseFlow')
end


%% Launch FreeFem codes
mydisp(1, ['FUNCTION SF_BFContinue : computing baseflow with pseudo arclength, step = ',num2str(step)]);

switch (lower(baseflow.mesh.problemtype))
    case('2d')
        % Determination of sign of predictor tangents
        mydisp(1, '## Entering SF_BFContinue (2D INCOMPRESSIBLE)');
        ffparams = [ num2str(step), ' ', num2str(tangent1), ' ', num2str(tangent2), ' ', baseflow.mesh.symmetry];
        ffsolver = 'ArcLengthContinuation2D.edp';
        ffbin = 'FreeFem++'; % (other option is FreeFem++-mpi ; specified if required)
        BFfilename = [ffdatadir, 'BASEFLOWS/BaseFlow_ArcLength'];
    case('2dcomp')
        % Determination of sign of predictor tangents
        mydisp(1, '## Entering SF_BFContinue (2D COMPRESSIBLE)');
        ffparams = [ num2str(step), ' ', num2str(tangent1), ' ', num2str(tangent2), ' ', baseflow.mesh.symmetry];
        ffsolver = 'ArcLengthContinuation2D_Comp.edp';
        ffbin = 'FreeFem++-mpi'; % (other option is FreeFem++ ; specified if required)
        BFfilename = [ffdatadir, 'BASEFLOWS/BaseFlow_ArcLength'];
  % case("your case...")
        % add your case here !
    otherwise
        error(['Error in SF_BFContinue : your case ', baseflow.mesh.problemtype 'is not yet implemented....'])    
end

value = SF_core_freefem(ffsolver,'parameters',ffparams,'bin',ffbin);
    
if (value>0);
    error('ERROR : SF_ base flow computation did not converge');
end

%% Import data to Matlab
baseflow = SFcore_ImportData(baseflow.mesh, [BFfilename, '.ff2m']);
% Update BFfilename once we know Omega and Re
switch (lower(baseflow.mesh.problemtype))
    case('2d')
        BFfilename = [ffdatadir, 'BASEFLOWS/BaseFlow_Re', num2str(baseflow.Re), '_Omegax', num2str(baseflow.Omegax)];
    case('2dcomp')    
         BFfilename = [ffdatadir, 'BASEFLOWS/BaseFlow_Re', num2str(baseflow.Re), '_Omegax', num2str(baseflow.Omegax), '_Mach', num2str(baseflow.Ma)];
end
% Copy in the proper directory
SFcore_MoveDataFiles([ffdatadir, 'BaseFlow.txt'],[BFfilename, '.txt']);
SFcore_MoveDataFiles([ffdatadir, 'BaseFlow.ff2m'],[BFfilename, '.ff2m']);
%SFcore_MoveDataFiles(baseflow.mesh.filename,'mesh.msh');
% SFcore_MoveDataFiles(baseflow.filename,'BaseFlow_guess.txt');
baseflow.filename = [BFfilename, '.txt']; 


if (baseflow.iter >= 1)
    message = ['=> Base flow converged in ', num2str(baseflow.iter), ' iterations '];
    if (isfield(baseflow, 'Fx') == 1) %% adding drag information for blunt-body wake
        message = [message, '; Fx = ', num2str(baseflow.Fx)];
    end
    if (isfield(baseflow, 'Lx') == 1) %% adding drag information for blunt-body wake
        message = [message, '; Lx = ', num2str(baseflow.Lx)];
    end
    if (isfield(baseflow, 'deltaP0') == 1) %% adding pressure drop information for jet flow
        message = [message, '; deltaP0 = ', num2str(baseflow.deltaP0)];
    end
    mydisp(2, message);
else
    mydisp(1, ['      ### Base flow recovered from previous computation for Re = ', num2str(Re)]);
end

mydisp(2, '### END FUNCTION SF_BASEFLOW ');
end


