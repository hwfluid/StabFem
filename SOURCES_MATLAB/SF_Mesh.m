function mesh = SF_Mesh(meshfile, varargin)
%> Matlab/FreeFem driver for generating a mesh using a FreeFem++ program
%>
%> Usage : mesh = SF_Mesh('Mesh.edp',[opt1,val1,...])
%>
%>  Optional parameters :
%>  'Params'  :      array of real-valued parameters [Param1 Param2 ...] 
%>                   containing the parameters (reals) needed by the freefem script ;  
%>                   for instance dimensions of the domain, etc...
%>  'problemtype' :  class of problems ('2d','axi','2ccomp',....) 
%>  'cleanworkdir' : 'yes' to remove any previously computed files 
%>                   from the working directory, 'no' instead.
%>
%> 'Mesh.edp' must be a FreeFem script which generates a file "mesh.msh".
%>  For the simplest cases this file will be the only needed.
%>
%>  For more elaborate cases available in the StabFem project, this function
%>  will also read the following auxiliary files (if present) : 
%>  * mesh.ff2m
%>  * mesh_connectivity.ff2m
%>  * mesh_surface.ff2m
%>
%> 
%> This file belongs to the StabFem project freely disctributed under gnu licence.
if(SF_core_getopt('storagemode')==2)
   SF_core_arborescence('cleanall');
end


p = inputParser;
addParameter(p, 'Params', []);
addParameter(p, 'problemtype','');
addParameter(p, 'symmetry','N');
addParameter(p, 'cleanworkdir','no');
parse(p, varargin{:})

switch lower(p.Results.problemtype)
    case({'axixrcomplex','2dcomplex'})
        SF_core_log('w', [' USE of problemtype = ',p.Results.problemtype,' not recommended any more !']); 
end

if strcmp(p.Results.cleanworkdir,'yes')
  SF_core_arborescence('cleanall');
  SF_core_arborescence('cleantmpfiles');
end
% Cleans the arborescence
%    SFcore_CleanDir('TEMPFILES','MESHES_FORCE'); 
%    SFcore_CleanDir('POSTADAPT'); 

% launches the FreeFem program
if (isempty(p.Results.Params))
 value = SF_core_freefem(meshfile);
else
    stringparam = ' ';
    for pp = p.Results.Params
        stringparam = [stringparam, num2str(pp), '  '];
    end
     value = SF_core_freefem(meshfile,'parameters',stringparam);
end
if (value>0) 
    SF_core_log('e','Leaving SF_Mesh here');
    return
end



% Imports the mesh (afted displacing it to the MESH folder)

meshfilename = SFcore_MoveDataFiles('mesh.msh','MESHES');
mesh = SFcore_ImportMesh(meshfilename);


% sets keyword 'problemtype' and writes corresponding file 
if ~isempty(p.Results.problemtype)
    mesh.problemtype = p.Results.problemtype;
    SFcore_Writeff2mFile('problemtype.ff2m',...
        'filedescription','This file was created by SF_Mesh','problemtype',mesh.problemtype);
%    if(exist([SF_core_getopt('ffdatadir'), 'MESHES'],'dir'))
%        SFcore_Writeff2mFile('MESHES/problemtype.ff2m',...
%        'filedescription','This file was created by SF_Mesh','problemtype',mesh.problemtype);
%    end
end

% Sets keyword 'symmetry' 
% (assuming symmetric flow if the mesh has a border label 6 along the x-axis)

sym = p.Results.symmetry;
if (ismember(6,mesh.labels)&&abs(min(mesh.points(2,:))<1e-10)&&strcmpi(sym,'n'))
    sym = 'S';
end
mesh.symmetry=sym;





% Copies the files under aliases in the relevant directories (NB : file SF_Init.ff2m is OBSOLETE)
%if(exist([SF_core_getopt('ffdatadir'), 'SF_Init.ff2m'],'file'))
%   mycp([SF_core_getopt('ffdatadir'), 'SF_Init.ff2m'],[SF_core_getopt('ffdatadir'), 'MESHES/SF_Init.ff2m']);
%end

%if(exist([SF_core_getopt('ffdatadir'), 'BASEFLOWS'],'dir')==7)
%     SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') '/BASEFLOWS']);
%     SF_core_syscommand('mkdir',[SF_core_getopt('ffdatadir') '/BASEFLOWS']);
%end

SF_core_log('n', ['      ### INITIAL MESH CREATED WITH np = ', num2str(mesh.np), ' points']);

% eventually clean working directory from temporary files
% (except for free surface cases)
% if ~strcmpi(mesh.problemtype,'3dfreesurfacestatic')&&~strcmpi(mesh.problemtype,'3dfreesurfacestatic')
%    SF_core_arborescence('cleantmpfiles')
% end
 
SF_core_log('d', '      END OF FUNCTION SF_Mesh.m');

end

