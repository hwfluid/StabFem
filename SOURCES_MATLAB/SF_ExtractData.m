
function values = SF_ExtractData(ffdata,field,X,Y)
%
% This function interpolated data from a P1-ordered field of a ffdata,
% on prescribed X and Y vectors.
%
% Usage : value = SF_ExtractData(ffdata,fieldname,X,Y)
% X and Y are expected as 1D-vectors of same length 
% (but if either X or Y are a single constant, the function will work as well)
%
% Examples of usage : UXaxis = SF_ExtractData(bf,'ux',[0:.01:10],0); -> values on the x-axis 
%                     Vortline = SF_ExtractData(em,'vort1',[0:.01:10],[[0:.01:10]); -> values of the vorticity on a diagonal line  
% 
% Adapted from ffplottri2gridint from Chloros, 2018. 
% Incorporated into the StabFem project by D. F on nov. 1, 2018.
% Redesigned on March 2019 based on ffinterpolate 
% (using VhSeq for P1, P2, P1b and vectorized fields)
%

    
if(length(X)==1)
    X = X*ones(size(Y));
end

if(length(Y)==1)
    Y = Y*ones(size(X));
end
    

switch(length(ffdata.(field)))
    case (ffdata.mesh.np)
        vhseq = reshape(ffdata.mesh.tri(1:3,:),size(ffdata.mesh.tri,2)*3,1)-1;
    case (3*ffdata.mesh.nt)
        vhseq = [0 : 3*ffdata.mesh.nt-1];
    case (4*ffdata.mesh.nt)
        vhseq = [0 : 4*ffdata.mesh.nt-1];
    case (6*ffdata.mesh.nt)
        vhseq = [0 : 6*ffdata.mesh.nt-1];
   case (ffdata.mesh.np2)
        vhseq = ffdata.mesh.Vh_P2;
    case (ffdata.mesh.np1b)
        vhseq = ffdata.mesh.Vh_P1b;
 end
   
 values = ffinterpolate(ffdata.mesh.points,ffdata.mesh.bounds,ffdata.mesh.tri,vhseq,X,Y,ffdata.(field));

end
