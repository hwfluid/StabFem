function bs = SF_Deform(bs, varargin)
% Matlab/SF_ driver for mesh deformation  (Newton iteration)
%
% usage : ffmesh = SF_Mesh_Deform(ffmesh,'Volume',Volume,[...])
%
% this driver will lanch the "NewtonMesh" program of the coresponding
% case.
%
% Version 2.0 by D. Fabre , september 2017
%

%ffdatadir = SF_core_getopt('ffdatadir');

%%% MANAGEMENT OF PARAMETERS (Re, Mach, Omegax, Porosity...)

%%% check which parameters are transmitted to varargin (Mode 1)
p = inputParser;

% Parameters for static problems
   addParameter(p,'gamma',1,@isnumeric); % Surface tension
   addParameter(p,'rhog',0,@isnumeric); % gravity parameter
   addParameter(p,'V',-1,@isnumeric); % Volume 
   addParameter(p,'P',1,@isnumeric); % Pressure 
   addParameter(p,'typestart','pined',@ischar); % 
   addParameter(p,'typeend','pined',@ischar); % 
   addParameter(p,'GAMMABAR',0,@isnumeric);
   
% Parameters for dynamic problems   
   addParameter(p,'Oh',1,@isnumeric); % Ohnesorgue number
   addParameter(p,'We',0,@isnumeric); % Weber number
   addParameter(p,'dS',[]); % Arclength continuation parameter
   
parse(p, varargin{:});

if(p.Results.GAMMABAR~=0)
    error('ERROR : GAMMABAR (rotation) not yet fully implemented... Newton_Axi_FreeSurface_Static.edp should be revised');
end

ffmesh = bs.mesh;

%mycp(ffmesh.filename, [ffdatadir, 'mesh_guess.msh']); % position mesh file
SFcore_MoveDataFiles(ffmesh.filename,'mesh_guess.msh','cp');
SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
%SFcore_MoveDataFiles(bs.filename,'FreeSurface.txt','cp'); %actually this is not useful
switch (lower(ffmesh.problemtype))
    
    case ('3dfreesurfacestatic')
        
        datafilename = 'FreeSurface.txt';
        
        if(p.Results.V~=-1)% V-controled mode
            SF_core_log('n','## Deforming MESH For STATIC FREE SURFACE PROBLEM (V-controled)'); 
            parameterstring = [' " V ',num2str(p.Results.V),' ',num2str(p.Results.gamma),...
                ' ',num2str(p.Results.rhog),' ',num2str(p.Results.GAMMABAR),'  ',p.Results.typestart,' ',p.Results.typeend,' " '];
            ffsolver = 'Newton_Axi_FreeSurface_Static.edp';
%            solvercommand = ['echo ',parameterstring, ' | ',ff,' ',ffdir,'Newton_Axi_FreeSurface_Static.edp'];

        elseif(p.Results.P~=-1)% P-controled mode
            SF_core_log('n','## Deforming MESH For STATIC FREE SURFACE PROBLEM (P-controled)'); 
            parameterstring = [' " P ',num2str(p.Results.P),' ',num2str(p.Results.gamma),...
                ' ',num2str(p.Results.rhog),' ',num2str(p.Results.GAMMABAR),' ',p.Results.typestart,' ',p.Results.typeend,' " '];
            ffsolver = 'Newton_Axi_FreeSurface_Static.edp';
            %solvercommand = ['echo ',parameterstring, ' | ',ff,' ',ffdir,'Newton_Axi_FreeSurface_Static.edp'];
        end
        
        
   case ('axifreesurf')
       
      SFcore_MoveDataFiles(bs.filename,'BaseFlow.txt','cp'); 
      datafilename = 'BaseFlow.txt';
      
      if isempty(p.Results.dS)
        SF_core_log('n','## Deforming MESH For FREE SURFACE PROBLEM (ALE) : Oh/We mode'); 
         SF_core_log('n',['## Parameters : Oh = ',num2str(p.Results.Oh),' ; We = ',num2str(p.Results.We), ' ']);
        parameterstring = [' 4 ',num2str(p.Results.Oh),' ',num2str(p.Results.We), ' '];
        ffsolver = 'Newton_Axi_Surface_ALE_Fourier.edp';
      else
        SF_core_log('n','## Deforming MESH For FREE SURFACE PROBLEM (ALE) : continuation mode'); 
        SF_core_log('n',['## Parameters : Oh = ',num2str(p.Results.Oh),' ; dS = ',num2str(p.Results.dS), ' ']);
        parameterstring = [' 4 ',num2str(p.Results.Oh),' ',num2str(p.Results.dS), ' '];
        ffsolver = 'Newton_Axi_Surface_ALE_Fourier_arclength.edp';
      end
   
   otherwise
        error('case not implemented in SF_Mesh_Deform')
end


%errormessage = 'ERROR : SF_BaseFlow_MoveMesh computation aborted';
%mysystem(solvercommand, errormessage); %needed to generate .ff2m file

value = SF_core_freefem(ffsolver,'parameters',parameterstring);

if(value ~= 0)
  SF_core_log('w','Newton iteration diverged !');
  bs.iter = -1;
  return
end
%    SF_core_log('n', '#### Mesh deformation converged');

    SF_core_arborescence('clean'); % will only clean if storagemode=1

    newname = SFcore_MoveDataFiles('mesh.msh','MESHES');
    ffmeshNew = SFcore_ImportMesh(newname);
    ffmeshNew.problemtype = ffmesh.problemtype;
    mesh = ffmeshNew; 
    
%    newname_surf = ['MESHES/FreeSurface',newname(21:end-4),'.txt']; % to get the number
%    finalname = SFcore_MoveDataFiles('FreeSurface.txt',newname_surf);
     finalname = SFcore_MoveDataFiles(datafilename,'MESHES');   
    bs = SFcore_ImportData(mesh,finalname);

    % next to be done better 
    for thefield = {'rsurf','zsurf','S0','N0r','N0z','K0a','K0b','alpha'}
        if isfield(bs,thefield{1})
            bs.mesh.(thefield{1}) = bs.(thefield{1});
            bs = rmfield(bs,thefield{1});
        end
    end
    

SF_core_log('n', '#### SF_Mesh_Deform : NEW MESH CREATED');
%SF_core_log('n', ['Volume = ', num2str(ffmesh.Vol)]);
%SF_core_log('n', ['P0 = ', num2str(ffmesh.P0)]);

end