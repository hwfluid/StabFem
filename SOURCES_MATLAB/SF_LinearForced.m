function res = SF_LinearForced(bf,varargin)
%>
%> Function SF_LinearForced
%>
%> This function solves a linear, forced problem for a single value of
%> omega or for a range of omega (for instance impedance computations)
%>
%> Usage :
%> 1/ res = SF_LinearForced(bf,'omega',omega) (single omega mode)
%>      in this case res will be a flowfield structure 
%> 
%>  2/ res = SF_LinearForced(bf,'omega',omega) (loop-omega mode)
%>      in this case res will be a structure composed of arrays, as specified in the Macro_StabFem.edp 
%>      (for instance omega and Z)
%>
%>  Parameters : 'plot','yes' => the program will plot the impedance and
%>                               Nyquist diagram
%>  Parameters : 'BC','SOMMERFELD' => Boundary condition for the farfield
%>                                    for the acoustic simulations 
%>
%> Copyright D. Fabre, 11 oct 2018
%> This program is part of the StabFem project distributed under gnu licence.

%ffdatadir = SF_core_getopt('ffdatadir');

if (mod(length(varargin),2)==1) 
    SF_core_log('l','SF_LinearForced : recommended to use [''Omega'',omega] in list of arguments ');  
    varargin = ['omega',varargin];
end
    
   p = inputParser;
   addParameter(p,'omega',1); 
   addParameter(p,'plot','no',@ischar); 
   addParameter(p,'solver','default');
   addParameter(p,'BC','SOMMERFELD',@ischar);
   addParameter(p, 'MappingDef', 'none'); % Array of parameters for the cases involving mapping
   addParameter(p, 'MappingParams', 'default'); % Array of parameters for the cases involving mapping

   parse(p,varargin{:});

   omega = p.Results.omega;

% Creating mapping if needed (even if no mapping we need to recreate the file !)
        SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams);
   
% Position input files
if(strcmpi(bf.datatype,'Mesh')==1)
       % first argument is a simple mesh
       ffmesh = bf; 
       SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp'); 
else
       % first argument is a base flow
       ffmesh = bf.mesh;
       SFcore_MoveDataFiles(bf.filename,'BaseFlow.txt','cp');
       SFcore_MoveDataFiles(bf.mesh.filename,'mesh.msh','cp'); 
end

switch lower(ffmesh.problemtype)

    case({'axixr','axixrcomplex'}) % Jet sifflant Axi Incomp.
        if strcmp(p.Results.MappingDef,'none')
            solver = 'LinearForcedAxi_m0.edp';
        else
            SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams);
            solver = 'LinearForcedAxi_COMPLEX_m0.edp';
        end
        FFfilename = [ 'Field_Impedance_Re' num2str(bf.Re) '_Omega' num2str(omega(1))];
        FFfilenameStat = [ 'Impedances_Re' num2str(bf.Re)];
        paramstring = [' array ' num2str(length(omega))];
        
%    case('axixrcomplex')
%        solver = [ 'LinearForcedAxi_COMPLEX_m0.edp'];
%        FFfilename = [ffdatadir 'Field_Impedance_Re' num2str(bf.Re) '_Omega' num2str(omega(1))];
%        FFfilenameStat = [ffdatadir 'Impedances_Re' num2str(bf.Re)];
%        paramstring = [' array ' num2str(length(omega))];    
        
    case({'2d','2dmobile'}) % VIV
        solver = 'LinearForced2D.edp';
        FFfilename = ['Field_Impedance_Re' num2str(bf.Re) '_Omega' num2str(omega(1))];
        FFfilenameStat = [ 'Impedances_Re' num2str(bf.Re)];
        paramstring = [' array ' num2str(length(omega))];
    case('acousticaxi') % Acoustic-Axi (Helmholtz).
        solver = 'LinearForcedAcoustic.edp';
        FFfilename = [ 'Field_Impedance_Re_Omega' num2str(omega(1))];
        FFfilenameStat = [ 'Impedances_Re'];
        paramstring = [p.Results.BC, ' array ' num2str(length(omega))];
    otherwise
        error(['Error : problemtype ' ffmesh.problemtype ' not recognized']);
end

if(strcmp(p.Results.solver,'default'))
    SF_core_log('n',['      ### USING STANDARD StabFem Solver ',solver]);        
else
    solver = p.Results.solver;
    SF_core_log('w',['      ### USING CUSTOM FreeFem++ Solver ',solver]);
end 

for i=1:length(omega)
    paramstring = [paramstring ' ' num2str(real(omega(i))),' ',num2str(imag(omega(i))) ];
end

% solvercommand = ['echo ',paramstring,' | ', ff, ' ', solver];
% errormsg = 'Error in SF_ForcedFlow';
% mysystem(solvercommand, errormsg);
    SF_core_freefem(solver,'parameters',paramstring);
 
 if(length(omega)==1)
     
    newfilename = SFcore_MoveDataFiles('ForcedFlow.ff2m',['FORCEDFLOWS/',FFfilename, '.ff2m']);
    res = SFcore_ImportData(ffmesh,newfilename);
    
 else
   % mycp([ 'LinearForcedStatistics.ff2m'],[ffdatadir FFfilenameStat,'.ff2m']);
    newfilename = SFcore_MoveDataFiles('LinearForcedStatistics.ff2m',['FORCEDSTATS/',FFfilenameStat,'.ff2m']); % not sure where to put it    
    res = SFcore_ImportData(ffmesh,newfilename);
    
    % if complex data is included reconstruct it here (to be improved)
        if(isfield(res,'Z_r'))
            res.Z = res.Z_r+1i*res.Z_i;
            res.omega = res.omega_r;
        end
         if(isfield(res,'Zr'))
            res.Z = res.Zr+1i*res.Zi;
        end
    
    %plots...
    if(~strcmp(p.Results.plot,'no'))
    figure;
    if(strcmpi(bf.mesh.problemtype,'axicomplex'))
    subplot(1,2,1); hold on;
    plot(res.omega,real(res.Z),'b-',res.omega,-imag(res.Z)./res.omega,'b--');hold on;
    plot(res.omega,0*real(res.Z),'k:','LineWidth',1)
    xlabel('\omega');ylabel('Z_r, -Z_i/\omega');
    title(['Impedance for Re = ',num2str(bf.Re)] );
    subplot(1,2,2);hold on;
    plot(real(res.Z),imag(res.Z)); title(['Nyquist diagram for Re = ',num2str(bf.Re)] );
    xlabel('Z_r');ylabel('Z_i');ylim([-10 2]);
    box on; pos = get(gcf,'Position'); pos(4)=pos(3)*.5;set(gcf,'Position',pos);
    pause(0.1);
    end
  
    if(strcmpi(bf.mesh.problemtype,'2d'))
    subplot(1,2,1); hold on;
    plot(res.omega/(2*pi),real(res.Z),'b-',res.omega/(2*pi),imag(res.Z),'b--');hold on;
    plot(res.omega/(2*pi),0*real(res.Z),'k:','LineWidth',1)
    xlabel('St');ylabel('Z_r, Z_i');
    title(['Impedance for Re = ',num2str(bf.Re)] );
    subplot(1,2,2);hold on;
    plot(real(res.Z),imag(res.Z)); title(['Nyquist diagram for Re = ',num2str(bf.Re)] );
    xlabel('Z_r');ylabel('Z_i');ylim([-10 2]);
    box on; pos = get(gcf,'Position'); pos(4)=pos(3)*.5;set(gcf,'Position',pos);
    pause(0.1);
    end
    
    end    
 end
 

 % eventually clean working directory from temporary files
 SF_core_arborescence('cleantmpfiles')
 
  
SF_core_log('d', '### END FUNCTION SF_LinearForced ');
 
end