function varargout = SF_Stability(baseflow,varargin)

%> StabFem wrapper for Eigenvalue calculations
%>
%> usage : 
%> 1/  [eigenvalues[,eigenvectors]] = SF_Stability(field, [,param1,value1] [,param2,value2] [...])
%> 2/  [eigenvalues[,sensitivity] [,evD,evA]] = SF_Stability(field,'type','S','nev',1, [...])
%> 3/  [eigenvalues[,Endogeneity] [,evD,evA]] = SF_Stability(field,'type','E','nev',1, [...])
%> 4/  [eigenvalues[,sensitivity],evD,evA,Endo] = SF_Stability(field,'type','S','nev',1, [...]) (not recommended)
%>
%> field is either a "baseflow" structure (with "mesh" structure as a subfield) 
%> or directly a "mesh" structure (for instance in problems such as sloshing where baseflow is not relevant).
%>
%> Output :
%> eigenvalues -> array containing the eigenvalues
%> eigenvector -> array of struct objects containing the eigenvectors.
%> [sensitivity,evD,evA,Endo] -> sensitivity, direct and adjoint eigenmodes and endogeneity (with type = 'S' and nev = 1)
%> 
%> List of accepted parameters (in approximate order of usefulness):
%>
%>  1/ geometrical parameters
%>
%>   m :         Azimuthal wavenumer (for axisymmetric problem) (def. 1)
%>   k :         Transverse wavenumber (for 3D stability of 2D flow) (def. 0)
%>   sym :       Symmetry condition for a 2D problem  (def. 'A')
%>               (set to 'S' for symmetric, 'A' for antisymmetric, or 'N' if no symmetry plane is present)
%> 
%>  2/ Numerical parameters of the solver
%>    
%>   shift :     shift for shift-invert algorithm.
%>               value can be either a numerical value (complex), 'prev' to use previously computed eigenvalue, 
%>               or 'cont' to use extrapolated value from two previous computations. 
%>               (obviously 'prev' and 'cont' cannot be used at first call to this function).
%>   nev :       requested number of eigenvalues
%>               (the solver will use Arnoldi method if nev>1 and shift-invert if nev=1)
%>   type :      'D' for direct problem (default) ; 'A' for ajoint problem ; 'DA' for discrete adjoint 
%>   solver :    Alternative solver 
%>               In this case the program will use an alternative ff solver
%>               (e.g. myStab2D.edp instead of Stab2D.edp)
%>               Useful in developpment/debugging mode ; the alternative
%>               solver has to use the same input parameters is the standard.
%>  guess :      eigenmode object used as initial condition for shift-invert (if nev=1)
%>  guessAdj :   same for the adjoint problem (if nev=1 and type = A,S or E)
%>  ifdiverge : 'error' | 'continue' behaviour in case 
%>              default is 'error' but in some loops it may be better to
%>              catch the error and continue)
%>
%>  3/ Physical parameters
%>
%>     Many options here, chose the ones relevant to your case)
%>     NB : in some cases the values will be automatically picked from the
%>     base flow (or mesh) object. But warning this is not done systematically
%>     
%>   Re :        Reynolds number (specify only if it differs from the one of base flow, which is not usual)
%>   STIFFNESS : For spring-mounted object
%>   MASS :      For spring-mounted object
%>   DAMPING :   For spring-mounted object
%>   gamma :     Surface tension (for free-surface problems)
%>   rhog :      gravity parameter (for free-surface problems)
%>   nu :        viscosity (for free-surface problems)
%>   GammaBAR :  circulation (for potential problems)
%>   alphaMILES :parameter modelling contact line dissipation (linear model of Miles & Hocking ) 
%> 
%>  4/ Post-processing options 
%> 
%>   sort :      how to sort the eigenvalues if nev>1. Accepted values :
%>               'LR' (largest real), 'SR' (smallest real), ,'SM' (smallest magnitude), 'SI', 'SIA' (smallest absolute value of imaginary part),
%>               'cont' (sort according to proximity with previous computation; continuation mode) 
%>   PlotSpectrum : set to 'yes' to launch the spectrum explorator.
%>               This option will draw the spectrum in figure 100 and will allow to display the eigenmodes 
%>               by clicking on the corresponding eigenvalue. 
%>  PlotSpectrumField : which field to plot in the spectrum explorator. 
%>  PlodModeOptions : list of keawords/values passed to SF_Plot in spectum explorator mode.
%>              (useful for instance to specifranges 'xlim','ylim' to be used) 
%>
%> STABFEM IMPLEMENTATION :
%> According to parameters, this wrapper will launch one of the following
%> FreeFem++ solvers :
%>      'Stab2D.edp'
%>      'StabAxi.edp'
%>       (list to be completed)
%>
%>
%> This program is part of the StabFem project distributed under gnu licence. 
%> Copyright D. Fabre, 2017-2019.
%>


ff = 'FreeFem++';
ffMPI = 'FreeFem++-mpi';



persistent sigmaPrev sigmaPrevPrev % for continuation on one branch
persistent eigenvaluesPrev % for sort of type 'cont'
  
%% Chapter 1 : management of optionnal parameters
    p = inputParser;
  
   % parameter for most cases
    SFcore_addParameter(p, baseflow, 'Re',1,@isnumeric); % Reynolds
    % parameter for compressible cases   
   SFcore_addParameter(p, baseflow, 'Ma',0.01,@isnumeric);
   
    % parameter for Rotating Porous Case
   SFcore_addParameter(p, baseflow, 'Omegax',0.,@isnumeric);
   SFcore_addParameter(p, baseflow, 'Darcy',0.1,@isnumeric);
   SFcore_addParameter(p, baseflow, 'Porosity',0.95,@isnumeric);  

   SFcore_addParameter(p, baseflow,  'Cu', 0., @isnumeric); % For rheology
   SFcore_addParameter(p, baseflow,  'AspectRatio', 1.0, @isnumeric); % For rheology
   SFcore_addParameter(p, baseflow,  'nRheo', 1.0, @isnumeric); % For rheology
   
   %parameters for spring-mounted object  
   addParameter(p,'STIFFNESS',0);
   addParameter(p,'MASS',0);
   addParameter(p,'DAMPING',0);
   
   % parameters for free-surface static problems
    SFcore_addParameter(p, baseflow, 'gamma' ,0.,@isnumeric);
    SFcore_addParameter(p, baseflow, 'rhog',1., @isnumeric);
    SFcore_addParameter(p, baseflow, 'nu' ,0., @isnumeric);
    SFcore_addParameter(p, baseflow, 'beta',1, @isnumeric);
    SFcore_addParameter(p, baseflow, 'GammaBAR',0., @isnumeric);
    SFcore_addParameter(p, baseflow, 'alphaMILES',0., @isnumeric);
    addParameter(p,'typestart','pined');
    addParameter(p,'typeend','pined');
    
    
    % parameters for free-surface dynamic problems
    SFcore_addParameter(p, baseflow, 'Oh' ,0.1,@isnumeric);
    SFcore_addParameter(p, baseflow, 'We',0., @isnumeric);
    addParameter(p, 'ALEoperator' ,'laplacian');
    
   %symmetry paramaters for axisymmetric case
   addParameter(p,'m',1,@isnumeric);
   %symmetry parameters for 2D case
   addParameter(p,'k',0,@isnumeric);

   if isfield(baseflow,'mesh')&&isfield(baseflow.mesh,'symmetry')&&strcmpi(baseflow.mesh.symmetry,'s') 
       symdefault = 'A'; 
   else
       symdefault = 'N'; 
   end
   addParameter(p,'sym',symdefault,@ischar);   
   
  %parameters for the eigenvalue solver
   addParameter(p,'shift',1+1i);
   addParameter(p,'nev',1,@isnumeric);
   addParameter(p,'type','D',@ischar); 
   addParameter(p,'solver','default',@ischar);
   addParameter(p,'guess','no',@isstruct);
   addParameter(p,'guessadj','no',@isstruct);
   
   %parameters for mpirun
   addParameter(p,'ncores',1,@isnumeric);
   
   % parameters for the post-processing options
   addParameter(p,'sort','no',@ischar); 
   addParameter(p,'PlotSpectrum','no',@ischar);
   addParameter(p,'PlotSpectrumField','ux',@ischar); 
   addParameter(p,'PlotModeOptions','',@iscell);
   addParameter(p,'plot','no',@ischar);
   addParameter(p,'ifdiverge','error');

   % parameters for COMPLEX MAPPING
   addParameter(p, 'MappingDef', 'none'); % Array of parameters for the cases involving mapping
   addParameter(p, 'MappingParams', 'default'); % Array of parameters for the cases involving mapping

   
   % parameters for acoustic pipes
   addParameter(p,'BC','SOMMERFELD',@ischar);

   parse(p,varargin{:});
   
   % parameters for continuation mode
   if(isempty(sigmaPrev))   
       sigmaPrev = p.Results.shift; 
       sigmaPrevPrev = p.Results.shift; 
   end
   if(isempty(eigenvaluesPrev)) 
       eigenvaluesPrev = p.Results.nev : -1 : 1 ; 
   end
   
   if(strcmp(p.Results.shift,'prev')==1)
       shift = sigmaPrev;       
       SF_core_log('d',['   # SHIFT from previous computation = ' num2str(shift)]); 
   elseif(strcmp(p.Results.shift,'cont')==1)      
       shift = 2*sigmaPrev-sigmaPrevPrev;      
       SF_core_log('d',['   # SHIFT extrapolated from two previous computations = ' num2str(shift)]); 
   elseif(isnumeric(p.Results.shift)==1)
       shift = p.Results.shift;
       SF_core_log('d',['   # SHIFT specified by user = ' num2str(shift)]); 
   else
       error('   # ERROR in SF_Stabilty while specifying the shift')
   end
 
%% position input files

   if(strcmpi(baseflow.datatype,'mesh')==1)
       % first argument is a simple mesh
       ffmesh = baseflow; 
       SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
   else
       % first argument is a base flow
       ffmesh = baseflow.mesh;
       SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
       SFcore_MoveDataFiles(baseflow.filename,'BaseFlow.txt','cp');
   end
   
   if(isstruct(p.Results.guess))
       SFcore_MoveDataFiles(p.Results.guess.filename,'Eigenmode_guess.txt'); 
   else
      mymyrm('Eigenmode_guess.txt') % TODO : add parameter to put a guess file only when required
   end
   
    if(isstruct(p.Results.guessadj))
       SFcore_MoveDataFiles(p.results.guessadj.filename,'EigenmodeAdj_guess.txt'); 
   else
      mymyrm('EigenmodeAdj_guess.txt') % TODO : add parameter to put a guess file only when required
   end
   
%% Chapter 2 : select the relevant freefem script

% explanation : we will launch a command with the form 
%   echo "47 0 0.7 A D 10" | FreeFem++ Stab2D.edp
%  
% this will be slitted in the form :
%   echo "argumentstring" | "fff" "solver" and processed thanks to SF_core_freefem
% so in each case we have to a) construct the argumentstring containing the parameters,
% b) define the fff command (usually FreeFem++ but can be FreeFem+++-mpi in some cases
%  and c) define the default solver (which can be replaced by a custom one)
%

if strcmpi(ffmesh.problemtype,'2d')&&((p.Results.STIFFNESS~=0)||(p.Results.MASS~=0))
  SF_core_log('n',' USING solver for 2D mobile object') 
  ffmesh.problemtype='2Dmobile';
end

switch lower(ffmesh.problemtype)

     case('acousticaxi')
     
     SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
     SF_core_log('n','      ### USING Axisymmetric Solver');
     argumentstring = [ (p.Results.BC) ' '  num2str(real(shift)) ' ' num2str(imag(shift)) ...
                          ' '  num2str(p.Results.nev) ];
     fff = ffMPI;               
     solver = 'StabAcoustics.edp';

    case({'axixr','axixrcomplex','axixrswirl'})
     
     SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
     if strcmp(p.Results.MappingDef,'none')
        SF_core_log('n','      ### USING Axisymmetric Solver');
        solver =  'StabAxi.edp';
         fff = ff;  
     else
         SF_core_log('n','      ### USING Axisymmetric Solver WITH COMPLEX MAPPING');
         SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams);    
         %if (p.Results.nev>1)
         %   fff = ffMPI;
         %else
         %    fff = ff;
         %    SF_core_log(2,'Nev = 1 : using ff instead of ffmpi (temporary fix waiting for Slepc/Mumps compatibility');
         %end
         % solver =  'StabAxi_COMPLEX.edp';
         if (p.Results.m==0)
             solver =  'StabAxi_COMPLEX_m0.edp';
         else
             solver =  'StabAxi_COMPLEX.edp';
         end
         fff = ffMPI;
     end
     argumentstring = [ num2str(p.Results.Re) ' '  num2str(real(shift)) ' ' num2str(imag(shift)) ...
                          ' ' num2str(p.Results.m) ' ' p.Results.type ' ' num2str(p.Results.nev) ];
                 

%     case ('axixrcomplex') 
     
%     SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
%     SF_core_log('n','      ### USING Axisymmetric Solver WITH COMPLEX MAPPING');
%     SFcore_CreateMappingParamFile(p.Results.MappingDef,p.Results.MappingParams);    
%     argumentstring = [' " ' num2str(p.Results.Re) ' '  num2str(real(shift)) ' ' num2str(imag(shift)) ...
%                          ' ' num2str(real(p.Results.m)) ' ' p.Results.type ' ' num2str(p.Results.nev) ' " ' ];
%     if(imag(p.Results.m)==0)
%        fff = ffMPI;               
%        solver =  'StabAxi_COMPLEX.edp'; 
%        solvercommand = ['echo ' argumentstring ' | ' ffMPI ' '  'StabAxi_COMPLEX.edp'];
%     else
%         SF_core_log('n','### TRICK ### m imaginary ; we use the alternative solver for m=0'); 
%         solvercommand = ['echo ' argumentstring ' | ' ffMPI ' '  'StabAxi_COMPLEX_m0.edp'];
%     end
     
%     status = mysystem(solvercommand);
        
    case('axixrporous')
    
     SF_core_log('n',['      ### FUNCTION SF_Stability POROUS : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
     SF_core_log('n','      ### USING Axisymmetric Solver WITH POROSITY AND SWIRL');
     argumentstring = [ num2str(p.Results.Re) ' ' num2str(baseflow.Omegax) ' ' num2str(baseflow.Darcy) ' ' num2str(baseflow.Porosity) ' '  num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' num2str(p.Results.m) ' ' p.Results.type ' ' num2str(p.Results.nev) ];
     fff = ff;               
     solver =  'StabAxi_Porous.edp';
      
    case('2drheology')
         % 2D flow (cylinder, etc...)

         if(p.Results.k==0)
            % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 2D Solver with Rheology');
        argumentstring = [ num2str(p.Results.Re) ' '  num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' p.Results.sym ' ' p.Results.type ' ' num2str(p.Results.nev)  ];
        fff = ff;               
        solver =  'Stab2D_Rheology.edp';
        else
          % TO BE DONE 
        end
 
    case('2d')
         % 2D flow (cylinder, etc...)

         if(p.Results.k==0)
            % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 2D Solver');
        argumentstring = [ num2str(p.Results.Re) ' '  num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' p.Results.sym ' ' p.Results.type ' ' num2str(p.Results.nev)  ];
        fff = ff;               
        solver =  'Stab2D.edp';
        
         else 
             % 2D BaseFlow / 3D modes
                 SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n',['      ### 3D Stability of 2D Base-Flow with k = ',num2str(p.Results.k)]);
        argumentstring = [num2str(p.Results.Re) ' ' num2str(p.Results.k) ' '  num2str(real(shift)) ....
            ' ' num2str(imag(shift)) ' ' p.Results.sym ' ' p.Results.type ' ' num2str(p.Results.nev)  ];
        fff = ff;               
        solver =  'Stab2D_Modes3D.edp';                 

         end
    
     case('2dboussinesq')
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 3D solver under Boussinesq approximation');
        argumentstring = [ num2str(p.Results.k) ' '  num2str(real(shift)) ' ' num2str(imag(shift))... 
                              ' ' p.Results.type ' ' num2str(p.Results.nev)  ];
        fff = ff;               
        solver =  'Stab2D_Boussinesq_Modes3D.edp';
    
         
    case('axicompcomplex')
         % AxiCompCOMPLEX flow (Whistling jet, etc...)

        % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING Axi compressible COMPLEX Solver');
        if(p.Results.sym == 'A')
            symmetry = 0;
        elseif(p.Results.sym == 'S')
            symmetry = 1;
        elseif(p.Results.sym == 'N')
            symmetry = 2;
        end
        
        if(p.Results.type == 'D')
            typeEig = 0;
        elseif(p.Results.type == 'A')
            typeEig = 1;
        elseif(p.Results.type == 'S')
            typeEig = 2;
        else
            typeEig = 0;
        end
        argumentstring = [' " ' num2str(p.Results.Re) ' ' num2str(p.Results.Ma) ' ' num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' num2str(symmetry) ' ' num2str(typeEig) ' ' num2str(p.Results.nev) ' " '];
        fff =  ffMPI ;               
        solver =  'Stab_Axi_Comp_COMPLEX.edp';
         
    case('axicompcomplex_m')
         % AxiCompCOMPLEX flow (Whistling jet, etc...)

        % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING Axi compressible with azimuthal component COMPLEX Solver');
        if(p.Results.sym == 'A')
            symmetry = 0;
        elseif(p.Results.sym == 'S')
            symmetry = 1;
        elseif(p.Results.sym == 'N')
            symmetry = 2;
        end
        
        if(p.Results.type == 'D')
            typeEig = 0;
        elseif(p.Results.type == 'A')
            typeEig = 1;
        elseif(p.Results.type == 'S')
            typeEig = 2;
        else
            typeEig = 0;
        end
        argumentstring = [' " ' num2str(p.Results.Re) ' ' num2str(p.Results.Ma) ' ' num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' num2str(symmetry) ' ' num2str(typeEig) ' ' num2str(p.Results.nev) ' " '];
        fff =  ffMPI ;               
        solver =  'Stab_Axi_Comp_COMPLEX_m.edp';

    case('2dcomp')
         % 2D flow (cylinder, etc...)

            % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 2D compressible Solver');
        if(p.Results.k==0)
            argumentstring = [' " ' num2str(p.Results.Re) ' ' num2str(p.Results.Ma) ' ' num2str(real(shift)) ' ' num2str(imag(shift))... 
                                 ' ' p.Results.sym ' ' p.Results.type ' ' num2str(p.Results.nev) ' " '];
           % fff = [ ffMPI ' -np ',num2str(ncores) ]; does not work with FreeFem++-mpi
              fff = ffMPI ; 

            solver =  'Stab2D_Comp.edp';

        else
            argumentstring = [' " ' num2str(p.Results.k) ' ' num2str(p.Results.Re) ' ' num2str(p.Results.Ma) ' ' num2str(real(shift)) ' ' num2str(imag(shift))... 
                                 ' ' p.Results.sym ' ' p.Results.type ' ' num2str(p.Results.nev) ' " '];
            fff = ffMPI ; 

            solver =  'Stab2D_Comp_Modes3D.edp';
        end
    
    case('2dcompsponge')
         % 2D flow (cylinder, etc...)

            % 2D Baseflow / 2D modes
        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 2D compressible Solver');
        if(p.Results.sym == 'A')
            symmetry = 0;
        elseif(p.Results.sym == 'S')
            symmetry = 1;
        elseif(p.Results.sym == 'N')
            symmetry = 2;
        end
        
        if(p.Results.type == 'D')
            typeEig = 0;
        elseif(p.Results.type == 'A')
            typeEig = 1;
        elseif(p.Results.type == 'S')
            typeEig = 2;
        else
            typeEig = 0;
        end
        argumentstring = [' " ' num2str(p.Results.Re) ' ' num2str(p.Results.Ma) ' ' num2str(real(shift)) ' ' num2str(imag(shift))... 
                             ' ' num2str(symmetry) ' ' num2str(typeEig) ' ' num2str(p.Results.nev) ' " '];
          fff = ffMPI; %does not work with FreeFem++-mpi
          % fff = ffMPI ; 

        solver =  'Stab2D_Comp_Sponge.edp';

        
    case('2dmobile')
        % for spring-mounted cylinder
             
        SF_core_log('n',['      ### FUNCTION SF_Stability VIV : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','      ### USING 2D Solver FOR MOBILE OBJECT (e.g. spring-mounted)');
        argumentstring = [ num2str(p.Results.Re) ' ' num2str(p.Results.MASS) ' ' num2str(p.Results.STIFFNESS) ' '... 
                            num2str(p.Results.DAMPING) ' ' num2str(real(shift)) ' ' num2str(imag(shift)) ' ' p.Results.sym...
                            ' ' p.Results.type ' ' num2str(p.Results.nev) ' R ']; 
        fff = ff;
        solver = 'Stab2D_VIV.edp';
       
     case('3dfreesurfacestatic')
        % for oscillations of a free-surface problem (liquid bridge, hanging drops/attached bubbles, etc...)             
        if(p.Results.nu==0)
        SF_core_log('n',['      ### FUNCTION SF_Stability FREE SURFACE POTENTIAL : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        argumentstring = ['  ' ... //num2str(p.Results.gamma) ' ' num2str(p.Results.rhog) ' ' num2str(p.Results.GammaBAR) ' '...
        num2str(p.Results.nu) ' ' num2str(p.Results.alphaMILES) ' ' ...
        p.Results.typestart ' ' p.Results.typeend  ' ' num2str(p.Results.m) ' '... 
        num2str(p.Results.nev)  ' ' num2str(real(p.Results.shift)) ' ' num2str(imag(p.Results.shift)) ' '];
        fff = ff; % chznged 26/06/2019
        solver =  'StabAxi_FreeSurface_Potential.edp';

        else
        SF_core_log('n',['      ### FUNCTION SF_Stability FREE SURFACE VISCOUS : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        argumentstring = [' " '  num2str(p.Results.nu) ...
            ' ' p.Results.typestart ' ' p.Results.typeend  ' '...
            ' ' num2str(p.Results.m) ' ' num2str(real(p.Results.shift)) ' ' num2str(imag(p.Results.shift)) ' ' num2str(p.Results.nev) ' " '];
       
        fff = ff;
        solver =  'StabAxi_FreeSurface_Viscous.edp';

        end
    
 case('axifreesurf')
         % Axi free surface ALE (p. BONNEFIS)

        SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);
        SF_core_log('n','       ### for FREE-surface flow USING ALE solver');
        symmetry = 1;
        if(p.Results.sym == 'A')
            symmetry = -1;
        elseif(p.Results.sym == 'S')
            symmetry = 1;
        end
        NsF = 20; % number of terms in the Fourier expansion
        argumentstring = [' " 1 ' num2str(p.Results.m) ' ' num2str(symmetry) ' ' ...
        num2str(p.Results.Oh) ' ' num2str(p.Results.We) ' ' num2str(p.Results.nev) ' ' ... 
        num2str(real(shift)) ' ' num2str(imag(shift)) ' ' num2str(NsF) ' '  p.Results.ALEoperator ' " '];
        fff =  ffMPI ; 
        if p.Results.m==0 && symmetry ==1
            solver =  'Stab_Axi_Surface_ALE_Fourier_m0.edp';
        else
            solver =  'Stab_Axi_Surface_ALE_Fourier.edp';
        end
    %case(...)    
    % adapt to your case !
    
    otherwise
        error(['Error in SF_Stability : "problemtype =',ffmesh.problemtype,'  not possible or not yet implemented !'])
end


%% Chapter 3 : launch the ff solver

%SF_core_log('n',['      ### FUNCTION SF_Stability : computation of ' num2str(p.Results.nev) ' eigenvalues/modes (DIRECT) with FF solver']);

if(strcmp(p.Results.solver,'default'))
    SF_core_log('n',['      ### USING STANDARD StabFem Solver ',solver]);        
else
        solver = p.Results.solver; 
        SF_core_log('n',['      ### USING CUSTOM StabFem Solver ',solver]);  

        if(strcmp(p.Results.solver,'StabAxi_COMPLEX_m0_nev1.edp')==1) 
            disp('NB USING ff instead of ff-mpi (VERY UGLY FIX TO OVERCOME A STRANGE BUG : otherwise shift-invert mode does not work with FreeFem++-mpi'); 
            fff = ff;
        end
end

    status = SF_core_freefem(solver,'parameters',argumentstring,'bin',fff);

if (status==202) 
        SF_core_log('w','SHIFT-INVERT iteration DID NOT CONVERGE !');
        ev=NaN;
        em.iter = -1;
        varargout = {ev,em}; 
        return
elseif (status>0)
        SF_core_log('e','Stop here');
end



%% Chapter 4 : post-processing

% Pick eigenvalues from file
rawData1 = myimportdata('Spectrum.txt');
EVr = rawData1(:,1);
EVi = rawData1(:,2); 
eigenvalues = EVr+1i*EVi;


% Append eigenvalues to StabStats.txt / StabStats.ff2m
if ~isfield(baseflow,'INDEXING')
  baseflow.INDEXING.soundspeed = 1; % Very ugly fix for acoustic pipe
end
  SF_WriteEVstats(eigenvalues,p.Results,baseflow.INDEXING,'StabStats');
  
% sort eigenvalues 
%           (NB the term 1e-4 is a trick so that the sorting still
%           works when eigenvalues come in complex-conjugate pairs)
    switch lower(p.Results.sort)
        case('lr') % sort by decreasing real part of eigenvalue
            [~,o]=sort(-real(eigenvalues)+1e-4*abs(imag(eigenvalues)));
        case('sr') % sort by increasing real part of eigenvalue
            [~,o]=sort(real(eigenvalues)+1e-4*abs(imag(eigenvalues)));
        case('sm') % sort by increasing magnitude of eigenvalue
            [~,o]=sort(abs(eigenvalues)+1e-4*abs(imag(eigenvalues)));
        case('lm') % sort by decreasing magnitude of eigenvalue
            [~,o]=sort(-abs(eigenvalues)+1e-4*abs(imag(eigenvalues)));
        case('si') % sort by increasing imaginary part of eigenvalue
            [~,o]=sort(imag(eigenvalues));  
        case('sia') % sort by increasing imaginary part (abs) of eigenvalue
            [~,o]=sort(abs(imag(eigenvalues))+1e-4*imag(eigenvalues)+1e-4*real(eigenvalues));
        case('dist') % sort by increasing distance to the shift
            [~,o]=sort(abs(eigenvalues-shift));  
        case('cont') % sort using continuation (to connect with previous branches)
            eigenvaluesSORT = eigenvalues;
            for i=1:length(eigenvalues)    
                [~, index] = min(abs(eigenvaluesSORT-eigenvaluesPrev(i)));
                o(i) = index;
                eigenvaluesSORT(index)=NaN;
            end
        case('no')
            o = 1:length(eigenvalues);
        otherwise 
            SF_core_log('w','sorting option not recognized');
            o = 1:length(eigenvalues);
    end
    eigenvalues=eigenvalues(o);     
    eigenvaluesPrev = eigenvalues;

% updating two previous iterations
if(strcmp(p.Results.shift,'cont')==1)
sigmaPrevPrev = sigmaPrev;
sigmaPrev = eigenvalues(1);
else
sigmaPrevPrev = eigenvalues(1);
sigmaPrev = eigenvalues(1); 
end

    if(nargout>1) %% process output of eigenmodes
    if(p.Results.nev==1)
        if (p.Results.type=='D')
            filename = SFcore_MoveDataFiles('Eigenmode.txt','EIGENMODES');
            eigenvectors=SFcore_ImportData(ffmesh,filename);
            eigenvectors.type=p.Results.type;
            %iter = eigenvectors.iter;
            SF_core_log('n',['      # Stability calculation completed, eigenvalue = ',num2str(eigenvalues),' ;']);
        elseif(p.Results.type=='A')
            filename = SFcore_MoveDataFiles('EigenmodeA.txt','EIGENMODES');
            eigenvectors=SFcore_ImportData(ffmesh,filename);
            eigenvectors.type=p.Results.type;
            %iter = eigenvectors.iter;
            SF_core_log('n',['      # Stability calculation completed (ADJOINT), eigenvalue = ',num2str(eigenvalues),' ;']);
        elseif(p.Results.type=='S'||p.Results.type=='E')
            %sensitivity=SFcore_ImportData(ffmesh,'Sensitivity.ff2m');
            filename = SFcore_MoveDataFiles('Sensitivity.txt','MISC');
            sensitivity = SFcore_ImportData(ffmesh,filename);
            iter = 1;% previous management of non convergence errors ; to be cleaned up
            if (nargout >2)
                filename = SFcore_MoveDataFiles('Eigenmode.txt','EIGENMODES');
                evD = SFcore_ImportData(ffmesh,filename);
                %evD=SFcore_ImportData(ffmesh,'Eigenmode.ff2m');
                evD.type='D';
                %iter = evD.iter;% previous management of non convergence errors ; to be cleaned up
            end
            if (nargout >3) 
                filename = SFcore_MoveDataFiles('EigenmodeA.txt','EIGENMODES');
                evA = SFcore_ImportData(ffmesh,filename);
                %evA=SFcore_ImportData(ffmesh,'EigenmodeA.ff2m');
                evA.type='A';
            end
             if (p.Results.type=='E'||(nargout >4) )
                 filename = SFcore_MoveDataFiles('Endogeneity.txt','MISC');
                 Endo = SFcore_ImportData(ffmesh,filename);
                %Endo=SFcore_ImportData(ffmesh,'Endogeneity.ff2m');
                Endo.type='S'; % useful ?
                SF_core_log('d','  # Endogeneity successfully imported');
             end
        end
        iter = 1; %TODO: WORKAROUND DUE TO THE FACT SLEPC DOES NOT GIVE ITER!!!!!
        if(iter<0) 
          %  if(verbosity>1)
          %      error([' ERROR : simple shift-invert iteration failed ; use a better shift of use multiple mode iteration (nev>1). If you want to continue your loops despite this error (confident mode) use verbosity=1 ']);
          %  else
          %      disp([' WARNING : simple shift-invert iteration failed ; continuing despite of this. If you want to produce an error (secure mode) use verbosity>1.']);
          %  end
          if strcmp(p.Results.ifdiverge,'error')
                SF_core_log('e',' ERROR : simple shift-invert iteration failed ; use a better shift of use multiple mode iteration (nev>1). If you want to continue your loops despite this error (confident mode) use verbosity=1 ');
                error('stop here');
          else
                SF_core_log('w',' Simple shift-invert iteration failed.  Parameter ''ifdiverge'' = ''continue'' so continuing despite of this.');
          end
        end  
        
    elseif(p.Results.nev>1&&p.Results.type=='D')
    eigenvectors=[];
        for iev = 1:p.Results.nev
        %egv=SFcore_ImportData(ffmesh,['Eigenmode' num2str(iev) '.ff2m']);
        filename = SFcore_MoveDataFiles(['Eigenmode' num2str(iev) '.txt'],'EIGENMODES');
        egv = SFcore_ImportData(ffmesh,filename);
        egv.type=p.Results.type;
        %eigenvectors(iev) = egv;
        eigenvectors = [eigenvectors egv];
        end
        eigenvectors=eigenvectors(o);%sort the eigenvectors with the same filter as the eigenvalues
    elseif(p.Results.nev>1&&p.Results.type=='A')
    eigenvectors=[];
    for iev = 1:p.Results.nev
        %egv=SFcore_ImportData(ffmesh,['EigenmodeA' num2str(iev) '.ff2m']);
        filename = SFcore_MoveDataFiles(['EigenmodeA' num2str(iev) '.txt'],'EIGENMODES');
        egv = SFcore_ImportData(ffmesh,filename);
        egv.type=p.Results.type;
        %eigenvectors(iev) = egv;
        eigenvectors = [eigenvectors egv];
    end
    eigenvectors=eigenvectors(o);%sort the eigenvectors with the same filter as the eigenvalues
    else
        error('ERROR');
    end
    
    end
    switch(nargout)
        case(1)
    varargout = {eigenvalues};
%     varargout = eigenvectors
        case(2)
             if(p.Results.type=='S')
                varargout = {eigenvalues,sensitivity};
             elseif(p.Results.type=='E')
                varargout = {eigenvalues,Endo};  
             else
                  varargout = {eigenvalues,eigenvectors}; 
             end
        case(3)
    error( 'number of output arguments not valid...' )
        case(4)
            if(p.Results.type=='S')
                varargout = {eigenvalues,sensitivity,evD,evA};  
            elseif(p.Results.type=='E')
                varargout = {eigenvalues,Endo,evD,evA};  
            end
        case(5)
            varargout = {eigenvalues,sensitivity,evD,evA,Endo}; 
    end
       
     % FINALLY : in spectrul explorator mode, plot the spectrum
    if(strcmp(p.Results.PlotSpectrum,'yes')==1)
        figure(100);
        %%mycmp = [[0 0 0];[1 0 1] ;[0 1 1]; [1 1 0]; [1 0 0];[0 1 0];[0 0 1]]; %color codes for symbols
        plot(real(shift),imag(shift),'o');hold on;
        for ind = 1:length(eigenvalues)
            h = plot(real(eigenvalues(ind)),imag(eigenvalues(ind)),'*');hold on;
            %%%%  plotting command for eigenmodes and callback function
            [plottitle1,plottitle2] = generateplottitle(eigenvectors(ind));            
            tt = [ ' emPLOT = SF_Load(''EIGENMODE'',''',eigenvectors(ind).filename,''') ;' ...
                   ' plottitle = { ''', plottitle1,''',''', plottitle2, '''}; '...
                   ' figure; '...
                   ' SF_Plot(emPLOT,''' p.Results.PlotSpectrumField ''',''title'',plottitle' mycell2str(p.Results.PlotModeOptions) ');'  
                  ];
               set(h,'buttondownfcn',tt);
            
            %if ~SF_core_getopt('isoctave')
            %  ax = gca;
            %  ax.XAxisLocation = 'origin';
            %  ax.YAxisLocation = 'origin';
            %end

        end
    xlabel('\sigma_r');ylabel('\sigma_i');
    title('Spectrum (click on eigenvalue to display corresponding eigenmode)');
    end
    
   % eventually clean working directory from temporary files
   SF_core_arborescence('cleantmpfiles')
    
   SF_core_log('n','END Function SF_Stability :');  
    
end


function str = mycell2str(cell)
% This function will convert a cell array into a string 
% (for interactive spectrum explorer with option 'plotspectrum'='yes')
%
str = '';
for i = 1:length(cell)
    str = [str ','];
    if(ischar(cell{i}))
        str = [ str, '''' cell{i}  '''' ];
    end
    if(isnumeric(cell{i}))
        str = [ str, '[' num2str(cell{i}) ']' ];
    end
end
str = [str ''];
end


function mymyrm(filename)
SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') filename]);
end

function data = myimportdata(filename)
data = importdata([SF_core_getopt('ffdatadir') filename]);
end

%function [] = myaddParameter(p,field,baseflow,default)
%    if(isfield(baseflow,field)) 
%        defaultvalue = baseflow.Re;
%    else
%        defaultvalue = default;
%    end
%    if isnumeric(default)
%        expectedtype = @isnumeric;
%    end
%    addParameter(p,field,default,expectedtype);
%end
    
function [plottitle1,plottitle2] = generateplottitle(em)
    if isfield(em,'INDEXING') && isfield(em.INDEXING,'eigenvalue')
        eigenvalue = em.INDEXING.eigenvalue;
    elseif isfield(em,'INDEXING') && isfield(em.INDEXING,'lambda')
        eigenvalue = em.INDEXING.lambda;
    else
        eigenvalue = 0;
        SF_core_log('w','Could not found a field "eigenvalue" in metadata associated to eigenmode to generate the title of the figures')
    end
    plottitle1 =  [em.datatype ' for \lambda = ', num2str(eigenvalue) ];
    plottitle2 = '';
    if isfield(em,'INDEXING')
        indexnames = fieldnames(em.INDEXING);
        for ind = indexnames'
            if ~strcmp(ind{1},'eigenvalue')
               plottitle2 = [plottitle2,ind{1},' = ',num2str(em.INDEXING.(ind{1})) ' ; '];
            end
        end
    plottitle2 = plottitle2(1:end-3);
    end
end


  