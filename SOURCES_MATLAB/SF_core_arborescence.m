function [] = SF_core_arborescence(mode)
%
% Function SF_core_arborescence
% (partly redundant with SFcore_CleanDir)
%
%>
%> @file SOURCES_MATLAB/SF_core_arborescence.m
%> @brief Matlab function to create or clean clean the StabFem arborescence
%>
%> Usage: 
%> [] = SF_core_arborescence('create') -> creation of arborescence
%>          
%> [] = SF_core_arborescence('clean') -> partial clean of arborescence accected by SF_core_getopt('storagemode') 
%>    'MESHES' is cleaned if SF_core_getopt('storagemode') =1
%>    'BASESFLOWS','DNSFLOWS','EIGENMODES','MISC',... are cleaned if ls storagemode = 1,2
%>    nothing is cleared if SF_core_getopt('storafemode') = 3
%> 
%> [] = SF_core_arborescence('cleanall') -> full clean of arborescence (including 'MESHES' ; OBSOLETE)
%>          
%> [] = SF_core_arborescence('cleantmpfiles') -> clean the tmp files in work dir
%> 
%> remarks : 
%> 1/ Function disabled when working in the base dir (ffdatadir = './') or debug mode (verbosity>5) 
%>
%> 2/ operation is affected by the global variable SF_core_getopt('storagemode') as follows :
%>          0 : no storage of data files (everything is kept in base directory workdir, nothing is put in subfolders)
%>          1 : stores results but cleans everything each time the
%>              mesh is reconstructed (recommended for static free-surface problems)
%>          2 : stores everything, each time the mesh is
%>              reconstructed we clean everything but keep a copy of each new mesh in subdirectory MESHES 
%>              (default mode, recommended for cases where the mesh is only adapted at the start) 
%>          3 : stores and keeps everything (SF_BaseFlow is automatically in 'force' mode)     

ffdatadir = SF_core_getopt('ffdatadir');

if isempty(ffdatadir)||strcmp(ffdatadir,'./')||strcmp(ffdatadir,'.') 
    SF_core_log('nnn','SF_core_arborescence disabled when using  "./" as workdir ');
    return
end

%if (SF_core_getopt('verbosity')>5)&&~strcmp(mode,'create')
%    SF_core_log('d','SF_core_arborescence disabled in debug mode ');
%    return
%end

SF_core_log('d','entering SF_core_arborescence');

if(nargin==0)
    mode = 'create';
end



SFDirList = {'BASEFLOWS','MEANFLOWS','EIGENMODES','MISC',...
             'FORCEDFLOWS','DNSFIELDS'};

switch(mode)
    case('create')
        SF_core_log('d','CREATE in SF_core_arborescence');
       % SF_core_syscommand('rmdir',ffdatadir); % NOOOO !
        SF_core_syscommand('mkdir',ffdatadir);
        SF_core_syscommand('mkdir',[ffdatadir 'MESHES']);
        SFDirList = [SFDirList {'STATS', 'FORCEDSTATS'}];
        for i = 1:length(SFDirList)
            %[ffdatadir SFDirList{i} ]
            SF_core_syscommand('mkdir',[ffdatadir SFDirList{i} ]);
        end
        
        fid = fopen('SF_Geom.edp','w');
        fprintf(fid,'%s','// File automatically created by StabFem');
        fclose(fid);
        
   
    case ('clean')
       if(SF_core_getopt('storagemode')==1||SF_core_getopt('storagemode')==2)    
       SF_core_log('d','CLEAN DATA DIRECTORIES in SF_core_arborescence');
       for i = 1:length(SFDirList)
            SF_core_syscommand('cleandir',[ffdatadir SFDirList{i} ]);
            SF_core_syscommand('mkdir',[ffdatadir SFDirList{i} ]);
       end 
       if(SF_core_getopt('storagemode')==1)    
       SF_core_log('d','CLEAN also MESH DIRECTORIES in SF_core_arborescence');
            SF_core_syscommand('cleandir',[ffdatadir 'MESHES' ]);
            SF_core_syscommand('mkdir',[ffdatadir 'MESHES' ]);
       end
       else
          SF_core_log('d','cLean dir DISABLED in SF_core_arborescence') 
       end
       
      case('cleanall')
            if(SF_core_getopt('storagemode')==1||SF_core_getopt('storagemode')==2)  
            %SF_core_log('l','CLEANALL in SF_core_arborescence (legacy; better use clean and SF_core_getopt('storagemode') to control)');
            SF_core_log('n',['CLEANALL in SF_core_arborescence ; working dir ',SF_core_getopt('ffdatadir'),' is now empty']); 
            SFDirListALL = ['MESHES' SFDirList];
       for i = 1:length(SFDirListALL)
            SF_core_syscommand('cleandir',[ffdatadir SFDirListALL{i} ]);
            SF_core_syscommand('mkdir',[ffdatadir SFDirListALL{i} ]);
       end
       SF_core_log('nnn',['Cleaning ALL arborescence ',SF_core_getopt('ffdatadir')]); 
       else
          SF_core_log('nnn','cLean dir DISABLED in SF_core_arborescence with storagemode = 3') 
       end
          
    case 'cleantmpfiles'
        
        SF_core_log('d','CLEANTMPFILES in SF_core_arborescence');
        if SF_core_getopt('verbosity')<0 %(TEMPORARILY DISABLED ! should be 6) 
           % SFcore_CleanDir('TMPFILES');
            thedir = dir(SF_core_getopt('ffdatadir'))
            for i = 1:length(thedir)
                [SF_core_getopt('ffdatadir') thedir(i).name]
                if (~thedir(i).isdir)&&(~strcmp(thedir(i).name,'problemtype.ff2m'))
                    [SF_core_getopt('ffdatadir') thedir(i).name]
                    SF_core_syscommand('rm',[SF_core_getopt('ffdatadir') thedir(i).name]);
                end
            end
            SF_core_log('nnn',['Cleaning directory ',SF_core_getopt('ffdatadir')]);
        else
            SF_core_log('d',' NB cleaning of temporary files is disabled in debug mode');
        end
    case default
        SF_core_log('e','mode operation in SF_core_arborescence not recognized');
end            
   
%if strcmp(mode,'cleanall')
%if strcmp(mode,'cleanall')
    %SFcore_CleanDir('MESHES_FORCE');
%    SF_core_syscommand('cleandir',[ffdatadir 'MESHES']);
%    SF_core_syscommand('mkdir',[ffdatadir 'MESHES']);
%end
%    SF_core_syscommand('cleandir',[ffdatadir 'MESHES']);
%    SF_core_syscommand('mkdir',[ffdatadir 'MESHES']);
%end

%    if(isunix)
%        system(['echo "// File automatically created by StabFem" > SF_Geom.edp']);
%    end
 fid = fopen('SF_Geom.edp');
 fprintf(fid,'%s','// File automatically created by StabFem');
 fclose(fid);
    
    % a file SF_Geom should be present, even if blank 
    % a file SF_Geom should be present in some legacy cases, even if blank 
    % (this line is here for retrocompatibility,  not sure it is still userul...)
    
SF_core_log('d','leaving SF_core_arborescence');
end
