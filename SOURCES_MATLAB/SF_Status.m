function sfstats = SF_Status(type,mode)
%>
%> This function allows to generate a useful summary of data present in the
%> working directories.
%> This can be useful to reconstruct structures from available data files,
%> for instance if you did a 'clear all' but data files are still present... 
%>
%> USAGE :
%> 1. SF_Status(DIRECTORY)
%>  -> check the content of folder ffdatadir/DIRECTORY
%> 
%> you can chose between the following directories :
%>      'MESHES'          (meshes available and corresponding baseflows)
%>      'BASEFLOWS'       (baseflows compatible with the current mesh)
%>      'MEANFLOWS'       (meanflows/HB flows compatible with the current mesh)
%>      'DNSFLOWS'        (dns snapshots compatible with the current mesh)
%>      'EIGENMODES'      (eigenmodes compatible with the current mesh)
%>      'MISC'            (miscleanous fields such as additions, masks, ...)
%>
%> 2. SF_Status()  [ or SF_Status('ALL') ]
%>  -> check all subfolders of ffdatadir
%>
%> 3. sfstats=SF_Status('ALL','quiet')
%>  (-> this last method is only for internal usage by SF_Load)
%>      sfstats has fields [LastMesh, LastAdaptedBaseFlow,LastComputedBaseFlow,LastComputedMeanFlow,LastDNS]
%>
%> NB this functions uses nestedSortStruct and SortStruct taken from
%> mathworks (to sort the files by dates)
%>

%% Options

if(nargin==0)
    type = 'all';
    mode = 'verbose';
end
if(nargin==1)
    mode = 'quiet';
end
if strcmp(mode,'verbose')
    isdisp=1; 
else
    isdisp=0;
end

if strcmpi(type,'cleanall')
  SF_core_arborescence('cleanall');
  return
end

if strcmpi(type,'clean')
  SF_core_arborescence('clean');
  return
end

ffdatadir = SF_core_getopt('ffdatadir');


%% checking problem type     
    
fileToRead2 = [SF_core_getopt('ffdatadir'), '/problemtype.ff2m'];
if(exist(fileToRead2,'file'))
    SF_core_log('d', ['FUNCTION  SF_Status.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(fileToRead2);
    sfstats.problemtype = m2.problemtype;
else
     SF_core_log('d', ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

%% MESHES
if (strcmpi(type,'meshes') || strcmpi(type,'all') )
        thedir = dir([ffdatadir, 'MESHES/*.msh']); 
       % ii = 1;
       % for i = 1:length(thedir0)
       %   name = thedir0(i).name
       %   if ~(length(name)>7&&strcmp(name(end-6:end),'aux.msh')) 
       %     thedir(ii)=thedir0(i)
       %     ii=ii+1;
       %   end     
       % end
       % thedir
    if ~isempty(thedir)
        mymydisp(isdisp,'#################################################################################');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', ffdatadir, 'MESHES :']);
        mymydisp(isdisp,'     (list of meshes previously created/adapted ; couples of .msh/.ff2m files )');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'Index | Name                      | Date                  | Nv (nb of vertices)'); %| (Number of vertices) ');
        thedir = nestedSortStruct(thedir, 'datenum');
        ii = 0;
        for i = 1:length(thedir)
          name = thedir(i).name;
          if ~(length(name)>7&&strcmp(name(end-6:end),'aux.msh')) 
            % warning : there may be some files with form ###_aux.msh which should not be counted
            ii = ii+1;
            date = thedir(i).date;
            fid = fopen([ffdatadir, 'MESHES/', name], 'r');
            headerline = textscan(fid, '%f %f %f', 1, 'Delimiter', '\n');
            nv(ii) = headerline{1};           
            mymydisp(isdisp,[num2str(ii), blanks(max(5-length(num2str(ii)),1)), ' | ', name, blanks(max(25-length(name),1)), ' | ', date, blanks(max(20-length(date),1)), ' | ', num2str(nv(ii))]);
            sfstats.MESHES(ii).MESHfilename = thedir(i).name;
            sfstats.MESHES(ii).date = date;
            sfstats.MESHES(ii).nv = nv(ii);
            fclose(fid);
          end  
        end
        
%        sfstats.LastMesh = [ffdatadir, 'MESHES/',name];
    else
%        sfstats.LastMesh = [];
        sfstats.MESHES = [];
        mymydisp(isdisp,'#################################################################################');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,['.... NO MESH FOUND IN DIRECTORY ', ffdatadir, 'MESHES :']);
        mymydisp(isdisp,' ');
    end
    
if(strcmpi(type,'meshes')||strcmpi(type,'all'))
    [sfstats,flag] = STATUS(sfstats,'MESHES',isdisp);
 
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A PREVIOUSLY COMPUTED MESH/BASEFLOW')
        mymydisp(isdisp,' simply type the following command  (where index is the number of the desired mesh)');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'    bf = SF_Load(''MESH'',index)');
        mymydisp(isdisp,' ');
end 
        mymydisp(isdisp,'#################################################################################');
       % mymydisp(isdisp,' ');
%    end  
end


%% BASEFLOWS
if(strcmpi(type,'baseflows')||strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'BASEFLOWS',isdisp);
end

%% Checking directory EIGENMODES
if(strcmpi(type,'eigenmodes')||strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'EIGENMODES',isdisp);
end

%% checking statistics file

if exist([ffdatadir 'STATS/StabStats.txt'],'file')
  Stabstats = SFcore_ImportData([ffdatadir 'STATS/StabStats.txt']);
  if ~isempty(Stabstats)
    neig = length(Stabstats.lambda);
    mymydisp(isdisp,' ');
    mymydisp(isdisp,[' You have previously computed ',num2str(neig), ' eigenvalues in this session'] );
    mymydisp(isdisp,[' The full list will be available as field  ''EIGENVALUES'' of this function''s result']);
    mymydisp(isdisp,' ');
    Stabstats = rmfield(Stabstats,{'filename','DataDescription','datatype','datastoragemode','datadescriptors'});
    sfstats.EIGENVALUES = Stabstats;
  else 
    sfstats.EIGENVALUES = [];
    mymydisp(isdisp,['Nothing found in file EIGENMODES/StabStats.stat']); 
  end
else  
  sfstats.EIGENVALUES = [];
  mymydisp(isdisp,['Not found any file EIGENMODES/StabStats.stat']); 
end


%% checking THRESHOLD statistics file

if exist([ffdatadir 'STATS/StatThreshold.txt'],'file')
  Stabstats = SFcore_ImportData([ffdatadir 'STATS/StatThreshold.txt']);
  if ~isempty(Stabstats)
    neig = length(Stabstats.lambda);
    mymydisp(isdisp,' ');
    mymydisp(isdisp,[' In these computations you have identified ',num2str(neig), ' neutral ceigenvalues'] );
    mymydisp(isdisp,[' The full list will be available as field  ''THRESHOLD'' of this function''s result']);
    mymydisp(isdisp,' ');
    Stabstats = rmfield(Stabstats,{'filename','DataDescription','datatype','datastoragemode','datadescriptors'});
    sfstats.THRESHOLDS = Stabstats;
  else 
    sfstats.THRESHOLDS = [];
    mymydisp(isdisp,['Nothing found in file EIGENMODES/StabStats.stat']); 
  end
else  
  sfstats.THRESHOLDS = [];
  mymydisp(isdisp,['Not found any file EIGENMODES/StabStats.stat']); 
end


%% DNSFLOWS
if(strcmpi(type,'dnsflows') || strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'DNSFIELDS',isdisp);
end


if exist([ffdatadir 'STATS/dns_Stats.txt'],'file')
  DNSstats = SFcore_ImportData([ffdatadir 'STATS/dns_Stats.txt']);
  if ~isempty(Stabstats)
    neig = length(DNSstats.t);
    mymydisp(isdisp,' ');
    mymydisp(isdisp,[' Detected a DNS statistics file STATS/dns_Stats.txt']);
    mymydisp(isdisp,[' You have previously computed ',num2str(neig), ' time steps of DNS'] );
    mymydisp(isdisp,[' The full list will be available as field  ''DNSSTATS'' of this function''s result']);
    mymydisp(isdisp,' ');
    Stabstats = rmfield(DNSstats,{'filename','DataDescription','datatype','datastoragemode','datadescriptors'});
    sfstats.DNSSTATS = DNSstats;
  else 
    sfstats.DNSSTATS = [];
    mymydisp(isdisp,['Nothing found in file STATS/dns_Stats.txt']); 
  end
else  
  sfstats.DNSSTATS = [];
  mymydisp(isdisp,['Not found any file EIGENMODES/StabStats.stat']); 
end




%% DNSSTATS (only check size)
%if(strcmpi(type,'misc') || strcmpi(type,'all'))
%    sfstats = STATUS(sfstats,'DNSSTATS',isdisp);
%end


%% FORCEDFLOWS
if(strcmpi(type,'dnsflows') || strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'FORCEDFLOWS',isdisp);
end


%% FORCEDSTATS (only check size)
%if(strcmpi(type,'misc') || strcmpi(type,'all'))
%    sfstats = STATUS(sfstats,'FORCEDSTATS',isdisp);
%end

%% MEANFLOWS/HB
if(strcmpi(type,'meanflows') || strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'MEANFLOWS',isdisp);
end
   
%% MISC
if(strcmpi(type,'misc') || strcmpi(type,'all'))
    sfstats = STATUS(sfstats,'MISC',isdisp);
end


mymydisp(isdisp,'#################################################################################');
 
 if  (flag==1)
      SF_core_log('w','WARNING : number of Meshes and Baseflows in directory MESHES differ !');
end


%% legacy       
  if ~isfield(sfstats,'MESHES')||isempty(sfstats.MESHES)
    sfstats.status = 'new';
  else
    sfstats.status = 'existent';
  end

  
  
%% end of function

end 

%% Function STATUS

function [sfstats,flag] = STATUS(sfstats,folder,isdisp)
    flag=1;
     thedir = dir([SF_core_getopt('ffdatadir'), folder,'/*.txt']);    
     if ~isempty(thedir)
        mymydisp(isdisp,'#################################################################################');
        mymydisp(isdisp,' ');
        if ~strcmp(folder,'MESHES')
          mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', SF_core_getopt('ffdatadir'), folder]);
          mymydisp(isdisp,'     (couples of .txt/.ff2m files )');
        else
          mymydisp(isdisp,'     (list of base flows associated to newly computed meshes ; couples of .txt/.ff2m files )');
          if length(thedir)==length(sfstats.MESHES)
              flag=0;
              %SF_core_log('w','WARNING : number of Meshes and Baseflows in directory MESHES differ !');
          end
        end
        mymydisp(isdisp,' ');
        thestring = [' Index',' | ',SFpad('Name',30),' | ',SFpad('Type',12),' | ', SFpad('Date',20)];
        metadata = SFcore_ImportData([SF_core_getopt('ffdatadir'), folder,'/',thedir(end).name],'metadataonly');
        if isfield(metadata,'INDEXING')
            indexnames = fieldnames(metadata.INDEXING);
            for ind = indexnames'
                thestring = [thestring ,' | ', SFpad(ind{1},10)];
                if(imag(metadata.INDEXING.(ind{1}))~=0)
                    thestring = [thestring, blanks(6)];
                end
            end
        else
            indexnames = [];
        end
        mymydisp(isdisp,thestring);
        thedir = nestedSortStruct(thedir, 'datenum') ;       
        for i = 1:length(thedir)
            name = thedir(i).name;
            date = thedir(i).date;
            metadata = SFcore_ImportData([SF_core_getopt('ffdatadir'), folder,'/',thedir(i).name],'metadataonly');
            if isfield(metadata,'datatype')
                datatype = metadata.datatype;
            else
                datatype = 'unknown';
            end
            thestring = [' ',SFpad(num2str(i),5),' | ', SFpad(name,30),' | ', SFpad(datatype,12), ' | ', SFpad(date,20) ];
           
            if isfield(metadata,'INDEXING')
                for ind = indexnames'
                    if isfield(metadata.INDEXING,ind{1})
                        value = metadata.INDEXING.(ind{1});
                        if(imag(value)==0)
                            thestring = [thestring ,' | ', SFpad(num2str(value),10)];
                        else
                            thestring = [thestring ,' | ', SFpad(num2str(value),16)];
                        end
                    else
                        thestring = [thestring ,' | ', SFpad(num2str(value),10)];
                    end
                end
             end
             mymydisp(isdisp,thestring);
           
            sfstats.(folder)(i).filename = name;
            if isfield(metadata,'INDEXING')    
                fff = fieldnames(metadata.INDEXING);
                for jj =  1:length(fff)
                    ff = fff{jj};
                    sfstats.(folder)(i).(ff) =metadata.INDEXING.(ff);
                end
            end
        end
      if ~strcmp(folder,'MESHES')
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A FIELD FROM THIS DIRECTORY')
        mymydisp(isdisp,' simply type the following command  (where index is the number of the desired field)');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,['    bf = SF_Load(''',folder,''',index)']);
        mymydisp(isdisp,' ');
      end
     else
         if ~isfield(sfstats,folder)
             sfstats.(folder) = [];
         end
       % mymydisp(isdisp,'#################################################################################');
       % mymydisp(isdisp,' ');
        mymydisp(isdisp,['.... NO DATA FILES FOUND IN DIRECTORY ', SF_core_getopt('ffdatadir'), folder]);
       % mymydisp(isdisp,' ');
     end
end     

%%
function mymydisp(isdisp,string)
if(isdisp)
    disp(string);
end
end

function Nfiles = countfiles(directory,suffix)
  thedir = dir([directory,'/*',suffix]);
  Nfiles = length(thedir)
  if strcmp(suffix,'.msh')
    thedir = dir([directory,'/*_aux.msh']);
    Nfilesaux = length(thedir)
    Nfiles = Nfiles-Nfilesaux;
  end
end
    
    
    
    