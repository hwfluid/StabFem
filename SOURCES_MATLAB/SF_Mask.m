function Mask = SF_Mask(mesh,params)
%
% function SF_Mask
% 
% The role of this function is to create an "adaptation mask", i.e. a set
% of data which, when using subsequently with SF_Adapt, will force the mesh
% adaptor to respect a minimum grid size (delta) within a given rectancular 
% domain defined as [Xmin Xmax] x [Ymin Ymax]. 
% 
% USAGE :
%   Mask = SF_Mask(mesh,[Xmin Xmax Ymin Ymax delta] )
%
% This program is part of the StabFem project distributed under gnu licence.

 if(strcmpi(mesh.datatype,'mesh')==0)
       % first argument is most likely a base flow
       mesh = mesh.mesh;
 end

   SFcore_MoveDataFiles(mesh.filename,'mesh.msh','cp');
   
% We use SF_Launch ; it would be better to change for SF_core_freefem
%Mask = SF_Launch('AdaptationMask.edp','Type','rectangle','Params',params,'Mesh',mesh,'DataFile',[SF_core_getopt('ffdatadir') 'Mask.ff2m']);
optionstring = [ 'rectangle ' num2str(params(1)) ' ' num2str(params(2)) ' ' ...
                  num2str(params(3)) ' ' num2str(params(4)) ' ' num2str(params(5)) ];
                  
SF_core_freefem('AdaptationMask.edp','parameters',optionstring);  


% We import using the new method 
filename = SFcore_MoveDataFiles('Mask.txt','MISC');
Mask = SFcore_ImportData(mesh,filename);

end