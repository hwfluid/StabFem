%%
% ------------------
% -- SETTING PATH --
% ------------------
addpath('../SOURCES_MATLAB/'); 
% warning : must put absolute path for octave ! This is done below  
%%
% ------------------------------
% -- DEFINING STABFEM OPTIONS --
% ------------------------------
% The structure of the current section copies that of SF_core_generateopts
% but does not require user-machine interactions. In case of doubt about
% expected options, the autorun exits with an status code = 1.

global sfopts

 % - Initializing sfopts -
if ~isempty(sfopts)
    warning('This autorun script expects sfopts to be empty. Its current content will be removed.')
end
sfopts = struct();
%
%% - Testing the current environment and defining sfopts -
%sfopts.verbosity = uint8(3); // useless here
%
%if ~isunix || ismac
%    warning('This autorun is meant to run on a linux platform.')
%    1
%    %exit(1)
%end
sfopts.platform = 'linux'; % nb mac works like a linux
%
sfopts.isoctave = (exist('OCTAVE_VERSION', 'builtin')~=0);
if(sfopts.isoctave) 
      disp('Detected octave');
      struct_levels_to_print(0);
  else
      disp('Detected Matlab');
  end

[s,~] = system('git --version');
if s==0
    sfopts.gitavailable = true;
end

if sfopts.gitavailable
    [s,~] = system('git rev-parse --is-inside-work-tree');
    if s==0
        sfopts.gitrepository = true;
    end
end

sfopts.storagemode = 2;

%if sfopts.gitavailable
%    [s,t] = system('git rev-parse --show-toplevel');
 %   if s==0
 %       sfroot = t(1:end-1);
%    end
%else
%    % Careful: this sfroot definition only works if current directory
%    % is AUTORUN.
%    sfroot = [pwd '/..'];
%end
%sfopts.sfroot = sfroot;

% DAVID : problems with octave, doing in a simpler way
currentdir = pwd;
sfopts.sfroot = currentdir(1:end-8); % (to remove '/AUTORUN' )
addpath([sfopts.sfroot '/SOURCES_MATLAB/']);



if(ismac)
    s=0;
    t='/usr/local/bin/FreeFem++'; % for David's mac
else
    [s,t] = SF_core_syscommand('which', 'FreeFem++');
end

if s==0
    [ffroot,~,~] = fileparts(t);
else
    warning('This autorun expects FreeFem++ binary to be in the current path.')
    exit(1)
end
sfopts.ffroot = ffroot;

% these ones are important too...
    sfopts.ffdir = [sfopts.sfroot '/SOURCES_FREEFEM/']; 
    sfopts.ffincludedir = [sfopts.ffdir 'INCLUDE'];
    sfopts.ffloaddir = [sfopts.ffdir 'LOAD'];


%
%% a number of other options not useful at this stage
% sfopts.ffdir = [sfopts.sfroot '/SOURCES_FREEFEM/'];  not useful yet 
%sfopts.ffarg = '-nw -v 0';
%sfopts.ffargDEBUG = '-v 1000';
%
%sfopts.ffdir = [sfopts.sfroot '/SOURCES_FREEFEM/']; 
%
%sfopts.ffarg = '-nw -v 0'; 
%sfopts.ffargDEBUG = '-v 10';
%sfopts.ffdatadir = './WORK/';
%
%% SF_core_opts('write'); % Better not to do that ?



 %if(sfopts.isoctave) 
 %     struct_levels_to_print(1);
 %   end
 % sfopts
