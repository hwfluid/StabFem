
% ================================================
% ==== AUTORUN SCRIPT USING SF_CORE FUNCTIONS ====
% ================================================

% ---------------------------
% -- LIST OF AUTORUN CASES --
% ---------------------------

AUTORUN_LIST.short = {'STABLE_CASES/EXAMPLE_Lshape',...
    'STABLE_CASES/CYLINDER_VIV','STABLE_CASES/DNS_CYLINDER',...
    'STABLE_CASES/ACOUSTICS_PIPES'};
%'STABLE_CASES/SLOSHING'

AUTORUN_LIST.long = {'STABLE_CASES/EXAMPLE_Lshape','STABLE_CASES/CYLINDER',...
    'STABLE_CASES/SLOSHING' 'STABLE_CASES/DNS_CYLINDER',...
    'STABLE_CASES/BLUNTBODY_IN_PIPE','STABLE_CASES/LiquidBridges',...
    'STABLE_CASES/ROTATING_POLYGONS','STABLE_CASES/WhistlingHole',...
    'STABLE_CASES/ACOUSTICS_PIPES', 'STABLE_CASES/CYLINDER_VIV'};
% NB the LONG list should be set automatically depending on hoe

AUTORUN_LIST.cylinder = {'STABLE_CASES/CYLINDER'};

%%
% ------------------------------
% -- DEFINING STABFEM OPTIONS --
% ------------------------------
% The structure of the current section copies that of SF_core_generateopts
% but does not require user-machine interactions. In case of doubt about
% expected options, the autorun exits with an status code = 1.

SF_core_start('staticopts', @errorIfStaticRequired);

%%
% --------------------------
% -- SELECTING AN AUTORUN --
% --------------------------

% Pulling StabFem AutoRun Version from env. variables.
sfarver = getenv('SFARVER');
if isempty(sfarver)
    sfarver='short';
end

if ~isfield(AUTORUN_LIST,sfarver)
    warning('The requested autorun is not defined.')
    exit(1)
end



case_list = AUTORUN_LIST.(sfarver);

%%
% ----------------------
% -- RUNNING AUTORUNS --
% ----------------------

numsuccess = 0;
numfailure = 0;
numerror = 0;

cd(SF_core_getopt('sfroot'));

list_status = zeros(1,numel(case_list));
list_stacks = cell(1,numel(case_list));

for i=1:numel(case_list)
    SF_core_log('n', sprintf('\nAUTORUN test number %i: %s\n',i,case_list{i}), true);
    
    try
        cd(case_list{i});
        a = autorun();
        if a==0
            numsuccess = numsuccess + 1;
            list_status(i) = 0;
        else
            numfailure = numfailure + 1;
            list_status(i) = 1;
        end
    catch err
        numerror = numerror + 1;
        list_status(i) = 2;
        if(SF_core_getopt('isoctave'))
            list_stacks{i} = 'error (not yet handled with octave)';
        else
            list_stacks{i} = getReport(err);
        end
        
    end
    cd(SF_core_getopt('sfroot'));
end

% ---------------------------
% -- SHOWING FINAL RESULTS --
% ---------------------------
SF_core_log('n', sprintf('\n== AUTORUN SUMMARY ==\n'), true);
SF_core_log('n', sprintf('NUMBER OF SUCCESSES: %i', numsuccess), true);
SF_core_log('n', sprintf('NUMBER OF PRECISION ISSUES: %i', numfailure), true);
SF_core_log('n', sprintf('NUMBER OF FATAL ERRORS: %i', numerror), true);

SF_core_log('n', sprintf('\n== AUTORUN DETAILS ==\n'), true);
for i=1:numel(case_list)
    switch list_status(i)
        case 0
            msg = 'SUCCESS';
        case 1
            msg = 'PRECISION';
        case 2
            msg = 'ERROR';
    end
    SF_core_log('n', sprintf('\t%s: %s', case_list{i}, msg), true);
    switch list_status(i)
        case 2
            SF_core_log('n', list_stacks{i}, true);
            
        otherwise
    end
end

cd AUTORUN/

sfarkeep = getenv('SFARKEEP');
if isempty(sfarkeep)
    sfarkeep=false;
end
sfarkeep=boolean(sfarkeep);

if ~sfarkeep
    if numerror>0
        exit(2)
    elseif numfailure>0
        exit(1)
    else
        exit(0)
    end
end

function r = errorIfStaticRequired()
if ~SF_core_isopt('sfroot') || ~SF_core_isopt('ffroot')
    warning('The autorun expects all environment to be automatically defined.');
    r = false;
else
    r = true;
end
end
