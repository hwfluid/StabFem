# StabFem

## *Latest news (17/07/2019)*




The showcase of the project, with several examples and tuturials is 
[here :](https://stabfem.gitlab.io/StabFem/)



A presentation of the main functionalities of the StabFem project was presented recently
(FreeFem++ days, dec. 13, 2018). The main document of the presentation is available
[here](https://gitlab.com/stabfem/StabFem/blob/master/99_Documentation/PRESENTATIONS/Beamer_13dec2018_handout.pdf).
The presentation is also based on three commented programs : [Basic example](https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape.pdf),
[acoustic pipes](https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/ACOUSTICS_PIPES/html/Acoustic%20field%20in%20a%20pipe%20with%20harmonic%20forcing%20at%20the%20bottom.pdf),
and [DNS cylinder](https://gitlab.com/stabfem/StabFem/blob/master/DEVELOPMENT_CASES/CYLINDER_DNS/html/SCRIPT_DNS_EXAMPLE.pdf).


 A presentation of the main functionalities of the project 




## General description of the project

StabFem is a set of programs to perform Global Stability calculations in Fluid Mechanics, which is developed 
for both research and education purposes.

The project is multi-system (linux, macOS, Windows), and based on two softwares :

- The finite-element software FreeFem++ is used to generate the meshes, construct the operators
and solve the various linear problems involved in the computation.

- Matlab/Octave is used as a driver to monitor the computations in terminal or script mode and as a graphical interface to plot the results.

The classes of problems currently integrated are as follows:
- Incompressible flows (wakes and jets, flow through conduits, etc...)
- Compressible flows,
- Fluid-structure interactions for rigid bodies (spring-mounted objects or freely falling objects),
- Boussinesq flows (stratified flows, Rayleigh-Taylor convection, ...)
- Static free-surface problems (sloshing problems, liquid bridges, etc..)
- Free-surface flows with deformable surfaces using Arbitrary Lagrangian Eulerian (ALE) framework,
- Linear acoustics,
- (...)

The kind of computations currently implemented comprises :
- Computation of steady equilibrium solutions (base flow) using  Newton iteration, including arclength continuation,
- Computation of eigenvalue/eigenmodes, including interactive exploration of the spectrum, adjoint and stuctural sensitivity approaches, 
- Direct Numerical simulation (DNS)
- Computation of nonlinear limit cycles using Harmonic Balance approach, 
- Powerful and versatile mesh adaptation, 
- (...)

Currently, 3D flow can be handled, but geometries have to be 2D or axisymmetric.


## Example

Here is an example of the sequence of commands you should type in a Matlab terminal (or in a Matlab script)
to compute a base flow and the leading eigenmode for Re=100 for a given geometry.

```
SF_core_start('workdir','./WORK/','verbosity',4); % set the global variables (path names, etc...) for stabfem
ffmesh = SF_Mesh('Mesh.edp','problemtype','2d'); % constructs and imports a mesh from a freefem program
bf= SF_Baseflow(ffmesh,'Re',1); % constructs a "guess" base flow for a low Re 
bf = SF_BaseFlow(bf,'Re',100) % computes a Baseflow for Re = 100
bf = Sf_Adapt(bf);      % adapts the mesh
m=1;shift=-0.1549 + 5.343i;
[ev,em] = SF_Stability(bf,'m',m,'shift',shift,'nev',1) % computes one eigenmode
SF_Plot(em,’ux.re’) % plots the real part of the axial velocity component of the eigenmode
```

## How to install and use this software ?

### Requiremements

Before installation you need to have :
- Linux (Ubuntu 18 or other), MacOS (10.0 or earlier), Windows 10 (compatibility still beta)

- FreeFem++ (version 3.57 or later)

- Octave (4.4.0 or later) OR Matlab (2017b or later)


### Installation 
- If you just want to install the current stable version, simply type the following command in terminal 
(after making sure the git command is available on your system)

```
git clone https://gitlab.com/stabfem/StabFem
```

- If you want to participare to the project the recommended procedure is to create a branch (explanations to come...)


### Initial settings

Once you have downloaded everything, open matlab (or Octave), go in the StabFem/SOURCES_MATLAB folder, 
and run the program ``SF_core_start()``. On the first run the program should set up automatically
all the required settings for proper operation.



### Instalation remarks :




Note that with Ubuntu 18 there is a little issue with the library libstdc++.so.6 which is not where Matlab looks for it
(see Instalationnotes.md for a simple solution to this problem) 



## Test-cases currently implemented :

The software is still under development. At the moment, the included cases are the following :

- CYLINDER : 

Study of the Bénard-Von Karman instability for Re>47 in the wake of a cylinder. This directory contains scripts performing the base flow computation, linear stability analysis, adjoint and sensitivity analysis, as well as a nonlinear determination of the limit cycle using "harmonic balance" method.

- DISK_IN_PIPE :

An example in axisymmetric coordinates : flow around a disk within a pipe. This directory contains a demonstrator of the software which performs the stability analysis, displays the spectrum, allows to click on eigenvalues to plot the corresponding eigenmodes, computes instability branches (eigenvalue as function of Re), and performs the weakly nonlinear analysis of the leading instability (steady, non-axisymmetric).

- LiquidBridge :

An example with a free surface : computation of the equilibrium shape of a liquid bridge and of the oscillation modes of this bridge (in the inviscid, potential case).

(see Chireux et al., Phys. Fluids 2015 for details on this case)  

(NB This case is operational but the way the curvature is computed is not optimal and should be improved...)

- POROUS_ROTATING_DISK

Flow over/across a porous disk. Work in progress.

- SQUARE_CONFINED

flow in a pipe with a square object. Work in progress.

- Example_Lshape :

A simple example to demonstrate the matlab/FreeFem exchange data format for a simple problem

### Test-cases currently not operational :

The next cases may not be operational with the present version and need to be updated to run with the latest version of StabFem.

- SPHERE_WORK :

(under development ; may not work)

- BIRDCALL :

(under work ; may not work due to recent changes in the nomenclature. If interested, get branch "Version2.0" instead of branch "Master").
 
- CYLINDER_VIV :
Study of the vortex-induced vibrations around a spring-mounted cylinder.

- STRAINED_BUBBLE :

Equilibrium shape and oscillation modes of a bubble within a uniaxial straining flow.





## Authors :

The Matlab part of the software is by D. Fabre (2017).

The FreeFem part incorporates a number of sources from J. Tchoufag, P. Bonnefis, J. Mougel, V. Citro, F. Giannetti, O. Marquet, and many other students and collegues.

The plotting part of the interface uses the function pdeplot2dff (alternative to pdeplot and fully compatible with Octave) developped by [Chloros](https://github.com/samplemaker/freefem_matlab_octave_plot). 

At longer term, a translation of the Software in python is under reflection.

Initially uploaded on GitHub with help of Alexei Stukov on july 7, 2017. 


*The project is currently in rapid progress. Here are the latest novelties:*

## ONGOING WORK

- The project just moved to GitLab. The previous GitHub site is still operational but will soon not be maintained any more.
- A mailing list is now available. To register please go [here](https://groupes.renater.fr/sympa/info/stabfem-dev) and click on "s'abonner".
- Rewriting the core of the project is under progress (to replace the unpractical SF_Start, etc..). Please wait...
- Publishing HTML scripts on GitLabPAges will soon be available. 
- A research paper advertising the software was submitted to Rev. Appl. Mech. [See here:](https://gitlab.com/stabfem/StabFem/blob/master/99_Documentation/ARTICLE_STABFEM/ARTICLE_ASME_Accepted.pdf)
- Writing a Manual is in progress. [See here:](https://github.com/erbafdavid/StabFem/blob/master/99_Documentation/MANUAL/main.pdf)
- An automatic documentation using Doxygen is now available
- Compatibility with Octave is now supported ! Test-cases "EXAMPLE_Lshape", "CYLINDER" and "ROTATING_POLYGONS" are successful. (a few points still may have to be fixed in other cases) 
- Compatibility with windows 10 is now supported (check test-cases "EXAMPPLES_Lshape", "CYLINDER" and "POROUSDISK")
- DNS is now possible within StabFem :)




