echo "Generation of files AutomaticIndex.html  and AutomaticIndex_local.html"

# prologue from template file
cat .AutomaticIndex_Beginning.html > .local/published_html/AutomaticIndex.html
cat .AutomaticIndex_Beginning.html > .local/published_html/AutomaticIndex_local.html

# feeding contents with detected html pages
for publish_script in `grep -rnw ./*/*/*.m -e '\[\[PUBLISH\]\]' -l`; do
  script_name=$(basename ${publish_script})
  script_dir=$(dirname ${publish_script})
  case_name=$(basename ${script_dir})
  
  echo $publish_script
  echo $script_name
  echo $script_dir
  echo $case_name

  echo "Adding ${publish_script} " 
  
  linkweb="<a href=\"https://stabfem.gitlab.io/StabFem/published_html/${case_name}/${script_name%.*}.html\"> 
${script_name} </a>"
  echo "<br>" >> .local/published_html/AutomaticIndex.html
  echo $linkweb >> .local/published_html/AutomaticIndex.html

  linklocal="<a href=\"file://${PWD}/.local/published_html/${case_name}/${script_name%.*}.html\"> 
${script_name} </a>"
  echo "<br>" >> .local/published_html/AutomaticIndex_local.html
  echo $linklocal >> .local/published_html/AutomaticIndex_local.html
  
done

# epilogue from template file
cat .AutomaticIndex_Ending.html >> .local/published_html/AutomaticIndex.html
cat .AutomaticIndex_Ending.html >> .local/published_html/AutomaticIndex_local.html


