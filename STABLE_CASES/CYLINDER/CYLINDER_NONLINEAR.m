%%  NONLINEAR STABILITY ANALYSIS of the wake of a cylinder with STABFEM  
%
%  this scripts demonstrates how to use StabFem to study the instability
%  in the wake of a 2D cylinder in incompressible flow using 
%   NONLINEAR STABILITY ANALYSIS (NLSA)
%
%   The script reproduces all figures of section 5 in Fabre et al. (AMR, 2019)
% 
%   WARNING : for good operation this script must be launched after CYLINDER_LINEAR.m 
%
% The script performs the following calculations :
% 
% # Determination of the instability threshold and Weakly-Nonlinear analysis
% # Harmonic-Balance for Re = [REc-100]
%%

%% CHAPTER 4 : computation of weakly nonlinear expansion
%
disp(' ');
disp('######     ENTERING NONLINEAR PART       ####### ');
disp(' ');

%%
% DETERMINATION OF THE INSTABILITY THRESHOLD
disp('COMPUTING INSTABILITY THRESHOLD');
bf=SF_BaseFlow(bf,'Re',50);
[ev,em] = SF_Stability(bf,'shift',+.75i,'nev',1,'type','D');
[bf,em]=SF_FindThreshold(bf,em);
Rec = bf.Re;  Fxc = bf.Fx; 
Lxc=bf.Lx;    Omegac=imag(em.lambda);

%%
% Compute the WNL expansion
% (for this we require the adjoint)

[ev,sensitivity,em,emA] = SF_Stability(bf,'shift',1i*Omegac,'nev',1,'type','S'); % type "S" because we require both direct and adjoint
[wnl,meanflow,mode] = SF_WNL(bf,em,'Retest',47.,'Adjoint',emA); % Here to generate a starting point for the next chapter

%% 
% PLOTS of WNL predictions
% Here we plot only the lift force, other quantities will be plotted at the
% end and compared with HB


epsilon2_WNL = -0.003:.0001:.005; % will trace results for Re = 40-55 approx.
Re_WNL = 1./(1/Rec-epsilon2_WNL);
A_WNL = wnl.Aeps*real(sqrt(epsilon2_WNL));
Fy_WNL = wnl.Fyeps*real(sqrt(epsilon2_WNL))*2; % factor 2 because of complex conjugate
omega_WNL =Omegac + epsilon2_WNL*imag(wnl.Lambda) ...
                  - epsilon2_WNL.*(epsilon2_WNL>0)*real(wnl.Lambda)*imag(wnl.nu0+wnl.nu2)/real(wnl.nu0+wnl.nu2)  ;
Fx_WNL = wnl.Fx0 + wnl.Fxeps2*epsilon2_WNL  ...
                 + wnl.FxA20*real(wnl.Lambda)/real(wnl.nu0+wnl.nu2)*epsilon2_WNL.*(epsilon2_WNL>0) ;


figure(24); hold on;
plot(Re_WNL,abs(Fy_WNL),'g--','LineWidth',2);
xlabel('Re');ylabel('Fy')


pause(0.1);



%% CHAPTER 5 : SELF CONSISTENT / HB1
%

disp('SC quasilinear model on the range [Rec , 100]');
Re_HB = [Rec 47 47.5 48 49 50 52.5 55 60 65 70 75 80 85 90 95 100];

% THE STARTING POINT HAS BEEN GENERATED ABOVE, WHEN PERFORMING THE WNL ANALYSIS
Res = 47. ; 

 Lx_HB = [Lxc]; Fx_HB = [Fxc]; omega_HB = [Omegac]; Aenergy_HB  = [0]; Fy_HB = [0];

[meanflow,mode] = SF_HB1(meanflow,mode,'sigma',0.,'Re',Res); 

for Re = Re_HB(2:end)
    [meanflow,mode] = SF_HB1(meanflow,mode,'Re',Re);
    Lx_HB = [Lx_HB meanflow.Lx];
    Fx_HB = [Fx_HB meanflow.Fx];
    omega_HB = [omega_HB imag(mode.lambda)];
    Aenergy_HB  = [Aenergy_HB mode.AEnergy];
    Fy_HB = [Fy_HB mode.Fy];
    
    if(Re==60)
       figure();
       SF_Plot(meanflow,'vort','xlim',[-1.5 4.5],'ylim',[0 3],...
       'cbtitle','vorticity','colormap','redblue','colorrange',[-3 3],'boundary','on','bdlabels',2,'bdcolors','k');
       hold on;
       SF_Plot(meanflow,'psi','contour','on','clevels',[-.02 0 .2 1 2 5],'cstyle','patchdashedneg','xystyle','off','xlim',[-1.5 4.5],'ylim',[0 3]);

       box on; pos = get(gcf,'Position'); pos(4)=pos(3)*.6;set(gcf,'Position',pos); % resize aspect ratio
       set(gca,'FontSize', 18);
       saveas(gca,'FIGURES/Cylinder_MeanFlowRe60',figureformat); 
    end
end


save('Cylinder_AllFigures.mat');


%% chapter 5b : figures
%load('Cylinder_AllFigures.mat');

figure(21);hold off;
plot(Re_LIN,imag(lambda_LIN)/(2*pi),'b+-');
hold on;
plot(Re_WNL,omega_WNL/(2*pi),'g--','LineWidth',2);hold on;
plot(Re_HB,omega_HB/(2*pi),'r-','LineWidth',2);
plot(Rec,Omegac/2/pi,'ko');
LiteratureData=csvread('./literature_data/fig7a_st_Re_experience.csv'); %read literature data
plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);xlabel('Re');ylabel('St');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
legend('Linear','WNL','HB1','Ref. [23]','Location','northwest');
saveas(gca,'FIGURES/Cylinder_Strouhal_Re_HB',figureformat);

figure(22);hold off;
plot(Re_LIN,Fx_LIN,'b+-');
hold on;
plot(Re_WNL,Fx_WNL,'g--','LineWidth',2);hold on;
plot(Re_HB,Fx_HB,'r+-','LineWidth',2);
plot(Rec,Fxc,'ro')
xlabel('Re');ylabel('Fx');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
legend('BF','WNL','HB1','Location','south');
saveas(gca,'FIGURES/Cylinder_Cx_Re_HB',figureformat);

figure(23);hold off;
plot(Re_LIN,Lx_LIN,'b+-');
hold on;
plot(Re_HB,Lx_HB,'r+-','LineWidth',2);
LiteratureData=csvread('./literature_data/fig7e_Lx_mean.csv'); %read literature data
plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);
plot(Rec,Lxc,'ro','LineWidth',2);
xlabel('Re');ylabel('Lx');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
legend('BF','HB1','Ref. [5]','Location','northwest');
saveas(gca,'FIGURES/Cylinder_Lx_Re_HB',figureformat);

figure(24);hold off;
plot(Re_WNL,abs(Fy_WNL),'g--','LineWidth',2);
hold on;
plot(Re_HB,real(Fy_HB),'r+-','LineWidth',2);
%title('Harmonic Balance results');
xlabel('Re');  ylabel('Fy')
box on;  pos = get(gcf,'Position');  pos(4)=pos(3)*AspectRatio;  set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
legend('WNL','HB1','Location','south');
saveas(gca,'FIGURES/Cylinder_Cy_Re_SC',figureformat);

figure(25);hold off;
plot(Re_WNL,A_WNL,'g--','LineWidth',2);
hold on;
plot(Re_HB,Aenergy_HB,'r+-','LineWidth',2);
LiteratureData=csvread('./literature_data/fig7d_energy_amplitude.csv'); %read literature data
plot(LiteratureData(:,1),LiteratureData(:,2),'ko','LineWidth',2);
%title('Harmonic Balance results');
xlabel('Re');ylabel('A_E')
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio; set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
legend('WNL','HB1','Ref. [5]','Location','south');
if(meshstrategy=='D')
    filename = 'FIGURES/Cylinder_Energy_Re_SC_AdaptD';
else
    filename = 'FIGURES/Cylinder_Energy_Re_SC_AdaptS';
end
saveas(gca,filename,figureformat);


tnolin = toc;
disp(' ');
disp('       cpu time for Nonlinear calculations : ');
disp([ '   ' num2str(tnolin) ' seconds']);

disp(' ');
disp('Total cpu time for the linear & nonlinear calculations and generation of all figures : ');
disp([ '   ' num2str(tlin+tnolin) ' seconds']);


save('Results_Cylinder_NONLINEAR.mat');

% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch it !)


