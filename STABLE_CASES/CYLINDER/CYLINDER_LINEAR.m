%%  LINEAR Stability ANALYSIS of the wake of a cylinder with STABFEM  
%[[PUBLISH]]
%
%  this scripts demonstrates how to use StabFem to study the instability
%  in the wake of a 2D cylinder in incompressible flow using 
%  LINEAR STABILITY ANALYSIS (LSA)
%
%  The script reproduces all figures of section 3 in Fabre et al. (ASME-AMR, 2019)
%  
%  The script performs the following calculations :
% 
%  # Generation of an adapted mesh
%  # Base-flow properties for Re = [2-40]
%  # Stability curves St(Re) and sigma(Re) for Re = [40-100]
%
%  The raw source code is <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER/CYLINDER_LINEAR.m here>. 

%%
%
% First we set a few global variables needed by StabFem
%
% (NB to understand the usage of the 'workdir' option see chapter 5 of this script)

close all;
SF_core_start();
SF_core_setopt('verbosity', 4);
SF_core_setopt('ffdatadir', './WORK/');
SF_core_arborescence('cleanall');

%% ##### CHAPTER 1 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE
   
disp(' STARTING ADAPTMESH PROCEDURE : ');    
disp(' ');
disp(' LARGE MESH : [-40:80]x[0:40] ');
disp(' ');    


%% 
% We create the initial mesh using the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mesh.m SF_Mesh.m>
% which launches the program 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_Mesh.edp Lshape_Mesh.edp> 
% and imports the generated mesh.
%

ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'problemtype','2D','symmetry','S');

%%
% We initially compute a base flow for a low value of the Reynolds number. 
%

bf=SF_BaseFlow(ffmesh,'Re',1);

%%
% Note that <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_BaseFlow.m SF_BaseFlow.m>
% is a generic wrapper which launches the relevant FreeFem++ program to compute a base flow using Newton iteration. 
% In the present case (2D incompressible problem), the relevant program is 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/Newton_2D.edp Newton_2D.edp>.
%
%

%%
% We now do a first mesh adaptation. 
%

bf=SF_Adapt(bf,'Hmax',5);


%%
% Here we use the
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Adapt.m SF_Adapt.m> function 
% to adapt the mesh to the base flow. This function (which is a wrapper for the FreeFem++ program  
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/AdaptMesh.edp AdaptMesh.edp>) 
% allows to perform mesh adaptation using up to 8 fields with various data type 
% (scalar P1 or P2 data ; composite data for instance [P2xP2xP1] ,...) 
% Have a look at these programs to understand how it works !
%

%%
%
% We then progressively increase the Reynolds number up to 60, and readapt.
%

bf=SF_BaseFlow(bf,'Re',10);
bf=SF_BaseFlow(bf,'Re',60);
bf=SF_Adapt(bf,'Hmax',5);

%%
%
% We now compute an eigenvalue/eigenmode pair using shift-invert method
% (the "shift" is a guess for the eigenvalue ; the performance of the algorithm strongly relies
% on the appropriate choice of this parameter !)
% (Note that here we compute the adjoint eigenmode which will be used for
% mesh adaptation ; see paper AMR, 2018 to learn more on mesh adaptation strategies)

[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'nev',1,'type','A');

%%
%
% Note that <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Stability.m SF_Stability.m>
% is a generic wrapper which launches the relevant FreeFem++ program to
% compute either a single eigenvalue (with shift-invert) or a collection of eigenvalues (with Arnoldi method)
% In the present case (2D incompressible problem), the relevant program is 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/Stab2D.edp Stab2D.edp>.
%

%%
%
% We then readapt the mesh to both the base flow and the computed
% eigenmode. 

bf=SF_Adapt(bf,em,'Hmax',5);

%% 
% We do this twice, and at second time we also use a 'MASK' which will force 
% the grid step to be at most 0.15 in the wake region.
%

[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'nev',1,'type','A');
MASK = SF_Mask(bf,[-2 5 0 2 0.15]);
bf=SF_Adapt(bf,em,MASK,'Hmax',5);


%% CHAPTER 1b : DRAW FIGURES for mesh and base flow

figureformat='png'; 
system('mkdir FIGURES');

%  plot the mesh (full size)
figure();SF_Plot(bf,'mesh','xlim',[-40 80],'ylim',[0 40]);
title('Initial mesh (full size)');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_Mesh_Full',figureformat); 

%  plot the mesh (zoom)
figure();SF_Plot(bf,'mesh','xlim',[-1.5 4.5],'ylim',[0 3]);
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_Mesh',figureformat);
    
%   plot the base flow for Re = 60
figure();
SF_Plot(bf,'vort','xlim',[-1.5 4.5],'ylim',[0 3],'cbtitle','\omega_z','colormap','redblue','colorrange',[-2 2]);
hold on;SF_Plot(bf,'psi','contour','only','clevels',[-.02 0 .2 1 2 5],'xlim',[-1.5 4.5],'ylim',[0 3]);
box on;  set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_BaseFlowRe60',figureformat);

%% Chapter 2 : eigenmode, adjoint, sensitivity
%
% We now recompute the eigenmode along with the adjoint and structural sensitivity 
% (parameter 'type'='S' ; note the way the results of the functions are returned) 
% 

 [ev,emS,em,emA] = SF_Stability(bf,'shift',0.04+0.76i,'nev',1,'type','S');

%%
%  Here is how to plot the eigenmode, adjoint and structural sensitivity
%

figure();SF_Plot(em,'ux','xlim',[-2 8],'ylim',[0 5],'colormap','redblue','colorrange','cropcentered',...
    'boundary','on','bdlabels',2,'bdcolors','k');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_EigenModeRe60_AdaptS',figureformat);  % 

figure();SF_Plot(emA,'ux','xlim',[-2 8],'ylim',[0 5],'colormap','redblue','colorrange','cropcentered','boundary','on','bdlabels',2,'bdcolors','k');
box on;  set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_EigenModeAdjRe60',figureformat);

figure();SF_Plot(emS,'sensitivity','xlim',[-2 4],'ylim',[0 3],'colormap','ice','boundary','on','bdlabels',2,'bdcolors','k');

%hold on; 
%SF_Plot(bf,'psi','contour','on','clevels', [0 0],'CColor','b','CStyle','monochrome','ColorBar','off','xlim',[-2 4],'ylim',[0 3],...
%'colormap','ice','colorrange',[min(real(emS.sensitivity)), max(real(emS.sensitivity))]);

box on;  
pause(0.1);


%% CHAPTER 3 : DESCRIPTION OF BASE FLOW PROPERTIES (range 2-50)
%
% Note that the Base Flow object "bf" has fields 'Fx' and 'Lx' corresponding
% to the drag and recirculation length. Such case-dependent post-processing fields
% are managed by the macro "SFWriteBaseFlow" which has to be defined in
% the file
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER/Macros_StabFem.edp Macros_StabFem.edp>
% 
% Here we do a loop over Re to generate a vector containing these data.
% 

Re_BF = [20 : 2.5: 50];
Fx_BF = []; Lx_BF = [];
    for Re = Re_BF
        bf = SF_BaseFlow(bf,'Re',Re);
        Fx_BF = [Fx_BF,bf.Fx];
        Lx_BF = [Lx_BF,bf.Lx];
    end

%% Chapter 3B : plots 
%
% Here is how to plot these data
%

figure(22);hold off;
plot(Re_BF,Fx_BF,'b+-','LineWidth',2);
xlabel('Re');ylabel('Fx'); box on; set(gca,'FontSize', 18);
saveas(gca,'FIGURES/Cylinder_Fx_baseflow',figureformat);

figure(23);hold off;
plot(Re_BF,Lx_BF,'b+-','LineWidth',2);
xlabel('Re');ylabel('Lx'); box on; set(gca,'FontSize', 18);
saveas(gca,'FIGURES/Cylinder_Lx_baseflow',figureformat);
pause(0.1);


%% CHAPTER 4 : COMPUTING STABILITY BRANCH
%
% Here we will do a loop over Re in the range [40-100] to compute the
% eigenvalue. Note the 'shift'='cont' options  which mean that the shift
% will be re-evaluated by extrapolation at each step, leading to
% particularly efficient computations.
%


bf=SF_BaseFlow(bf,'Re',40);
[ev,em] = SF_Stability(bf,'shift',-.03+.72i,'nev',1,'type','D');

Re_LIN = [40 : 2.5: 60];lambda_LIN=[];
    for Re = Re_LIN
        bf = SF_BaseFlow(bf,'Re',Re);
        ev = SF_Stability(bf,'nev',1,'shift','cont');
        lambda_LIN = [lambda_LIN ev];
    end  

%% 
% NB in this basic example we show the loop in the present script ; it is
% also possible to do the same thing using the driver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Stability_LoopRe.m SF_Stability_LoopRe.m>
% (see how to use this driver in the more advanced tutorial examples)
% 
    
    


%% CHAPTER 4b : figures
%
% Here is how to plot these data
%

figure(20);
plot(Re_LIN,real(lambda_LIN),'b+-');
xlabel('Re');ylabel('$\sigma$','Interpreter','latex');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_Sigma_Re',figureformat);

figure(21);hold off;
plot(Re_LIN,imag(lambda_LIN)/(2*pi),'b+-');
xlabel('Re');ylabel('St');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Cylinder_Strouhal_Re',figureformat);
pause(0.1);

%% Chapter 5 : DATA-MINING FOR POST-PROCESSING
%
% In this example, we have successively computed a number of base flows and
% eigenvalues. You may legitimely ask 'where are the data stored' ? and
% 'how to reaccess all these data' ?
% 
% Even though this is not appearent in the sequence of calls to the Matlab
% drivers, StabFem actually stores all the data (as pairs of .txt/.ff2m files) 
% in a storage arborescence, which is created at the starting (when invoking SF_core_start).   
% To see what is available in the present case launch the data-mining
% wizard 'SF_Status'. For further explanations on how to do post-processing
% using this, see the script 
% <https://stabfem.gitlab.io/StabFem/stable/cylinder/CYLINDER_LINEAR_POSTPROCESS.m CYLINDER_LINEAR_POSTPROCESS.m>.

sf = SF_Status;

%%
% SF_Status explains the contents or the ./WORK/ directory and
% subdirectories, in a much more efficient way than just doing 'ls ./WORK/' in a bash shell.
% We can see that we have previously created 5 meshe (during the mesh-adaptation process), 
% stored 18 baseflows computed with the current mesh, and 3 eigenmodes. 
%
% You may wonder:
% Why only 3 modes are stored while a total of 11 calls to SF_Stability were done ? The answer is that 
% modes are stored if the driver is called as  [ev,em] = SF_Stability(...)  instead of ev = SF_Stability(...) ).
% Thus here only the calls at lines 116 and 147 lead to the storage of eigenmodes.

%% APPENDIX : HOW TO USE STABFEM WITHOUT MATLAB/OCTAVE
%
% If you don't have (or don't like) the matlab/octave environment, you can
% perfectly use the FreeFem++ part of the softare directly in a bash
% terminal ! 
% For instace here is how to perform the mesh adaptation and eigenmode
% computation (basically equivalent to the first part of the present
% program) in bash mode.
%
%
%
%{

> FreeFem++ -v 0 Mesh_Cylinder.edp
$$ Generation of an initial mesh for a 2D cylinder
$$ Enter the dimensions xmin, xmax, ymax >> -40 40 80
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number : >> 10
(...)

> cp WORK/BaseFlow.txt WORK/BaseFlow_guess.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number >> 60
(...)

> cp WORK/BaseFlow.txt WORK/FlowFieldToAdapt1.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/AdaptMesh.edp
$$ ENTERING ADAPTMESH.edp
$$ Enter nfields (number of fields to be used for adaptation) >> 1
$$ Enter storage mode of .txt file number 1 ? (string like ReP2P2P1, etc...) >> ReP2P2P1
$$ Enter number of additional real scalars associated to flowfield in file number 0 >> ?1
(...)

> cp WORK/FlowFieldToAdapt1.txt WORK/BaseFlow_guess.txt
> cp WORK/mesh_adapt.msh WORK/mesh.msh
> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number >> 60
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Stab2D.edp
$$ ENTERING Stab2D.edp?
$$ Enter Reynolds               		>>  60
$$ Enter SHIFT (re,im)          		>> 0.04 0.74
$$ Symmetry properties ?? (A, S or N).  >> A
$$ Direct (D), Adjoint (A), D&A+sensitivity (S) or Endo. (E) ?>> S
$$ Enter nev ? (will use simple shift-invert if nev = 1) >> 1
(...)

> cp WORK/BaseFlow.txt WORK/FlowFieldToAdapt1.txt
> cp WORK/Sensitivity.txt WORK/FlowFieldToAdapt2.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/AdaptMesh.edp
$$ Entering ADAPTMESH.edp 
$$ Enter nfields (number of fields to be used for adaptation) >> 2
$$ Enter storage mode of .txt file number 1 ? (string like ReP2P2P1, etc...) >> ReP2P2P1
$$ Enter number of additional real scalars associated to flowfield in file number 0 >> ?1
$$ Enter storage mode of .txt file number 2 ? (string like ReP2P2P1, etc...) >> ReP2
$$ Enter number of additional real scalars associated to flowfield in file number 1 >> ?0
(...)

> cp WORK/FlowFieldToAdapt1.txt WORK/BaseFlow_guess.txt
> cp WORK/mesh_adapt.msh WORK/mesh.msh

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number ?>> 60
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Stab2D.edp
$$ ENTERING Stab2D.edp?
$$ Enter Reynolds               		>>  60
$$ Enter SHIFT (re,im)          		>> 0.04 0.74
$$ Symmetry properties ?? (A, S or N).  >> A
$$ Direct (D), Adjoint (A), D&A+sensitivity (S) or Endo. (E) ?>> D
$$ Enter nev ? (will use simple shift-invert if nev = 1) >> 1
(...)

%}
