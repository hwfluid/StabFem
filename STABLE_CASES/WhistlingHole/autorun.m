function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE

close all;
if(nargin==0)
    verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_chi1/');

value = 0;
% Test 1 :

bf = SF_Load('lastadapted');

% test : check if BF is correcty recovered


% Test 2 : impedance computation for Re = 1600

bf = SF_BaseFlow(bf,'Re',1600);
Params = [5 1e30 1.25 .5 20 1e30];
omega = 0.8;
foA = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

Zref = -0.1630 - 1.0387i;


error1 = abs(foA.Z/Zref-1);

if(error1>1e-2) 
    value = value+1 
end

% test 3 with stability calculation

Params = [5 1e30 1.25 .5 20 1e30];
ev = SF_Stability(bf,'shift',-2.1i,'m',0,'nev',10,'type','D','mappingdef','jet','mappingparams',Params);

EVref = 0.0819 - 2.1013i;

error2 = abs(ev(1)/EVref-1);

if(error2>1e-2) 
    value = value+1 
end

end
