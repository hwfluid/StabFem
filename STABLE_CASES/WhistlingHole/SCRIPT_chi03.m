


%% Chapter 0 : Initialization 
SF_core_start('verbosity',4,'workdir','./WORK_chi03/') % NB these two parameters are optional ; SF_core_Start() will do the same

%% Chapter 1 : builds or recovers base flow
chi = 0.3;
sfstat = SF_Status();
if strcmp(sfstat.status,'new')
    bf = SmartMesh_Hole(0.3); % or your own sequence for mesh/bf generation
else
    bf = SF_Load('lastadapted'); 
end 


%% Plot BF
Re = 1600;
bf = SF_BaseFlow(bf,'Re',1600);
figure;
SF_Plot(bf,'ux','title','Base Flow','colormap','redblue','xlim',[-3 3],'ylim',[-1.5 1.5],'contour','on','clevels',[0 0],...
    'boundary','on','bdlabels',2,'bdcolors','k','symmetry','XS');
pause(0.1);

%% Forced structures

% Computes forced structures for 3 values of omega
bf = SF_BaseFlow(bf,'Re',1600);
Params = [5 1e30 1.25 .5 20 1e30];

omega = 2.6;
foA = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

omega = 5.45;
foB = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

omega = 8.25;
foC = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

%% Here is how to plot the velocity and pressure in two subfigures with different ranges
Re = 1600;
figure;
make_it_tight = true;
subplot = @(m,n,p) subtightplot (m, n, p, [0.1 0.1], [0.05 0.05], [0.05 0.05]);
if ~make_it_tight,  clear subplot;  end

% a and b 
%foA.p1f = logfilter(foA.p1,1);foA.ux1f = logfilter(foA.ux1,1);foA.vort1f = logfilter(foA.vort1,1);
subplot(3,2,1);
%p1corr = SF_ExtractData(foA,'p1',3,3);
%foA.p1C = foA.p1+480;
SF_Plot(foA,'p','boundary','on','colormap','redblue','colorrange',[-3,3],'xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''','logsat',1);
           text(-2.8,3.5,'(a)');
subplot(3,2,2);
SF_Plot(foA,'ux','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','u''_x','logsat',1);
             text(-2.8,3.5,'(b)');

% c and d
%foB.p1f = logfilter(foB.p1,0.5);foB.ux1f = logfilter(foB.ux1,0.2);foB.vort1f = logfilter(foB.vort1,1);
subplot(3,2,3);
SF_Plot(foB,'p','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''','logsat',.5);
           text(-2.8,3.5,'(c)');
subplot(3,2,4);
SF_Plot(foB,'ux','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','u''_x','logsat',.2);
             text(-2.8,3.5,'(d)');
             
% e and f 
%foC.p1f = logfilter(foC.p1,1);foC.ux1f = logfilter(foC.ux1,1);foC.vort1f = logfilter(foC.vort1,1);
subplot(3,2,5);
SF_Plot(foC,'p','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''','logsat',1);
           text(-2.8,3.5,'(e)');
subplot(3,2,6);
SF_Plot(foC,'ux','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','u''_x','logsat',1);
             text(-2.8,3.5,'(f)');

             
 pos = get(gcf,'Position'); pos(4)=pos(3)*.6;set(gcf,'Position',pos);            
% pos = get(gcf,'Position'); pos(3) = 800;pos(4)=pos(3)*.6;set(gcf,'Position',pos); 
saveas(gcf,['ForcedModes_PU_chi', num2str(chi), '_Re',num2str(Re),'.png'],'png');
saveas(gcf,['ForcedModes_PU_chi', num2str(chi), '_Re',num2str(Re), '.fig'],'fig');
%saveas(gcf,['ForcedModes_chi', num2str(chi), '_Re',num2str(Re), '.svg'],'svg');

%% Here is how to plot the PRESSURE 
figure;
make_it_tight = true;
subplot = @(m,n,p) subtightplot (m, n, p, [0.1 0.1], [0.05 0.05], [0.05 0.05]);
if ~make_it_tight,  clear subplot;  end

% a and b 
%foA.p1f = logfilter(foA.p1,1);foA.ux1f = logfilter(foA.ux1,1);foA.vort1f = logfilter(foA.vort1,1);
subplot(3,2,1);
%p1corr = SF_ExtractData(foA,'p1',3,3);
%foA.p1C = foA.p1+480;
SF_Plot(foA,'p','boundary','on','colormap','redblue','colorrange',[-3,3],'xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_r','logsat',1);
           text(-2.8,3.5,'(C1)');
subplot(3,2,2);
SF_Plot(foA,'p.im','boundary','on','colormap','redblue','colorrange',[-3,3],'xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_i','logsat',1);
             %text(-2.8,3.5,'(b)');

% c and d
%foB.p1f = logfilter(foB.p1,0.5);foB.ux1f = logfilter(foB.ux1,0.2);foB.vort1f = logfilter(foB.vort1,1);
subplot(3,2,3);
SF_Plot(foB,'p','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_r','logsat',.2);
           text(-2.8,3.5,'(S1)');
subplot(3,2,4);
SF_Plot(foB,'p.im','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_i','logsat',.2);
           %  text(-2.8,3.5,'(d)');
             
% e and f 
%foC.p1f = logfilter(foC.p1,1);foC.ux1f = logfilter(foC.ux1,1);foC.vort1f = logfilter(foC.vort1,1);
subplot(3,2,5);
SF_Plot(foC,'p','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_r','logsat',1);
           text(-2.8,3.5,'(C2)');
subplot(3,2,6);
SF_Plot(foC,'p.im','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 5],'ylim',[0 3],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','p''_i','logsat',1);
             %text(-2.8,3.5,'(f)');

             
 pos = get(gcf,'Position'); pos(4)=pos(3)*.6;set(gcf,'Position',pos);            
% pos = get(gcf,'Position'); pos(3) = 800;pos(4)=pos(3)*.6;set(gcf,'Position',pos); 
saveas(gcf,['ForcedModes_P_chi', num2str(chi), '_Re',num2str(Re),'.png'],'png');
saveas(gcf,['ForcedModes_P_chi', num2str(chi), '_Re',num2str(Re), '.fig'],'fig');
%saveas(gcf,['ForcedModes_chi', num2str(chi), '_Re',num2str(Re), '.svg'],'svg');



%% Plot the pressure and axial velocity along axis
Xa = [-4*chi :.01*chi :2*chi];
figure;

pA = SF_ExtractData(foA,'p',Xa,0);
uA = SF_ExtractData(foA,'ux',Xa,0);
subplot(3,1,1);
plot(Xa,real(uA),'r',Xa,imag(uA),'r--',Xa,real(pA),'b',Xa,imag(pA),'b--');
hold on;
plot([-2*chi -2*chi], [-2 2], 'k:',[0 0], [-2 2], 'k:');
xlabel('x');
legend('u_{x,r}','u_{x,i}','p_r','p_i');

pB = SF_ExtractData(foB,'p',Xa,0);
uB = SF_ExtractData(foB,'ux',Xa,0);
subplot(3,1,2);
plot(Xa,real(uB),'r',Xa,imag(uB),'r--',Xa,real(pB),'b',Xa,imag(pB),'b--');
hold on;
plot([-2*chi -2*chi], [-2 2], 'k:',[0 0], [-2 2], 'k:');
xlabel('x');
legend('u_{x,r}','u_{x,i}','p_r','p_i');

pC = SF_ExtractData(foC,'p',Xa,0);
uC = SF_ExtractData(foC,'ux',Xa,0);
subplot(3,1,3);
plot(Xa,real(uC),'r',Xa,imag(uC),'r--',Xa,real(pC),'b',Xa,imag(pC),'b--');
hold on;
plot([-2*chi -2*chi], [-2 2], 'k:',[0 0], [-2 2], 'k:');
xlabel('x');
legend('u_{x,r}','u_{x,i}','p_r','p_i');

saveas(gcf,['ForcedModes_chi', num2str(chi), '_Re',num2str(Re),'.png'],'png');
saveas(gcf,['ForcedModes_chi', num2str(chi), '_Re',num2str(Re), '.fig'],'fig');

%% Here is how to plot the VORTICITY 

figure(12);

% a and b 
%foA.p1f = logfilter(foA.p1,1);foA.ux1f = logfilter(foA.ux1,1);foA.vort1f = logfilter(foA.vort1,1);
subplot(3,2,1);
SF_Plot(foA,'vort.re','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
           text(-2.8,3.5,'(a)');
subplot(3,2,2);
SF_Plot(foA,'vort.im','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,i}');
             text(-2.8,3.5,'(b)');

% c and d
%foB.p1f = logfilter(foB.p1,1);foB.ux1f = logfilter(foB.ux1,1);foB.vort1f = logfilter(foB.vort1,1);
subplot(3,2,3);
SF_Plot(foB,'vort','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
           text(-2.8,3.5,'(c)');
subplot(3,2,4);
SF_Plot(foB,'vort.im','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,i}');
             text(-2.8,3.5,'(d)');
             
% e and f 
%foC.p1f = logfilter(foC.p1,1);foC.ux1f = logfilter(foC.ux1,1);foC.vort1f = logfilter(foC.vort1,1);
subplot(3,2,5);
SF_Plot(foC,'vort','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
               'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
           text(-2.8,3.5,'(e)');
subplot(3,2,6);
SF_Plot(foC,'vort.im','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,i}');
             text(-2.8,3.5,'(f)');

             
pos = get(gcf,'Position'); pos(4)=pos(3)*.6;set(gcf,'Position',pos);               
saveas(gcf,['ForcedModes_OMP_chi', num2str(chi), '_Re',num2str(Re),'.png'],'png');
saveas(gcf,['ForcedModes_OMP_chi', num2str(chi), '_Re',num2str(Re), '.fig'],'fig')
%saveas(gcf,['ForcedModes_OMP_chi', num2str(chi), '_Re',num2str(Re), '.svg'],'svg')




%% Here is how to plot the VORTICITY  (only real part)
figure;
subplot = @(m,n,p) subtightplot (m, n, p, [0.1 0.1], [0.05 0.05], [0.05 0.05]);
% a and b 
subplot(3,1,1);
SF_Plot(foA,'vort.re','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
             text(-.85,1.2,'(C1)');

% c and d
subplot(3,1,2);
SF_Plot(foB,'vort.re','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
             text(-.85,1.2,'(S1)');
             
% e and f 
subplot(3,1,3);
SF_Plot(foC,'vort.re','boundary','on','colormap','redblue','colorrange',[-100 100],'xlim',[-.9 .3],'ylim',[.675 1.125],...
                'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_{\phi,r}');
             text(-.85,1.2,'(C2)');

             
pos = get(gcf,'Position'); pos(4)=pos(3)*.6*1.2; pos(3) = pos(3)/2*1.2; set(gcf,'Position',pos);               
saveas(gcf,['ForcedModes_VORTre_chi', num2str(chi), '_Re',num2str(Re),'.png'],'png');
saveas(gcf,['ForcedModes_VORTre_chi', num2str(chi), '_Re',num2str(Re), '.fig'],'fig')
%saveas(gcf,['ForcedModes_VORT_chi', num2str(chi), '_Re',num2str(Re), '.svg'],'svg')

%% Impedances for this case
figure(30);
fo = SF_LinearForced(bf,'omega',[0:.1:8],'mappingdef','jet','mappingparams',Params,'plot','yes');

