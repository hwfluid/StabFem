function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE
%
% USAGE : 
% autorun(0) -> automatic check
% autorun(1) -> produces the figures used for the manual
% autorun(2) -> may produces "bonus" results...
%%
close all;

if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');

value =0;

%% Test 1 : base flow
bf = SmartMesh_L6();

disp(['Test 1 : Drag : ', bf.Cx]);
CxREF = 3.2618

error1 = abs(bf.Cx/CxREF-1)

if(error1>1e-2) 
    value = value+1 
end




%% Test 2 : eigenvalues 
[ev,em] = SF_Stability(bf,'m',1,'shift',.2+0.3i,'nev',20,'type','D') ;  

%evREF = [     0.0501 + 0.6574i 0.2425 + 0.0000i ]
ev(1:2)
evREF = [0.2426 + 0.0000i, 0.0504 + 0.6587i]

error2 = abs(ev(1)/evREF(1)-1)+abs(ev(2)/evREF(2)-1)

if(error2>1e-2) 
    value = value+1 
end


%% Test 3 : mesh stretch

bf2 = SF_MeshStretch(bf,'Yratio',1.1,'Ymin',0.5);

bf2.Cx
CxREF = 2.8379

error3 = abs(bf2.Cx/CxREF-1)

if(error3>1e-3) 
    value = value+1 
end

end

 

