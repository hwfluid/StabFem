


%% Chapter 0 : Initialization 
SF_core_start('verbosity',4,'workdir','./WORK/') % NB these two parameters are optional ; SF_core_Start() will do the same

%% Chapter 1 : builds or recovers base flow
sfstat = SF_Status();
if strcmp(sfstat.status,'new')
    bf = SmartMesh_L6();
else
    bf = SF_Load('lastadapted'); 
end


%% plot the base flow
figure;SF_Plot(bf,'ux','contour','on','clevels',[0,0],'xlim',[-3 10],'ylim',[0 1]);  %
pause(0.1);


%% Spectrum explorator 
[ev,eigenmode] = SF_Stability(bf,'m',1,'shift',1+0.2i,'nev',20,'type','D','plotspectrum','yes');   
pause(0.1);



 
%% COMPUTING THE UNSTEADY BRANCH (going backwards)
Re_RangeI = [500:-20:300];
guessI = 0.04+0.657i;
[EVI,ResI,omegasI] = SF_Stability_LoopRe(bf,Re_RangeI,guessI,'plot','r');
       

        
%% COMPUTING THE STEADY BRANCH (going backwards)
Re_RangeS = [500:-50:300 280 :-20:160];
guessS = 0.246;
[EVS,ResS,omegasS] = SF_Stability_LoopRe(bf,Re_RangeS,guessS,'plot','b');
      

%% FIGURES
figure(11);hold on;
subplot(2,1,1);hold on;
plot(Re_RangeS,real(EVS),'-b',Re_RangeI,real(EVI),'-r');hold on;
plot(Re_RangeS,0*real(EVS),':k');%axis
title('growth rate lambda_r vs. Reynolds ; unstready mode (red) and steady mode (blue)')
subplot(2,1,2);hold on;
plot(Re_RangeS,imag(EVS),'-b',Re_RangeI,imag(EVI),':r',Re_RangeI(real(EVI)>0),imag(EVI(real(EVI)>0)),'r-')
title('oscillation rate lambda_i vs. Reynolds')
suptitle('Leading eigenvalues of a blunt body with L/d=6; D/d=2');
