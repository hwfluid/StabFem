%% initialisation
%clear all
%close all
close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start();

%set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
%set(groot, 'defaultLegendInterpreter','latex');

%% Mesh computation
ffmeshInit = SF_Mesh('Mesh_1.edp','Params',4,'problemtype','acousticaxi');
%% Eigenvalue problem

    [evCM,emCM] = SF_Stability(ffmeshInit,'shift',1,'BC','CM','nev',30);
    [evS,emS] = SF_Stability(ffmeshInit,'shift',1,'BC','SOMMERFELD','nev',30);
  %  evListCM = [evCM, evListCM];
  %  evListS  = [evS,  evListS];
%end
%% Plot
figure;hold off; 
plot(imag(evCM),real(evCM),'s');
hold on;
plot(imag(evS),real(evS),'x');


% [evCM,emCM] = SF_Stability(ffmeshInit,'shift',0.8,'BC','CM','nev',10);
% [evS,emS] = SF_Stability(ffmeshInit,'shift',1,'BC','SOMMERFELD','nev',10);
% figure;
% plot(imag(evCMS),real(evCMS),'s');
% hold on;
% plot(imag(evCM),real(evCM),'x');
% hold off;

