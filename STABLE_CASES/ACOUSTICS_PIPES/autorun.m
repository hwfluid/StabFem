function value = autorun
% Autorun function for StabFem. 
% This function will produce sample results for the acoustic forced flow in a open pipe  

value = 0;

SF_core_setopt('verbosity', 0);
SF_core_setopt('ffdatadir', './WORK/');

ffmesh = SF_Mesh('Mesh_1.edp','Params',10,'problemtype','acousticaxi');
Forced = SF_LinearForced(ffmesh,1,'BC','SOMMERFELD');
ffmesh = SF_Adapt(ffmesh,Forced,'Hmax',1); % Adaptation du maillage
 
IMPPML = SF_LinearForced(ffmesh,[0.1:.1:1],'BC','PML','plot','no');

%Zref = 0.3886 - 0.4582i;
Zref = 4.2233e-01 - 4.72014e-01i; % changed value  on 26/06/2019, to be checked ??
Zcomp = IMPPML.Z(10)
disp('##### autorun test 1 : impedance');
error1 = abs(Zcomp/Zref-1)
if(error1>1e-3) 
    value = value+1 
end



end
