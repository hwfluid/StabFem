function value = autorun(verbosity);
% Autorun function for StabFem. 
% This function will produce sample results for the wake of an airfoil  
%  - Base flow computation
%  - Linear stability with CM (box)
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests
% autorun(1) -> produces the figures (in present case not yet any figures)

if(nargin==0) 
    verbosity=0; 
end;
SF_core_start('verbosity',verbosity,'workdir','./WORK/');


% We now only have to specify ffdatadir and verbosity
%global sfopts;
%sfopts.verbosity= 0;
%sfopts.ffdatadir = './WORK/';
%SF_core_createarborescence;



format long;


%% Chapter 0 : reference values for non-regression tests

np_REF = 15881;
Fx_REF =  0.9133;
ev_REF =   0.708717 + 2.60929i;

%% ##### CHAPTER 1 : PARAMETERS OF THE TEST 
Ma = 0.1;
Re = 1000;
np = 128;
Rout = 20;
HMax = 10;
%% ##### CHAPTER 2 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE

disp('##### autorun test 1 : mesh and BASE FLOW');
ffmesh = SF_Mesh('Naca0012MeshFull.edp','problemtype','2dcomp','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'Re',10,'Mach',Ma,'type','NEW');
bf=SF_BaseFlow(bf,'Re',100,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',HMax);
bf=SF_BaseFlow(bf,'Re',500,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',HMax);
bf=SF_BaseFlow(bf,'Re',1000,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',HMax);
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams',...
                       [-10,10,-10,10,-0.3,5,-0.3,5]) 
[ev,em] = SF_Stability(bf,'shift',0.7121 + 2.5875i,'nev',3,'type','D');
bf=SF_Adapt(bf,em(1),'Hmax',HMax);

error(1) = abs(bf.Fx/Fx_REF-1)+abs(bf.mesh.np/np_REF-1)


%%  CHAPTER 3 : linear mode for Re=50


disp('##### autorun test 2 : LINEAR MODE');
[evCM,emCM] = SF_Stability(bf,'shift',ev(1),'nev',3,'type','D');

evCM = evCM(1);
error(2) = abs(evCM/ev_REF-1)
value = sum((error>1e-2))



end
