  
% THIS SCRIPT DEMONSTRATES HOW TO DO DNS with StabFem

%%

SF_core_start('verbosity',0,'workdir','./WORK/');

%% Chapter 1 : generation of a mesh and base flow for Re=60
%
% If a base flow coming from a previous computation exists in the
% WORK/MESHES directory, we recover it
% (this trick is to save time)
bf = SF_Load('lastbaseflow'); 

if isempty(bf)

%%
% Othewise  we recompute it 
%
% First we build an initial mesh on a half-domain in progressively increase
% the Reynolds as in "CYLINDER_LINEAR.m"
    ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'problemtype','2D','symmetry','S');
    bf=SF_BaseFlow(ffmesh,'Re',1);
    bf=SF_BaseFlow(bf,'Re',10);
    bf=SF_BaseFlow(bf,'Re',60);
    bf = SF_Adapt(bf,'Hmax',5);
    
%%
% At this stage we use SF_Mirror.m to transform the half-mesh into a
% symmetrical mesh
% (this procedure guarranties that the mesh is perfectly symmetric ;
% one may also produce direclty a mesh on a full domain but such meshes are
% not symmetric)

    ffmesh = SF_Mirror(bf.mesh); 
%%
% After using SF_Mirror we have to restart the Base-flow computation
% process as previously
% (take care not to do any more mesh adaptation after this stage, otherwise
% the symmetry of the mesh will be lost ! 
    
    bf=SF_BaseFlow(ffmesh,'Re',1);
    bf=SF_BaseFlow(bf,'Re',10,'type','NEW');
    bf=SF_BaseFlow(bf,'Re',60,'type','NEW');
    
%%
% You now have a efficient mesh fitted to DNS. If you don't trust the mesh
% you may try other adaptation strategies (adaptation to eigenmode, etc..)
% or more simply use SF_Split to split each triangles in 4. 
% to try this simply uncomment the following line :
    
%    bf = SF_Split(bf);

end

%% Chapter 1bis : generation of a starting point for DNS using stability results
%
% We want to initialise the DNS with an initial condition 
% $u = u_b + \epsilon u_1$ where $u_b$ is the base flow, $u_1$ is the
% eigenmode, and $\epsilon = 0.01$.
% 
%

[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'sym','N'); % compute the eigenmode. 
%%
% Mind to use 'N' as symmetry property when using a mirrored mesh.

startfield = SF_Add(bf,em,'coefs',[1 0.01]); % creates startfield = bf+0.01*em


%% Chapter 2 : Launch a DNS

%  We do 5000 time steps and produce snapshots each 10 time steps 

Nit = 100; iout = 20;dt = 0.02;
[DNSstats,DNSfields] =SF_DNS(startfield,'Re',60,'Nit',Nit,'dt',dt,'iout',iout)


%% plot 
h = figure;
filename = 'html/DNS_Cylinder_Re60_Symmetric.gif';
SF_Plot(DNSfields(1),'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-3 3],...
        'title',['t = 0'],'boundary','on','bdlabels',2)
set(gca,'nextplot','replacechildren');

%% Generate a movie

for i=1:Nit/iout
    SF_Plot(DNSfields(i),'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-3 3],...
        'title',['t = ',num2str((i-1)*dt)],'boundary','on','bdlabels',2)
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if i == 1 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60_Symmetric.gif>>
%

%% Plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstats.t,DNSstats.Fy);title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.t,DNSstats.Fx);title('Drag force as function of time');
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');

