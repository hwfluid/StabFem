function value = autorun
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE
%
% USAGE : 
% autorun(0) -> automatic check
% autorun(1) -> produces the figures used for the manual
% autorun(2) -> may produces "bonus" results...
%%
close all;

SF_core_setopt('verbosity', 0);
SF_core_setopt('ffdatadir', './WORK/');

value =0;
    
%%
    ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp','problemtype','2D');
    bf=SF_BaseFlow(ffmesh,'Re',1);
    bf=SF_BaseFlow(bf,'Re',10);
    bf=SF_BaseFlow(bf,'Re',60);
    bf = SF_Adapt(bf,'Hmax',5);
%%
[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'sym','N'); % compute the eigenmode. 
startfield = SF_Add(bf,em,'coefs',[1 0.01]); % creates startfield = bf+0.01*em
%%
Nit = 100; iout = 20;dt = 0.02;
[DNSstats,DNSfields] = SF_DNS(startfield,'Re',60,'Nit',Nit,'dt',dt,'iout',iout);


FyendREF = 1.679930000000000e-04;
uxtestREF = -0.019835672300519;

Fyend = DNSstats.Fy(end);
uxtest = SF_ExtractData(DNSfields(end),'uy',2,.5);


error = abs(Fyend/FyendREF-1)
if(error>1e-2) 
    value = value+1 ;
end


error = abs(uxtest/uxtestREF-1)
if(error>1e-2) 
    value = value+1 ;
end

end
