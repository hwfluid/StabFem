%%
figure;

x = 0:.1:10;
y = sin(x);
plot(x,y,'r');
hold on;
plot(x,y.^2,'b');

%%
figure;
plot(x,cos(x));

%%
figure(30);

plot(x,tan(x));
hold on;


%%
figure(30);

plot(x,atan(x));

