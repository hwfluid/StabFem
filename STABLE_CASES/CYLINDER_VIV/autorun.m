function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a SPRING-MOUNTED cylinder with STABFEM  

if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');


%% ##### CHAPTER 1 : EIGENVALUES FOR modes EM and FM

bf = SmartMesh_Cylinder('S',[-40 80 40]); 

mstar = 20; Ustar = 3;

% mode "FM"

M = mstar*pi/4; K = pi^3*mstar/Ustar^2;

shiftFM = 0.04689 + 0.74874i; 
[evFM,emFM] = SF_Stability(bf,'shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);


% mode "EM"
shiftEM = -0.0188 + 2.0100i; 
[evEM,emEM] = SF_Stability(bf,'shift',shiftEM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);


error(1) = abs(evEM/shiftEM-1)+abs(evFM/shiftFM-1);


%% Test 2 : impedance for Re = 35, omega = 0.75
bf = SF_BaseFlow(bf,'Re',35);
fo = SF_LinearForced(bf,'omega',0.75);

Zref = -0.5854 - 0.6950i;

error(2) = abs(fo.Z/Zref-1);

%% BILAN
value = sum((error>1e-2))

end
