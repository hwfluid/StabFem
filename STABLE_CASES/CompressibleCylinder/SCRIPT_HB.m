%% Stability analysis of compressible flow around a cylinder
%
%  This script generates figures 8-9 of the ASME-AMR paper by Fabre et al.
%  (Reproducing results of Fani et al.)
%
%  NB this script needs some clean-up !
%
%%




%% CHAPTER 0 : set the global variables needed by the drivers
disp(' ');
disp('######     ENTERING LINEAR PART       ####### ');
disp(' ');
clear all;
close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/');
figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;
Ma = 0.4;
Re = 100;
% NB the drivers may produce warning messages such as "rm: ./WORK/*.txt: No such file or directory"
% don't mind them, this should disapear in future evolutions of StabFem

%% 1A : mesh creation

% Mesh in the style of that used by Fani et al, but on half-domain
% parameters for mesh creation ; Italian method
xinfm=-40.; xinfv=80.; yinf=40.;
% Inner domain
x1m=-2.5; x1v=10.; y1=2.5;
% Middle domain
x2m=-10.;x2v=30.;y2=10;
% Sponge extension
ls=300.0; 
% Refinement parameters
n=1.8; % Vertical density of the outer domain
ncil=100; % Refinement density around the cylinder
n1=7; % Density in the inner domain
n2=3; % Density in the middle domain
ns=0.15; % Density in the outer domain
nsponge=.15; % density in the sponge region

%Compressibility to create a mesh 
Ma = 0.1
ParamsForSponge = [xinfm,xinfv,yinf,x1m,x1v,y1,x2m,x2v,y2,ls,n,ncil,n1,n2,ns,nsponge];
ffmesh = SF_Mesh('Mesh.edp','Params',ParamsForSponge,'problemtype','2dcompsponge');
% Number of vertices in this mesh : 
ffmesh.np

%% WNL (multiple scales)
bf=SF_BaseFlow(ffmesh,'Re',47.275,'Mach',Ma,'type','NEW');
[ev,em] = SF_Stability(bf,'shift',0.74i,'nev',1,'type','D');
[ev,emAdj] = SF_Stability(bf,'shift',ev,'nev',1,'type','A');
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em,'Retest',47.3,'Adjoint',emAdj,'Normalization','L');
[meanflow,mode] = SF_HB1(bf,em,'Re',47.15,'Ma',Ma,'Amp',0.22);
% 
% for Re=[[49:1:60],[60:2.5:100]]
%     [meanflow,mode] = SF_HB1(meanflow,mode,'Re',Re,'Amp',mode.AEnergy);
% end
% 
% 
% bf=SF_BaseFlow(bf,'Re',47,'Ma',Ma);
% [ev,em] = SF_Stability(bf,'shift',+.73i,'nev',1,'type','D','sym','N','Ma',Ma);
% 
% [ev,em] = SF_Stability(bf,'shift',1i*Omegac,'nev',1,'type','S','sym','N','Ma',Ma); % type "S" because we require both direct and adjoint
% [wnl,meanflow,mode] = SF_WNL(bf,em,'Retest',47.0,'Normalization','L'); % Here to generate a starting point for the next chapter
