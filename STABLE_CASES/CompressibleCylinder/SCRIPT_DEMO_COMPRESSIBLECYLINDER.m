%% Stability analysis of compressible flow around a cylinder
%
%  This script generates figures 8-9 of the ASME-AMR paper by Fabre et al.
%  (Reproducing results of Fani et al.)
%
%  NB this script needs some clean-up !
%
%%




%% CHAPTER 0 : set the global variables needed by the drivers
disp(' ');
disp('######     ENTERING LINEAR PART       ####### ');
disp(' ');
clear all;
close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/');
figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;
Ma = 0.2;
Re = 150;
% NB the drivers may produce warning messages such as "rm: ./WORK/*.txt: No such file or directory"
% don't mind them, this should disapear in future evolutions of StabFem

%% Chapter 1 : Italian-style mesh (multiple imbricated zones)

%% 1A : mesh creation

% Mesh in the style of that used by Fani et al, but on half-domain
% parameters for mesh creation ; Italian method
xinfm=-100.; xinfv=100.; yinf=100.;% Outer Domain 
x1m=-10; x1v=20.; y1=10;% Inner domain
x2m=-30.;x2v=40.;y2=25;% Middle domain
ls=300.0; % Sponge extension
% Refinement parameters
n=1.8; % Vertical density of the outer domain
ncil=60; % Refinement density around the cylinder
n1=15; % Density in the inner domain
n2=5; % Density in the middle domain
ns=.5; % Density in the outer domain
nsponge=.05; % density in the sponge region
ParamsForSponge = [xinfm,xinfv,yinf,x1m,x1v,y1,x2m,x2v,y2,ls,n,ncil,n1,n2,ns,nsponge];
ffmesh = SF_Mesh('Mesh_HalfDomain_WITHSUBZONES.edp','Params',ParamsForSponge,'problemtype','2dcompsponge');
% Number of vertices in this mesh : 
ffmesh.np
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',0.4,'type','NEW');
bf=SF_BaseFlow(bf,'Re',60,'Mach',0.4);
bf=SF_BaseFlow(bf,'Re',100,'Mach',0.4);
%%
% Plot this mesh
figure;
subplot(2,2,1);SF_Plot(bf,'mesh');
subplot(2,2,2);SF_Plot(bf,'mesh','xlim',[-100 100],'ylim',[0 100]);
subplot(2,2,3);SF_Plot(bf,'mesh','xlim',[-5 15],'ylim',[0 10]);
subplot(2,2,4);SF_Plot(bf,'mesh','xlim',[-1 1],'ylim',[0 1]);
%suptitle('mesh at various scales (italian strategy)');
pause(1);
box on; 
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_Compressible_ItalianMesh',figureformat);
pause(0.1);

%% 1B. Computation of a BF for Re = 150, Ma = 0.2

bf=SF_BaseFlow(bf,'Re',150,'Mach',0.2);

figure();    
SF_Plot(bf,'ux','xlim',[-5 15],'ylim',[0 5]);
title('Base flow at Re=150 (axial velocity)');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_BaseFlowRe60Ma02',figureformat);
pause(0.1);

%% Chapter 1C : Compute eigenmode
%Plot eigenmode (figure 6 of Fani et al)
[ev,emD] = SF_Stability(bf,'shift',0.152 + 0.642i,'nev',1,'type','D','sym','A'); 

figure;
SF_Plot(emD,'vort1','xlim',[-2 5],'ylim',[0 3],'colorrange','cropcentered','colormap','redblue');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_EigenmodeRe60Ma02_vort',figureformat);
pause(0.1);

%
figure;
SF_Plot(emD,'p1','xlim',[-100 100],'ylim',[-100 100],'colorrange',[-1e-6,1e-6],'colormap','redblue');
hold on;
SF_Plot(emD,'p1.im','xlim',[-100 100],'yl-im',[-100 100],'colorrange',[-1e-6,1e-6],'colormap','redblue','symmetry','XM');
box on; 
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_EigenmodeRe60Ma02_p',figureformat);
pause(0.1);




%% TO ADD HB



bf = SF_BaseFlow(bf,'Re',Rec);
%[ev,sensib,em,emAdaj] = SF_Stability(bf,'shift',ev,'nev',1,'type','S'); % type S = direct+adjoint (adjoint is needed for WNL)

[ev,em] = SF_Stability(bf,'shift',ev,'nev',1,'type','D');
[ev,emAdj] = SF_Stability(bf,'shift',ev,'nev',1,'type','A');

Res = 47;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em,'Retest',Res,'Adjoint',emAdj);

wnl.Lambda
wnl.nu0
wnl.nu2

error(4) = abs(wnl.Lambda/Lambda_REF-1)+abs(wnl.nu0/nu0_REF-1)+abs(wnl.nu2/nu2_REF-1)



%% CHAPTER 5 : SELF CONSISTENT (or HB1), INITIATED BY LINEAR RESULTS WITH A GUESS
%             (alternative to the method using the WNL as a guess)
%
% HERE the initial guess is for Re=47 (slightly above the instability threshold)
% The initialisation is done with the linear eigmode
% with a "small" amplitude (measured by lift force), namely Fy=0.006 .



    disp(' ');disp('###### autorun test 5 : HB1 #######');disp(' ');
    [meanflow,mode] = SF_HB1(meanflow,mode,'Re',Res);
    Lx_HB = meanflow.Lx
    Fx_HB = meanflow.Fx
    omega_HB = imag(mode.lambda)
    Aenergy_HB  = mode.AEnergy
    Fy_HB = mode.Fy
    
    abs(Lx_HB/Lx_HB_REF-1);
    abs(Fx_HB/Fx_HB_REF-1);
    abs(omega_HB/omega_HB_REF-1);
    abs(Aenergy_HB/Aenergy_HB_REF-1);
    abs(Fy_HB/Fy_HB_REF-1);
    
error(5) = abs(Lx_HB/Lx_HB_REF-1)+abs(Fx_HB/Fx_HB_REF-1)+abs(omega_HB/omega_HB_REF-1)+abs(Fy_HB/Fy_HB_REF-1)




% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
