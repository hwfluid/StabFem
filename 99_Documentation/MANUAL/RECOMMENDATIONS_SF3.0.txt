Recommmendations for updating your cases to StabFem 3.0 

0. Macros_StabFem.edp does not exist any longer !
A/ in your .edp (mesh generation, etc...) replace include "Macros_StabFem.edp" by include "Macros_StabFem.idp"
B/ rename your Macros_StabFem.edp into SF_Custom.idp

################

1. SF_Custom.idp
################

Macros_StabFem.edp does not exist any longer ! If you are still using one you should :

A/  
In your .edp files (mesh generation, etc...) replace 'include "Macros_StabFem.edp"' by 'include "Macros_StabFem.idp"'

B/
move Macros_StabFem.edp into the new name SF_Custom.idp 
(NB use 'git mv' and make sure you REMOVE Macros_StabFem.edp from git repository ! )

C/ update this file (you should be able to remove most of its content)

This file must now contain :
- info on solvers (for instance if using SLEPC) 
- macro STORAGEMODES "P2,P2P2P1" // (or some other combination including what you need)
- the boundary-conditions and post-processing macros IF THEY DIFFER FROM DEFAULT ONES
(including, if needed, macros like Uinlet, Trainee, Portance, CalcPsi...)
If you are using customized macros WE RECOMMEND THAT YOU UPDATE THEM SO THAT 
THEY ARE AS CLOSE AS POSSIBLE TO THE DEFAULT ONES.

What this file should NOT contain any more :
- include "workdir.idp" (in older versions string ffdatadir = "./WORK/")
- Macro SFWriteConnectivity (now in SF_Tools)
- macro FREEFEMPLOTS is now useless (and should be removed from the solvers as well)
- all things related to complex mapping



2. Mesh generation
##################

We now require to write only 3 files : mesh.msh, mesh.ff2m and mesh_connectivity.ff2m

The mesh generator should end up with something like this :

// SAVE THE MESH in mesh.msh file 
savemesh(th,ffdatadir+"mesh.msh");

// AUXILIARY FILE  for Stabfem : mesh.ff2m
	SFWriteMesh(ffdatadir+"mesh.ff2m",th)

// AUXILIARY FILE NEEDED TO PLOT P2 and vectorial DATA
    SFWriteConnectivity(ffdatadir+"mesh_connectivity.ff2m",th);
    
ALL THE OTHER FILES ARE NOW USELESS : "SF_Init.ff2m"; "SF_Geom.edp" "BaseFlow_guess.txt";  "BaseFlow_guess.ff2m"

// Recommendation for boundary numbering (used by default BC macros)
1			Inlet
2			Wall
21,22,23 	Wall used in post-processing (for instance the surface of your object)
3			Outlet (no stress)
4			Slip or symmetry (for horizontal boundary)
5			Slip or symmetry (for vertical boundary)
6			Symmetry axis (for axisymmetric cases ; equivalent to 4 for 2D cases)
7			Deformable solid (to be implemented) or porous object
8			Deformable free surface 
9			Internal boundaries used during mesh generation


3. Base flow calculation
########################

The structure of your Newton solvers should look like this :

Chap. 0 : Preambule 
Please update the header so that it is up-to date with what the solver does !
The only included file for most cases should be Macros_StabFem.idp (in folder INCLUDE) 
which lists all the .idp files to be included

Chap. 1	: Input parameters
Even if this looks useless when using the drivers, it is recommended that the drivers still work in
direct bash modes, so that messages AND values of the parameters are displayed with cout
(so that everything appears in the freefem log file)
for instance :
cout << "Enter Re >> " ;
cin << Re;
cout << Re << endl;

Chap. 2 : lecture of mesh and baseflow files
Please use SFcheckfile to detect if the file exists and determine its dimension.
Use SFwarning to signal if a baseflow was found or not.

Chap. 3 : definition of macros (DEFAULT ONES)
A. boundary conditions
B. postprocessing
C. differential operators 

Chap. 4 : Build operators

Chap. 5 : Resolution (Newton loop)

Chap. 6 Post-processing
In case of failure please use macro "SFerror" and generate error code 201
Otherwise use the macros to generage BaseFlow.ff2m
NB in BaseFlow.txt, if you need to write auxiliary data possibly needed by subsequent solvers,
please reference them using keywords "datastoragemode" and "datadescriptors" in the 3rd line of BaseFLow.ff2m

(plots as you want ! the FREEFEMPLOTS macro is no longer useful because "-nw" is used when lanching through the wrapper)

4. Eigenmode calculation
########################
Note that ALL the solvers should now be written in mapped coordinates 
-> Use dX,dY instead of dx,dy and include metric factor JJJ in integrals (replaces y or Rphys*JJ)


The structure of your Stability solvers should look like this :

Chap. 0 : Preambule 
(same recommendations as for base flows)

Chap. 1	: Input parameters
(same recommendations as for base flows)

Chap. 2 : lecture of mesh, baseflow and possibly eigenmode_guess files
Please use SFcheckfile to detect if the file exists and determine its dimension.

Chap. 3 : definition of macros (DEFAULT ONES)
A. default dX=dx, dY=dy
B. boundary conditions
C. postprocessing
D. differential operators (same as in bf but in mapped coordinates)

Chap. 4 : Build operators

Chap. 5 : Resolution 
Here we should find a way to rationalize and simplify !
-> generic macros SFShiftInvert, SFEVsolve in SF_Tools ???

Chap. 6 : Post-processing
In case of failure please use macro "SFerror" and generate error code 202
Otherwise use the macros to generage Eigenmodes_#.ff2m

Chap. 7 : Structural sensitivity

(NB in chap 5.6.7 maybe we could find a simpler way to handle direct/adjoint/sensitivity 
and single/multiple mode calculations ?




    